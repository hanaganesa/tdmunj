<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head'); ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('home') ?>" class="simple-text logo-normal">
          <i class="fa fa-home" aria-hidden="true"></i>&nbsp;
          Home
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarU'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbarU'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="container-fluid">
            <div class="card">
              <!--awal-->
              <div class="card-header card-header-primary">
                <h4 class="card-title">Berikut adalah manual yang ada di halaman ini</h4>
              </div>
              <div class="card-body">
                <table class="table table-light">
                  <p>1. Cara Sign Up/Registrasi</p>
                  <p>2. Cara Sign In/Log In</p>
                  <p>3. Ukur Kesamaan Skripsi</p>
                </table>
              </div>
              <!--akhir awal-->
              <!--registrasi-->
              <div class="card-header card-header-primary">
                <h4 class="card-title">Cara Sign Up/Registrasi</h4>
              </div>
              <div class="card-body">
                <table class="table table-light">
                  <p>1. Klik "REGISTRASI" di halaman awal.</p>
                  <img src="<?php echo base_url('assets/manual/frontend/reg1.png') ?>" alt="" width="100%"><br><br>
                  <p>2. Isi data dengan lengkap, lalu klik "Daftar".</p>
                  <img src="<?php echo base_url('assets/manual/frontend/reg2.png') ?>" alt="" width="100%"><br><br>
                </table>
              </div>
              <!--akhir registrasi-->
              <!--sign in-->
              <div class="card-header card-header-primary">
                <h4 class="card-title">Cara Sign In/Log In</h4>
              </div>
              <div class="card-body">
                <table class="table table-light">
                  <p>1. Klik "LOGIN" di halaman awal.</p>
                  <img src="<?php echo base_url('assets/manual/frontend/log2.png') ?>" alt="" width="100%"><br><br>
                  <p>2. Isi username/nim/nidn dan password, lalu klik "Masuk".</p>
                  <img src="<?php echo base_url('assets/manual/frontend/log1.png') ?>" alt="" width="100%"><br><br>
                </table>
              </div>
              <!--akhir sign in-->
              <!--ukur-->
              <div class="card-header card-header-primary">
                <h4 class="card-title">Ukur Kesamaan Skripsi</h4>
              </div>
              <div class="card-body">
                <table class="table table-light">
                  <p>1. Klik "List Skripsi".</p>
                  <img src="<?php echo base_url('assets/manual/frontend/ukur.png') ?>" alt="" width="100%"><br><br>
                  <p>2. Klik "Ukur Kesamaan Skrpisi".</p>
                  <img src="<?php echo base_url('assets/manual/frontend/ukur1.png') ?>" alt="" width="100%"><br><br>
                  <p>3. Isi judul dan abstrak skripsi, lalu klik "Ukur".</p>
                  <img src="<?php echo base_url('assets/manual/frontend/ukur2.png') ?>" alt="" width="100%"><br><br>
                  <p>4. Akan muncul skripsi yang memiliki kemiripan diatas 30%.</p>
                  <img src="<?php echo base_url('assets/manual/frontend/ukur4.png') ?>" alt="" width="100%"><br><br>
                  <p>5. Jika tidak ada skripsi yang mirip maka tidak akan keluar data apapun.</p>
                  <img src="<?php echo base_url('assets/manual/frontend/ukur3.png') ?>" alt="" width="100%"><br><br>
                </table>
              </div>
              <!--akhir ukur-->
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/material-dashboard-master/assets/js/plugins/bootstrap-notify.js') ?>"></script>
</body>

</html>