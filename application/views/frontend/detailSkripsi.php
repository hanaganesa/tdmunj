<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('umum-skripsi') ?>" class="simple-text logo-normal">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;
          Back
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarU'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbarU'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title ">Detail Skripsi</h3>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="dataTable">
                      <?php
                      if ($histori_count != 0) {
                        $no = 1;
                        foreach (@$histori as $h) {
                      ?>
                          <tbody>
                            <tr></tr>
                            <th>NIM</th>
                            <td align="left"><?php echo $h->username ?></td>
                            <tr></tr>
                            <th>Nama</th>
                            <td align="left"><?php echo $h->nama ?></td>
                            <tr></tr>
                            <th>Judul</th>
                            <td class="text-primary" align="justify"><?php echo $h->judul_histori ?></td>
                            <tr></tr>
                            <th>Abstrak</th>
                            <td align="justify"><?php echo $h->abstrak_histori ?></td>
                            <tr></tr>
                            <th>Kategori</th>
                            <td align="left"><?php echo $h->kategori_histori ?></td>
                            <tr></tr>
                            <th>Status</th>
                            <td align="left">
                              <?php if ($h->status_histori == 'pengajuan') {
                                echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status_histori;
                              } elseif ($h->status_histori == 'ditolak') {
                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status_histori;
                              } elseif ($h->status_histori == '-') {
                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status_histori;
                              } else {
                                echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status_histori;
                              }
                              ?>
                            </td>
                            <tr></tr>
                          </tbody>
                        <?php
                        }
                      } elseif ($histori_count == 0) {
                        foreach ($skripsi as $s) {
                        ?>
                          <tbody>
                            <tr></tr>
                            <th>NIM</th>
                            <td align="left"><?php echo $s->username ?></td>
                            <tr></tr>
                            <th>Nama</th>
                            <td align="left"><?php echo $s->nama ?></td>
                            <tr></tr>
                            <th>Judul</th>
                            <td class="text-primary" align="justify"><?php echo $s->judul ?></td>
                            <tr></tr>
                            <th>Abstrak</th>
                            <td align="justify"><?php echo $s->abstrak ?></td>
                            <tr></tr>
                            <th>Kategori</th>
                            <td align="left"><?php echo $s->kategori ?></td>
                            <tr></tr>
                          </tbody>
                      <?php
                        }
                      }
                      ?>
                    </table>
                  </div>
                </div>
              </div>
              <a class="btn btn-default pull-right" href="<?php echo base_url('umum-skripsi') ?>" role="button"><i class="fa fa-hand-o-left">&nbsp;</i></i>Back</a>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
</body>

</html>