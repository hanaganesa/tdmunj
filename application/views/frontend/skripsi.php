<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head'); ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('home') ?>" class="simple-text logo-normal">
          <i class="fa fa-home" aria-hidden="true"></i>&nbsp;
          Home
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarU'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbarU'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title ">List Skripsi</h3>
                  <p class="card-category"> List Skripsi Mahasiswa</p>
                </div>
                <div class="card-body">
                  <!-- <h4>Filter Kategori</h4>
                  <select name="" id="kategori" class="caret">
                    <option value="0">Show All</option>
                    <option value="pendidikan">Pendidikan</option>
                    <option value="linguistik">Linguistik</option>
                  <option value="komprehensif">Komprehensif</option>
                  </select>
                  <br><br> -->
                  <div class="table-responsive">
                    <table class="table" id="dataTable">
                      <thead class=" text-primary">
                        <th>No</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Judul Skripsi</th>
                        <th>Kategori</th>
                      </thead>
                      <tbody>
                        <?php
                        $no = 1;
                        foreach ($skripsi as $m) {
                        ?>
                          <tr>
                            <td><?php echo $no++ ?></td>
                            <td class="text-primary"><?php echo $m->username ?></td>
                            <td><?php echo $m->nama ?></td>
                            <td><a href="<?= ('umum-skripsi/detail/' . @$m->id_histori . '/' . @$m->id_skripsi) ?>" title="Detail"><?php echo $m->judul_histori ?></a></td>
                            <td><?php echo $m->kategori_histori ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/material-dashboard-master/assets/js/plugins/bootstrap-notify.js') ?>"></script>
</body>

</html>