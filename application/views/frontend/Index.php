<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
  <title>TDM UNJ</title>
  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
  <link href="<?php echo base_url('assets/https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300') ?>" rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/bootstrap.min.css") ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/font-awesome.min.css") ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/animate.css") ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/style.css") ?>">
</head>

<body>
  <!--header-->
  <header class="main-header" id="header">
    <div class="bg-color">
      <!--nav-->
      <nav class="nav navbar-default navbar-fixed-top">
        <div class="container">
          <div class="col-md-12">
            <div class="navbar-header">
              <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mynavbar" aria-expanded="false" aria-controls="navbar">
                            <span class="fa fa-bars"></span>
                        </button> -->
              <a href="<?php echo base_url('home') ?>" class="navbar-brand">TDM UNJ</a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mynavbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#header">Home</a></li>
                <li><a href="#feature">Feature</a></li>
                <li><a href="#portfolio">日本</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!--/ nav-->
      <div class="container text-center"><br><br><br>
        <div class="wrapper wow fadeInUp delay-05s">
          <h2 class="top-title">Universitas Negeri Jakarta</h2>
          <h4 class="title">Thesis Data Manager</h4>
          <h4 class="sub-title">Pendidikan Bahasa Jepang</h4>
          <a href="<?php echo base_url('signup') ?>"><button type="submit" class="btn btn-submit">Registrasi</button></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo base_url('umum-skripsi') ?>"><button type="submit" class="btn btn-submit">List Skripsi</button></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo base_url('signin') ?>"><button type="submit" class="btn btn-submit">Login</button></a><br><br><br><br>
        </div>
      </div>
    </div>
  </header>
  <section id="cta-1">
    <div class="container">
      <div class="row">
        <div class="cta-info text-center">
          <h3>VISI<br><br></span>Menjadi Program Studi Pendidikan Bahasa Jepang unggulan pada tataran nasional dengan mencetak sarjana pendidikan bahasa Jepang profesional yang memiliki keunggulan, keahlian dan intelektual berjiwa entrepreneur, percaya diri, mandiri dan berwawasan luas sehingga mampu mengaplikasikannya untuk mengembangkan pendidikan dan pengajaran bahasa Jepang di sekolah menengah, pendidikan nonformal.</span></h3>
          <!-- <h3><span class="dec-tec">“</span>Tell me and I FORGET. Teach me and I REMEMBER. Involve me and I LEARN.<span class="dec-tec">”</span> -Benjamin Franklin</h3> -->
        </div>
      </div>
    </div>
  </section>
  <section id="feature" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-3 wow fadeInLeft delay-05s">
          <div class="section-title">
            <h2 class="head-title">Fitur</h2>
            <hr class="botm-line">
            <p class="sec-para">Berikut merupakan fitur dari aplikasi website Thesis Data Manager yang sedang dalam proses pengembangan..</p>
          </div>
        </div>
        <div class="col-md-9">
          <div class="col-md-6 wow fadeInRight delay-02s">
            <div class="icon">
              <i class="fa fa-paint-brush"></i>
            </div>
            <div class="icon-text">
              <h3 class="txt-tl">Kelola Data</h3>
              <p class="txt-para">Kelola data skripsi, daftarkan dan ubah data skripsi dapat dilakukan secara online. </p>
            </div>
          </div>
          <div class="col-md-6 wow fadeInRight delay-02s">
            <div class="icon">
              <i class="fa fa-cogs"></i>
            </div>
            <div class="icon-text">
              <h3 class="txt-tl">Papan Pengumuman</h3>
              <p class="txt-para">Lihat pengumuman terbaru dari koordinator skripsi. </p>
            </div>
          </div>
          <div class="col-md-6 wow fadeInRight delay-04s">
            <div class="icon">
              <i class="fa fa-mobile"></i>
            </div>
            <div class="icon-text">
              <h3 class="txt-tl">Lihat Data Skripsi</h3>
              <p class="txt-para">Cari referensi? Fitur ini menampilkan data skripsi mahasiswa yang dapat diakses melalui menu list skripsi. </p>
            </div>
          </div>
          <div class="col-md-6 wow fadeInRight delay-06s">
            <div class="icon">
              <i class="fa fa-lightbulb-o"></i>
            </div>
            <div class="icon-text">
              <h3 class="txt-tl">Ubah Judul Skripsi</h3>
              <p class="txt-para">Ajukan perubahan judul skripsi pada fitur ubah judul skripsi. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-padding parallax bg-image-2 section wow fadeIn delay-08s" id="cta-2">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="cta-txt">
            <h3>昨日 途中であきらめたこと、今日もう一度 始めよう！</h3>
            <p>命は美しい — 乃木坂46</p>
            <!-- <h3>Check The Latest Information or Article</h3>
            <p>Check unjngkronbun website for latest information about thesis, schedule, and other knowlegde.</p> -->
          </div>
        </div>
        <!-- <div class="col-md-4 text-center">
          <a href="https://unjngkronbun.000webhostapp.com/" class="btn btn-submit">Go To Website</a>
        </div> -->
      </div>
    </div>
  </section>
  <!---->
  <!---->
  <section class="section-padding wow fadeInUp delay-02s" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="section-title">
            <h2 class="head-title">日本</h2>
            <hr class="botm-line">
            <p class="sec-para">
              日本国（にほんこく、にっぽんこく、英: Japan）、または日本（にほん、にっぽん）は、東アジアに位置し、日本列島および南西諸島・伊豆諸島・小笠原諸島などからなる民主制国家。首都は東京都。<br><br>
              気候は四季の変化に富み、国土の多くは山地で、人口は沿岸の平野部に集中している。国内には行政区分として47の都道府県があり、大和民族・琉球民族・アイヌ民族・外国人系の人々などが居住し、事実上の公用語として日本語が使用される。(Wikipedia) </p>
          </div>
        </div>
        <div class="col-md-9 col-sm-12">
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/omise.jpeg') ?>" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/temple.jpeg') ?>" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/httpswww.google.commapscontrib112987405780231554263photos@35.3199737,136.1676063,7zdata=!3m1!4b1!4m3!8m2!3m1!1e1hl=en-GB.jpg') ?>" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/sakura.jpeg') ?>" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/shibuya.jpeg') ?>" alt="" class="img-responsive">
            </div>
          </div>
          <div class="col-md-4 col-sm-6 padding-right-zero">
            <div class="portfolio-box design">
              <img src="<?php echo base_url('assets/img/fuji.jpeg') ?>" alt="" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!---->
  <!---->
  <section class="section-padding wow fadeInUp delay-05s" id="contact">
    <div class="container">
      <div class="row white">
        <div class="col-md-8 col-sm-12">
          <div class="section-title">
            <h2 class="head-title black">Hubungi Kami</h2>
            <hr class="botm-line">
            <!-- <p class="sec-para black">Informasi</p> -->
            <div class="location-info">
              <p class="white"><span aria-hidden="true" class="fa fa-map-marker"></span>Jl. Margonda Raya, RT.11/RW.14, Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13220</p>
              <p class="white"><span aria-hidden="true" class="fa fa-phone"></span>Phone: (021) 4898486</p>
              <p class="white"><span aria-hidden="true" class="fa fa-envelope"></span>Email: <a href="" class="link-dec">humas@unj.ac.id</a></p>
            </div><br><br><br><br>
          </div>
        </div>
        <div class="col-md-4 col-sm-12">
          <div class="contact-icon-container hidden-md hidden-sm hidden-xs">
            <span aria-hidden="true" class="fa fa-envelope-o"></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <!---->
  <!---->
  <footer class="" id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 footer-copyright">
          © TDM UNJ - 2019<br>
          © Bethany Theme - All rights reserved
          <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!---->
  <!--contact ends-->
  <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.easing.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/wow.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
  <script src="<?php echo base_url('assets/contactform/contactform.js') ?>"></script>

</body>

</html>