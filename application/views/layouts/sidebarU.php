<div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a href="<?php echo base_url ('umum-skripsi') ?>" class="nav-link">
              <i class="material-icons">view_list</i>
              <p>List Skripsi</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item">
            <a href="<?php echo base_url ('umum-skripsi/kesamaan') ?>" class="nav-link">
              <i class="material-icons">search</i>
              <p>Ukur Kesamaan Skripsi</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item">
            <a href="<?php echo base_url ('umum/help') ?>" class="nav-link">
              <i class="material-icons">help</i>
              <p>Help</p>
            </a>
          </li>
        </ul>
      </div>