<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <a class="navbar-brand"><?= @$judul ?></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <!-- nama -->
        <div class="nav-item"><?= @$nama; ?></div>
        <!-- end nama -->
        <?php if (@$jenisAkun != 'admin') { ?>
          <li class="nav-item dropdown">
            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">notifications</i>
              <span class="notification" id="notif-count"></span>
              <p class="d-lg-none d-md-block">
                Some Actions
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" id="notif-list">

            </div>
          </li>
        <?php  } ?>
        <li class="nav-item dropdown">
          <a class="nav-link" href="" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <?php if (@$jenisAkun == 'admin') { ?>
              <a class="dropdown-item" href="<?= base_url('admin-profile') ?>">Profile</a>
            <?php } ?>
            <?php if (@$jenisAkun == 'mahasiswa') { ?>
              <a class="dropdown-item" href="<?= base_url('mahasiswa-profile') ?>">Profile</a>
            <?php } ?>
            <?php if (@$jenisAkun == 'dosen') { ?>
              <a class="dropdown-item" href="<?= base_url('dosen-profile') ?>">Profile</a>
            <?php } ?>
            <a class="dropdown-item" href="<?= base_url('logout') ?>">Log out</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>