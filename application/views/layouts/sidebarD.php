<div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('dashboard-dosen') ?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('dosen-skripsi') ?>">
              <i class="material-icons">content_paste</i>
              <p>List Skripsi</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('dosen-bimbingan') ?>">
              <i class="material-icons">supervised_user_circle</i>
              <p>Bimbingan</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('dosen/help') ?>">
              <i class="material-icons">help</i>
              <p>Help</p>
            </a>
          </li>
        </ul>
      </div>