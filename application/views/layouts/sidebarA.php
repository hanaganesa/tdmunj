<div class="sidebar-wrapper">
  <ul class="nav">
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('dashboard-admin') ?>">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('post') ?>">
        <i class="material-icons">notifications</i>
        <p>Postingan</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('admin-skripsi') ?>">
        <i class="material-icons">content_paste</i>
        <p>List Skripsi</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('admin-bimbingan') ?>">
        <i class="material-icons">supervisor_account</i>
        <p>List Bimbingan</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('mahasiswa') ?>">
        <i class="material-icons">how_to_reg</i>
        <p>List Mahasiswa</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('dosen') ?>">
        <i class="material-icons">person_add</i>
        <p>Kelola Akun Dosen</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('admin') ?>">
        <i class="material-icons">people_alt</i>
        <p>Kelola Admin</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('laporan') . '?tahun=' . date('Y'); ?>">
        <i class="material-icons">equalizer</i>
        <p>Laporan</p>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('admin/help') ?>">
        <i class="material-icons">help</i>
        <p>Help</p>
      </a>
    </li>
  </ul>
</div>