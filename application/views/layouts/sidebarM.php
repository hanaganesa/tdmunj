<div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('dashboard-mahasiswa') ?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('mahasiswa-listSkripsi') ?>">
              <i class="material-icons">content_paste</i>
              <p>List Skripsi</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('mahasiswa-skripsi') ?>">
              <i class="material-icons">book</i>
              <p>Kelola Skripsi</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('pembimbing') ?>">
              <i class="material-icons">supervisor_account</i>
              <p>Pembimbing</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('mahasiswa-skripsi/kesamaan') ?>">
              <i class="material-icons">search</i>
              <p>Ukur Kesamaan Skripsi</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo base_url ('mahasiswa/help') ?>">
              <i class="material-icons">help</i>
              <p>Help</p>
            </a>
          </li>
        </ul>
      </div>