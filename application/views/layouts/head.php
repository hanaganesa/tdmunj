<meta charset="utf-8">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
<link rel="icon" type="image/png" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>
  TDM UNJ
</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- CSS Files -->
<link href="<?php echo base_url('assets/material-dashboard-master/assets/css/material-dashboard.css?v=2.1.1') ?>" rel="stylesheet">
<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="<?php echo base_url('assets/material-dashboard-master/assets/demo/demo.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/datatables/datatables.css') ?>" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/dropify/dist/css/dropify.min.css') ?>" rel="stylesheet">