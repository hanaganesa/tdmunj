<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

 <!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url ('dashboard-dosen') ?>" class="simple-text logo-normal">
          Dosen Pembimbing
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarD.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
      <div class="container-fluid">
        <div class="container-fluid">
            <div class="card">
                <!--awal-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Berikut adalah manual yang ada di halaman ini</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Cara Acc Request Pembimbingan</p>
                        <p>2. Cara Acc Request Perubahan Judul</p>
                        <p>3. Cara Melakukan Edit Profile</p>
                        <p>Jika ada error silahkan laporkan melalui email ke: hanafitriaganesa@gmail.com (dengan screenshot lebih baik)</p>
                        <p>Terima kasih</p>
                    </table>
                </div>
                <!--akhir awal-->
                <!--pbb-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Acc Request Pembimbingan</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik menu "Bimbingan" atau "Lihat" di card Request Mahasiswa. Angka di card menunjukkan jumlah mahasiswa yang meminta request.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pbb1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Di halaman bimbingan, bagian "Request Mahasiswa Bimbingan" akan menunjukkan data mahasiswa yang melakukan request. Kolom "Pembimbing"
                        menunjukkan request untuk menjadi pembimbing 1 atau 2. Jika mahasiswa tersebut benar merupakan mahasiswa bimbingan, klik tombol "Terima". Jika
                        mahasiswa bukan merupakan mahasiswa bimbingan, maka klik tombol "Tolak".</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pbb2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Data mahasiswa bimbingan akan tersimpan di bagian "Mahasiswa Bimbingan".</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pbb3.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir pbb-->
                <!--pbb-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Acc Request Perubahan Judul</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik menu "Bimbingan" atau "Lihat" di card Request Ganti Judul. Angka di card menunjukkan jumlah mahasiswa yang meminta request ganti judul.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/bim1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Di halaman bimbingan, bagian "Request Ganti Judul" akan menunjukkan data mahasiswa yang melakukan request ganti judul. Klik tombol
                        "Detail".</p>
                        <img src="<?php echo base_url('assets/manual/dosen/bim2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Halaman detail ganti judul akan menampilkan data skripsi lama dan data skripsi baru yang diajukan oleh mahasiswa. Gambar menunjukkan bahwa dosen
                        merupakan pembimbing 2 dari mahasiswa dan status acc oleh pembimbing 1 masih dalam proses pengajuan.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/bim3.png') ?>" alt="" width="100%"><br><br>
                        <p>4. Dosen dapat menerima atau menolak permintaan perubahan judul.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/bim4.png') ?>" alt="" width="100%"><br><br>
                        <p>5. Pada bagian bawah terdapat data skripsi mahasiswa lain yang memiliki kemiripan diatas 30% dengan data skripsi baru yang diajukan
                        oleh mahasiswa.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/bim5.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir pbb-->
                <!--pro-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Melakukan Edit Profile</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik icon di pojok kanan atas seperti gambar di bawah.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pro1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Lalu pilih menu "Profile".</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pro2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Klik "Submit" jika ingin menyimpan perubahan.</p>
                        <img src="<?php echo base_url('assets/manual/dosen/pro3.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir pro-->
            </div>
        </div>
    </div>
    </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer') ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
</body>

</html>
