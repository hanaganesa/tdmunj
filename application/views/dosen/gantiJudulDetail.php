<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php')  ?>
  <style>
    .tb-kemiripan td div {
      height: 150px;
      overflow: scroll;
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-dosen') ?>" class="simple-text logo-normal">
          Dosen Pembimbing
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarD.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="container-fluid">
            <div class="card">
              <div class="card-header card-header-primary">
                <h3 class="card-title">Detail Skripsi</h3>
              </div>
              <div class="card-body">
                <table class="table table-light">
                  <tbody>
                    <tr></tr>
                    <tr>
                      <th>NIM</th>
                      <td class="text-primary"><?= $skripsi->username ?></td>
                      <th>Nama Mahasiswa</th>
                      <td><?= $skripsi->nama ?></td>
                    </tr>
                    <tr>
                      <?php if ($bimbingan->pembimbing == "1") { ?>
                        <th>Pembimbing</th>
                        <td>1</td>
                        <th>Status ACC Pembimbing 2</th>
                        <td><?= $skripsi->status2 ?></td>
                      <?php } else { ?>
                        <th>Pembimbing</th>
                        <td>2</td>
                        <th>Status ACC Pembimbing 1</th>
                        <td><?= $skripsi->status1 ?></td>
                      <?php } ?>
                    </tr>
                    <tr align="center">
                      <th colspan="2">Skripsi yang Sedang Dikerjakan</th>
                      <th colspan="2">Perubahan Skripsi yang Diajukan</th>
                    </tr>
                    <tr>
                      <th width="10%">Judul Skripsi Lama</th>
                      <td align="justify" width="40%"><?= $histori1->judul_histori ?></td>
                      <th width="10%">Judul Skripsi yang Diajukan</th>
                      <td align="justify" width="40%"><?= $skripsi->judul_histori ?></td>
                    </tr>
                    <tr>
                      <th width="10%">Abstrak Skripsi Lama</th>
                      <td align="justify" width="40%"><?= $histori1->abstrak_histori ?></td>
                      <th width="10%">Abstrak Skripsi yang Diajukan</th>
                      <td align="justify" width="40%"><?= $skripsi->abstrak_histori ?></td>
                    </tr>
                    <tr>
                      <th width="10%">Kategori Skripsi Lama</th>
                      <td width="40%"><?= $histori1->kategori_histori ?></td>
                      <th width="10%">Kategori Skripsi yang Diajukan</th>
                      <td width="40%"><?= $skripsi->kategori_histori ?></td>
                    </tr>
                    <tr>
                      <th width="10%">Alasan Ganti Judul</th>
                      <td colspan="3"><?= $skripsi->alasan ?></td>
                    </tr>
                    <tr>
                      <td colspan="4"></td>
                    </tr>
                  </tbody>
                </table>
                <a class="btn btn-danger pull-right btn-tolak form-submit" data-href="<?= base_url('dosen-bimbingan/tolakJudul/' . $bimbingan->pembimbing . '/' . $skripsi->id_histori . '/' . $histori1->id_skripsi) ?>" role="button" href="#" title="Tolak Perubahan Judul"><i class="fa fa-times-circle">&nbsp;&nbsp;</i></i>Tolak</a>
                <a class="btn btn-primary pull-right btn-acc form-submit" data-href="<?= base_url('dosen-bimbingan/acceptJudul/' . $bimbingan->pembimbing . '/' . $skripsi->id_histori . '/' . $histori1->id_skripsi) ?>" role="button" href="#" title="Terima Perubahan Judul"><i class="fa fa-check-circle">&nbsp;&nbsp;</i></i>Terima</a>
                <a class="btn btn-default pull-left" href="<?php echo base_url('dosen-bimbingan') ?>" role="button" title="Back"><i class="fa fa-hand-o-left">&nbsp;&nbsp;</i></i>Back</a>
              </div>
              <br><br>
              <div class="card-header card-header-success">
                <h3 class="card-title">Kemiripan Skripsi yang Diajukan dengan Skripsi Mahasiswa Lain</h3>
              </div>
              <div class="card-body">
                <table class="table table-hover tb-kemiripan">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Kemiripan</th>
                      <th>NIM</th>
                      <th>Nama Mahasiswa</th>
                      <th>Judul Skripsi</th>
                      <th>Abstrak</th>
                      <th>Kategori</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (@$skripsiMhsLain != NULL) {
                      foreach (@$skripsiMhsLain as $key => $v) {
                        if ($v->percent >= 30) { ?>
                          <tr>
                            <td style="vertical-align: top"><?= $key + 1 ?></td>
                            <td style="vertical-align: top">
                              <?php
                              echo 'Kemiripan Judul: <font color="red">' . $v->percent . '%' . '</font><br>';
                              echo 'Kemiripan Abstrak: <font color="red">' . $v->percent2 . '%' . '</font>';
                              ?>
                            </td>
                            <td style="vertical-align: top"><?= $v->username ?></td>
                            <td style="vertical-align: top"><?= $v->nama ?></td>
                            <td style="vertical-align: top">
                              <div><?= $v->judul ?></div>
                            </td>
                            <td style="vertical-align: top">
                              <div><?= $v->abstrak ?></div>
                            </td>
                            <td style="vertical-align: top"><?= $v->kategori ?></td>
                          </tr>
                        <?php
                        } else if ($v->percent2 >= 30) { ?>
                          <tr>
                            <td style="vertical-align: top"><?= $key + 1 ?></td>
                            <td style="vertical-align: top">
                              <?php
                              echo 'Kemiripan Judul: <font color="red">' . $v->percent . '%' . '</font><br>';
                              echo 'Kemiripan Abstrak: <font color="red">' . $v->percent2 . '%' . '</font>';
                              ?>
                            </td>
                            <td style="vertical-align: top"><?= $v->username ?></td>
                            <td style="vertical-align: top"><?= $v->nama ?></td>
                            <td style="vertical-align: top">
                              <div><?= $v->judul ?></div>
                            </td>
                            <td style="vertical-align: top">
                              <div><?= $v->abstrak ?></div>
                            </td>
                            <td style="vertical-align: top"><?= $v->kategori ?></td>
                          </tr>
                      <?php
                        }
                      }
                    } else { ?>
                      <tr>
                        <td colspan="7" align="center">Tidak Ada Data</td>
                      </tr>
                    <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <div id="ModalStatus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalStatus" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="my-modal-title">Title</h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="get" action="" id="formstatus">
          <div class="modal-body">
            <div class="form-group">
              <label for="komentar">Komentar</label>
              <textarea id="komentar" class="form-control" name="komentar" rows="3"></textarea>
            </div>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-primary">Kirim</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol hapus di click
      $(document).on('click', '.btn-tolak', function() {
        $('#formstatus').attr('action', $(this).data('href'));
        $('#my-modal-title').html('Tolak Perubahan Judul');
        $('#ModalStatus').modal('show');
      });
      $(document).on('click', '.btn-acc', function() {
        $('#formstatus').attr('action', $(this).data('href'));
        $('#my-modal-title').html('Terima Perubahan Judul');
        $('#ModalStatus').modal('show');
      });
    });
  </script>
</body>

</html>