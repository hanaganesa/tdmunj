<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head.php')  ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-dosen') ?>" class="simple-text logo-normal">
          Dosen Pembimbing
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarD.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="container-fluid">
            <div class="card">
              <div class="card-header card-header-primary">
                <h3 class="card-title">Detail Mahasiswa</h3>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table border="0" align="center">
                    <tbody><br>
                      <tr></tr>
                      <td align="center"><img src="<?php echo base_url('assets/foto/') . @$mahasiswa->foto ?>" class="img-responsive" width="200" alt="Image"></td>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                      <td align="center">
                        <h4><?php echo $mahasiswa->username ?></h4>
                      </td>
                      <tr></tr>
                      <td align="center">
                        <h4><?php echo $mahasiswa->nama ?></h4>
                      </td>
                      <tr></tr>
                      <td align="center">
                        <h4><?php echo $mahasiswa->email ?></h4>
                      </td>
                      <tr></tr>
                    </tbody>
                  </table>
                  <table>
                    <td><?php if (@$pembimbing1 == NULL) { ?>
                        <h5>Pembimbing 1 : Tidak ada</h5>
                      <?php } elseif (@$pembimbing1 != NULL) { ?>
                        <h5>Pembimbing 1 : <?php echo $pembimbing1->nama_dosen ?></h5>
                      <?php } ?>
                    </td>
                    <tr></tr>
                    <td><?php if (@$pembimbing2 == NULL) { ?>
                        <h5>Pembimbing 2 : Tidak ada</h5>
                      <?php } elseif (@$pembimbing2 != NULL) { ?>
                        <h5>Pembimbing 2 : <?php echo $pembimbing2->nama_dosen ?></h5>
                      <?php } ?>
                    </td>
                  </table><br>
                  <table class="table">
                    <thead>
                      <th>Judul Sempro</th>
                      <th>Kategori</th>
                    </thead>
                    <tbody>
                      <?php
                      foreach (@$skripsi as $m) {
                      ?>
                        <tr>
                          <td><a href="<?= base_url('dosen-skripsi/detail/' . $mahasiswa->username . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $m->judul ?></a></td>
                          <td><?php echo $m->kategori ?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <?php if ($histori1 != NULL) { ?>
                    <table class="table">
                      <thead>
                        <th>Skripsi Aktif</th>
                        <th>Kategori</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a href="<?= base_url('dosen-skripsi/detail/' . $histori1->id_histori . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $histori1->judul_histori ?></a></td>
                          <td><?php echo $histori1->kategori_histori ?></td>
                        </tr>
                      </tbody>
                    </table>
                  <?php } ?>
                  <td>
                    <h3>Histori Skripsi</h3>
                  </td>
                  <table class="table" id="dataTable">
                    <thead>
                      <th>No.</th>
                      <th>Tanggal & Waktu</th>
                      <th>Judul</th>
                      <th>Status</th>
                      <th>Status Pembimbing 1</th>
                      <th>Status Pembimbing 2</th>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach (@$histori as $h) {
                      ?>
                        <tr>
                          <td><?php echo $no . '.'; ?></td>
                          <td><?php echo $h->tanggal_histori ?></td>
                          <?php if ($pembimbing == 2) { ?>
                            <td><a href="<?= base_url('dosen-skripsi/detail/' . $h->id_histori . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $h->judul_histori ?></a></td>
                          <?php } elseif ($pembimbing != 2) { ?>
                            <td><?php echo $h->judul_histori ?></td>
                          <?php } ?>
                          <td><?php echo $h->status_histori ?></td>
                          <td>
                            <?php if ($h->status1 == 'pengajuan') {
                              echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status1;
                            } elseif ($h->status1 == 'ditolak') {
                              echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                            } elseif ($h->status1 == '-') {
                              echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                            } else {
                              echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status1;
                            }
                            ?>
                          </td>
                          <td>
                            <?php if ($h->status2 == 'pengajuan') {
                              echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status2;
                            } elseif ($h->status2 == 'ditolak') {
                              echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                            } elseif ($h->status2 == '-') {
                              echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                            } else {
                              echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status2;
                            }
                            ?>
                          </td>
                        </tr>
                      <?php $no++;
                      } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php if ($request_c == 1) { ?>
              <a class="btn btn-large pull-right btn-primary btn-acc" href="#" data-href="<?php echo base_url('dosen-bimbingan/acc' . '/' . $request->id_bimbingan . '/' . $request->nim . '/' . $request->pembimbing) ?>" role="button"><i class="fa fa-check">&nbsp;</i> Terima</a>
              <a class="btn btn-large pull-right btn-danger btn-decl" href="#" data-href="<?php echo base_url('dosen-bimbingan/decl' . '/' . $request->id_bimbingan . '/' . $request->nim . '/' . $request->pembimbing) ?>" role="button"><i class="fa fa-times">&nbsp;</i> Tolak</a>
              <br><br><br>
            <?php } ?>
            <a class="btn btn-default pull-right" href="<?php echo base_url('dosen-bimbingan') ?>" role="button"><i class="fa fa-hand-o-left">&nbsp;</i></i>Back</a><br>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol terima di click
      $(document).on('click', '.btn-acc', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menerima mahasiswa bimbingan?',
          text: "Klik ya jika mahasiswa tersebut merupakan mahasiswa bimbingan anda!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, terima!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
      //event ketika tombol tolak di click
      $(document).on('click', '.btn-decl', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menolak mahasiswa bimbingan?',
          text: "Klik ya jika mahasiswa tersebut bukan merupakan mahasiswa bimbingan anda!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, tolak!'
        }).then((result) => {
          if (result.value) {
            if (result.value) {
              window.location.href = $(this).data('href');
            }
          }
        })
      });
    });
  </script>
</body>

</html>