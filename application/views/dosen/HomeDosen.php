<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php')  ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-dosen') ?>" class="simple-text logo-normal">
          Dosen Pembimbing
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarD.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">supervised_user_circle</i>
                  </div>
                  <p class="card-category">Mahasiswa Bimbingan</p>
                  <h3 class="card-title"><?= @$jml_mahasiswa_bimbingan ?>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="dosen-bimbingan">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">supervisor_account</i>
                  </div>
                  <p class="card-category">Request Mahasiswa</p>
                  <h3 class="card-title"><?= @$jml_req_mhs ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="dosen-bimbingan">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">folder_shared</i>
                  </div>
                  <p class="card-category">Request Ganti Judul</p>
                  <h3 class="card-title"><?= @$jml_req_ganti_judul ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="dosen-bimbingan">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12"></div>
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Announcement Board</h3>
              <p class="card-category">
                Pengumuman terbaru dari Koordinator Skripsi.
              </p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="dropdown-divider"></div>
                  <!-- get data -->
                  <?php
                  $no = 1;
                  foreach ($postingan as $m) {
                  ?>
                    <?php $no++ ?>

                    <div class="alert alert-info">
                      <span>
                        <font size="5px">
                          <strong>
                            <?php echo $m->judul_postingan ?>
                          </strong>
                        </font>
                      </span>
                      <span>
                        <pre>
                        <?php echo ""; ?><br><?php echo "" . $m->isi_postingan; ?>
                      </pre>
                      </span>
                      <span>
                        Link: <a href=<?php echo "http://";
                                      echo $m->link ?>>
                          <font color="blue"><?php echo $m->link ?></font>
                        </a>
                      </span>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <?php $this->load->view('layouts/footer'); ?>
        </footer>
      </div>
    </div>
    <!--   Core JS Files   -->
    <?php $this->load->view('layouts/js'); ?>
</body>

</html>