<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php')  ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-dosen') ?>" class="simple-text logo-normal">
          Dosen Pembimbing
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarD.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Mahasiswa Bimbingan</h3>
            </div>
            <div class="card-body">
              <table class="table table-light" id="dataTable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Pembimbing</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 0;
                  foreach ($bimbingan as $b) {
                  ?>
                    <?php $no++ ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $b->nim ?></td>
                      <td><a href="<?php echo base_url('dosen-bimbingan/show/' . $b->nim); ?>" title="Detail"><?= $b->nama ?></a></td>
                      <td><?= $b->pembimbing ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <br>
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Request Mahasiswa Bimbingan</h3>
            </div>
            <div class="card-body">
              <table class="table table-light" id="dataTable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Pembimbing</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($jml_request == 0) { ?>
                    <tr>
                      <td colspan="5" align="center">Tidak Ada Request</td>
                    </tr>
                  <?php } ?>
                  <?php
                  $no = 0;
                  foreach ($request as $r) {
                  ?>
                    <?php $no++ ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $r->nim ?></td>
                      <td><a href="<?php echo base_url('dosen-bimbingan/show/' . $r->nim); ?>" title="Detail"><?= $r->nama ?></a></td>
                      <td><?= $r->pembimbing ?></td>
                      <td>
                        <a class="btn btn-large btn-block btn-primary btn-acc" href="#" data-href="<?php echo base_url('dosen-bimbingan/acc' . '/' . $r->id_bimbingan . '/' . $r->nim . '/' . $r->pembimbing) ?>" role="button"><i class="fa fa-check">&nbsp;</i> Terima</a>
                        <a class="btn btn-large btn-block btn-danger btn-decl" href="#" data-href="<?php echo base_url('dosen-bimbingan/decl' . '/' . $r->id_bimbingan . '/' . $r->nim . '/' . $r->pembimbing) ?>" role="button"><i class="fa fa-times">&nbsp;</i> Tolak</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <br>
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Request Ganti Judul</h3>
            </div>
            <div class="card-body">
              <table class="table table-light" id="dataTable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Judul Baru</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($jml_ganti_judul == 0) { ?>
                    <tr>
                      <td colspan="5" align="center">Tidak Ada Request</td>
                    </tr>
                  <?php } ?>
                  <?php
                  $no = 0;
                  foreach (@$gantiJudul as $j) {
                  ?>
                    <?php $no++ ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $j->username ?></td>
                      <td><?= $j->nama ?></td>
                      <td><?= $j->judul_histori ?></td>
                      <td><a href="<?php echo base_url('dosen-bimbingan/detailGantiJudul/' . $j->id_histori . '/' . $j->username); ?>" class="btn btn-primary btn-sm btn-round">Detail</a></td>
                    </tr>
                  <?php } ?>
                  <?php
                  $no = 0;
                  foreach (@$gantiJudul2 as $h) {
                  ?>
                    <?php $no++ ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $h->username ?></td>
                      <td><?= $h->nama ?></td>
                      <td><?= $h->judul_histori ?></td>
                      <td><a href="<?php echo base_url('dosen-bimbingan/detailGantiJudul/' . $h->id_histori . '/' . $h->username); ?>" class="btn btn-primary btn-sm btn-round">Detail</a></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol terima di click
      $(document).on('click', '.btn-acc', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menerima mahasiswa bimbingan?',
          text: "Klik ya jika mahasiswa tersebut merupakan mahasiswa bimbingan anda!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, terima!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
      //event ketika tombol tolak di click
      $(document).on('click', '.btn-decl', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menolak mahasiswa bimbingan?',
          text: "Klik ya jika mahasiswa tersebut bukan merupakan mahasiswa bimbingan anda!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, tolak!'
        }).then((result) => {
          if (result.value) {
            if (result.value) {
              window.location.href = $(this).data('href');
            }
          }
        })
      });
    });
  </script>
</body>

</html>