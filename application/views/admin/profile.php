<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
          Admin
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarA.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-5">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Profile</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <?php echo form_open(@$admin ? 'admin-profile/update/' . $admin->username : 'dashboard-admin') ?>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Username</label>
                        <input type="text" required="required" name="username" class="form-control" value="<?= @$admin->username ?>">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Nama Admin</label>
                        <input type="text" required="required" name="nama" class="form-control" value="<?= @$admin->nama ?>">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Password</label>
                        <input type="password" required="required" name="password" class="form-control" value="">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Konfirmasi Password</label>
                        <input type="password" required="required" name="confirm_password" class="form-control" value="">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">Email</label>
                        <input type="email" required="required" name="email" class="form-control" value="<?= @$admin->email ?>">
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Simpan perubahan</button>
                  <!-- <a href="<?php base_url('dashboard-admin'); ?>"><button class="btn btn-danger pull-right">Cancel</button></a> -->
                  <div class="clearfix"></div>
                  <!-- </form> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer') ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
  <script>
    $(document).ready(function() {

      var validation = <?= json_encode(explode('.', strip_tags(validation_errors()))); ?>;
      validation.pop();
      if (validation) {
        $.each(validation, function(idx, msg) {
          $.notify({
            // options
            message: msg
          }, {
            // settings
            type: 'danger'
          });
        });
      }
    });
  </script>
</body>

</html>