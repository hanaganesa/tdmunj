<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layouts/head'); ?>
</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
                    Admin
                </a>
            </div>
            <?php $this->load->view('layouts/sidebarA'); ?>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <?php $this->load->view('layouts/navbar'); ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h3 class="card-title ">Mahasiswa Bimbingan Dosen : <?= @$dosen->nama_dosen ?></h3>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="dataTable">
                                            <thead class=" text-primary">
                                                <th>No</th>
                                                <th>NIM</th>
                                                <th>Nama Mahasiswa</th>
                                                <th>Pembimbing</th>
                                                <th>Status Pengajuan</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 1;
                                                foreach (@$bimbingan as $m) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $no++ ?></td>
                                                        <td><?php echo $m->nim ?></td>
                                                        <td><?php echo $m->nama ?></td>
                                                        <td><?php echo $m->pembimbing ?></td>
                                                        <td><?php echo $m->status_pengajuan ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <?php $this->load->view('layouts/footer'); ?>
            </footer>
        </div>
    </div>
    <!--   Core JS Files   -->
    <?php $this->load->view('layouts/js'); ?>
</body>

</html>