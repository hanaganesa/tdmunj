<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php')  ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
          Admin
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarA.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Announcement Board</h3>
              <p class="card-category">
                Pengumuman terbaru dari Koordinator Skripsi.
              </p>
              <a class="nav-link" href="<?php echo base_url('post/create') ?>">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus-circle">&nbsp;</i> Pengumuman Baru </button>
              </a>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="dropdown-divider"></div>
                  <!-- get data -->
                  <?php
                  $no = 1;
                  foreach ($postingan as $m) {
                  ?>
                    <?php $no++ ?>

                    <div class="alert alert-info">
                      <span>
                        <font size="5px">
                          <strong>
                            <?php echo $m->judul_postingan ?>
                          </strong>
                        </font>
                      </span>
                      <span>
                        <pre>
                        <?php echo ""; ?><br><?php echo "" . $m->isi_postingan; ?>
                      </pre>
                      </span>
                      <span>
                        Link: <a href=<?php echo "http://";
                                      echo $m->link ?>>
                          <font color="blue"><?php echo $m->link ?></font>
                        </a>
                      </span>
                      <span>
                        <a align="right" class="btn btn-primary btn-sm btn-edit" href="<?php echo base_url('post/edit/' . $m->id_postingan) ?>">Edit</a>
                        <a align="right" class="btn btn-danger btn-sm btn-delete" href="#" data-href="<?php echo base_url('post/delete/' . $m->id_postingan) ?>">Hapus</a>

                      </span>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer.php')  ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol hapus di click
      $(document).on('click', '.btn-delete', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menghapus postingan ini?',
          text: "Postingan juga akan tehapus pada kolom pengumuman mahasiswa",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
    });
  </script>
</body>

</html>