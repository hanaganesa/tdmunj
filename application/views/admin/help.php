<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
                    Admin
                </a>
            </div>
            <?php $this->load->view('layouts/sidebarA.php')  ?>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <?php $this->load->view('layouts/navbar.php')  ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="container-fluid">
                        <div class="card">
                            <!--awal-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Berikut adalah manual yang ada di halaman ini</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Cara Membuat Akun Dosen</p>
                                    <p>2. Cara Upload Data Mahasiswa</p>
                                    <p>3. Cara Membuat Pengumuman/Postingan</p>
                                    <p>4. Cara Membuat Akun Admin</p>
                                    <p>5. Cara Melakukan Edit Profile</p>
                                    <p>6. Cara Melihat Data Skripsi</p>
                                    <p>7. Cara Melihat Dosen dan Mahasiswa Bimbingan</p>
                                    <p>Jika ada error silahkan laporkan melalui email ke: hanafitriaganesa@gmail.com (dengan screenshot lebih baik)</p>
                                    <p>Terima kasih</p>
                                </table>
                            </div>
                            <!--akhir awal-->
                            <!--Cara 1-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Membuat Akun Dosen</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "Kelola Akun Dosen" atau "Lihat" di card Dosen. Angka di card menunjukkan jumlah dosen.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/dos1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman kelola akun dosen, admin dapat mengaktif dan nonaktifkan dosen, melakukan edit data dosen, dan melakukan tambah dosen. Untuk
                                        menambah akun dosen maka klik tombol "Tambah Dosen", sedangkan untuk melakukan edit klik tombol "Edit".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/dos2.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Jika memilih tambah dosen, makan akan diarahkan pada halaman Buat Akun Dosen. Isi NIDN dan nama sesuai dengan data dosen yang bersangkutan.
                                        Perlu diberitahukan kepada dosen bahwa password sementara untuk login/sign in adalah NIDN dan dapat diganti demi keamanan akun.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/dos3.png') ?>" alt="" width="100%"><br><br>
                                    <p>4. Jika memilih edit dosen, makan akan diarahkan pada halaman Edit Akun Dosen. Jika melakukan perubahan, klik tombol "Submit" untuk menyimpan
                                        perubahan. Perlu diberitahukan kepada dosen bahwa password sementara untuk login/sign in adalah NIDN dan dapat diganti demi keamanan akun.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/dos4.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 1-->
                            <!--Cara 2-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Upload Data NIM Mahasiswa</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "List Mahasiswa" atau "Lihat" di card Mahasiswa. Angka di card menunjukkan jumlah mahasiswa.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/mhs1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman List Mahasiswa, terdapat list mahasiswa yang dan nim yang sudah di upload. Untuk upload data NIM mahasiswa, klik tombol "Choose file".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/mhs3.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Pastikan file yang diunggah berupa excel dengan format sebagai berikut (nim saja, tidak perlu menyertakan nama atau keterangan apapun).</p>
                                    <img src="<?php echo base_url('assets/manual/admin/mhs4.png') ?>" alt="" width="100%"><br><br>
                                    <p>4. Jika sudah memilih file untuk diunggah, klik tombol "Unggah Data Mahasiswa".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/mhs5.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 2-->
                            <!--Cara 3-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Membuat Pengumuman/Postingan</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "Postingan".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pos1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman postingan, terdapat postingan yang telah ada dan tombol "Pengumuman Baru". Untuk menambah pengumuman baru klik tombol
                                        "Pengumuman Baru", sedangkan untuk postingan yang sudah ada dapat diedit dengan menekan tombol "Edit" atau dihapus dengan menekan tombol
                                        "Hapus".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pos2.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Jika memilih "Pengumuman Baru", makan akan diarahkan pada halaman Buat Postingan. Judul dan isi postingan harus diisi, sedangkan
                                        untuk link dapat diisi atau dikosongkan. Klik post jika sudah selesai.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pos4.png') ?>" alt="" width="100%"><br><br>
                                    <p>4. Jika memilih "Edit", maka akan diarahkan pada halaman Edit Postingan. Judul dan isi postingan harus diisi, sedangkan
                                        untuk link dapat diisi atau dikosongkan. Klik post jika sudah selesai melakukan perubahan.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pos3.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 3-->
                            <!--Cara 4-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Membuat Akun Admin</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "Kelola Admin" atau "Lihat" di card Admin. Angka di card menunjukkan jumlah admin.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/adm1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman Kelola Admin, admin dapat melakukan edit, hapus data admin lain, dan melakukan tambah admin. Untuk
                                        menambah akun admin maka klik tombol "Tambah Admin", sedangkan untuk melakukan edit klik tombol "Edit".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/adm2.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Jika memilih tambah admin, makan akan diarahkan pada halaman Buat Akun Admin. Username dan nama admin harus diisi. Perlu diberitahukan
                                        kepada admin yang bersangkutan bahwa password sementara untuk login/sign in adalah username dan dapat diganti demi keamanan akun.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/adm3.png') ?>" alt="" width="100%"><br><br>
                                    <p>4. Jika memilih edit admin, makan akan diarahkan pada halaman Edit Akun Admin. Jika melakukan perubahan, klik tombol "Submit" untuk menyimpan
                                        perubahan. Perlu diberitahukan kepada admin yang bersangkutan bahwa password sementara untuk login/sign in adalah username dan dapat diganti demi
                                        keamanan akun.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/adm4.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 4-->
                            <!--Cara 5-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Melakukan Edit Profile</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik icon di pojok kanan atas seperti gambar di bawah.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pro1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Lalu pilih menu "Profile".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pro2.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Klik "Submit" jika ingin menyimpan perubahan.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/pro3.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 5-->
                            <!--Cara 6-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Melihat Data Skripsi</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "List Skripsi" atau "Lihat" di card Skripsi. Angka di card menunjukkan jumlah skripsi aktif.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/skr1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman List Skripsi, terdapat list skripsi mahasiswa. Untuk melihat detail skripsi, klik pada judul skripsi.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/skr2.png') ?>" alt="" width="100%"><br><br>
                                    <p>3. Halaman detail skripsi akan menunjukkan detail skripsi mahasiswa. Untuk kembali dapat menekan tombol "Back" atau menu di sebelah
                                        kiri.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/skr3.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 6-->
                            <!--Cara 7-->
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">Cara Melihat Dosen dan Mahasiswa Bimbingan</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-light">
                                    <p>1. Klik menu "List Bimbingan".</p>
                                    <img src="<?php echo base_url('assets/manual/admin/bim1.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Di halaman bimbingan, terdapat daftar dosen pembimbing. Pada Tabel sebelah kanan, terdapat jumlah mahasiswa bimbingan yang dimiliki oleh dosen tersebut.
                                        Untuk melihat daftar mahasiswa bimbingan, klik tombol "Lihat Mahasiswa Bimbingan".
                                    </p>
                                    <img src="<?php echo base_url('assets/manual/admin/bim2.png') ?>" alt="" width="100%"><br><br>
                                    <p>2. Pada halaman mahasiswa bimbingan, terdapat list mahasiswa, informasi pembimbing 1 atau 2, dan status pengajuan pembimbing.</p>
                                    <img src="<?php echo base_url('assets/manual/admin/bim3.png') ?>" alt="" width="100%"><br><br>
                                </table>
                            </div>
                            <!--akhir 7-->
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <?php $this->load->view('layouts/footer') ?>
            </footer>
        </div>
    </div>
    <!--   Core JS Files   -->
    <?php $this->load->view('layouts/js.php')  ?>
</body>

</html>