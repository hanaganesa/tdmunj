<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head'); ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
          Admin
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarA'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Akun Dosen</h4>
                  <p class="card-category">Kelola akun dosen pembimbing.</p>
                  <a class="nav-link" href="<?php echo base_url('dosen/create') ?>">
                    <button type="button" class="btn btn-primary"><i class="fa fa-plus-circle">&nbsp;</i> Tambah Dosen </button>
                  </a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="dataTable">
                      <thead class=" text-primary">
                        <th>No</th>
                        <th>NIDN</th>
                        <th>Nama Dosen</th>
                        <th>Status</th>
                        <th width="170">Aksi</th>
                      </thead>
                      <tbody>
                        <?php
                        $no = 1;
                        foreach ($dosen as $m) {
                        ?>
                          <tr>
                            <td><?php echo $no++ ?></td>
                            <td class="text-primary"><?php echo $m->username ?></td>
                            <td><?php echo $m->nama_dosen ?></td>
                            <td>
                              <?= $m->status == "aktif" ? '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $m->status : '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $m->status ?>
                            </td>
                            <td>
                              <?= $m->status == "aktif" ? '<a href="#" data-href="' . base_url('dosen/updateStatus') . '/' . $m->username . '/' . $m->status . '" class="btn btn-sm btn-danger btn-nonaktif"><i class="fa fa-times">&nbsp;</i> Nonaktifkan</a>' : '<a href="#" data-href="' . base_url('dosen/updateStatus') . '/' . $m->username . '/' . $m->status . '" class="btn btn-sm btn-success btn-aktif" ><i class="fa fa-check">&nbsp;</i> Aktifkan</a>' ?>
                              <a href="<?php echo base_url('dosen/edit/' . $m->username) ?>" data-href="" title="Edit" class="btn btn-sm btn-primary btn-edit"><i class="fa fa-edit">&nbsp;</i> Edit</a>
                              <!-- <a href="#" data-href="'.base_url('dosen/delete').'/'.$m->username'" title="Hapus" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-trash"></i></a> -->

                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol nonaktif di click
      $(document).on('click', '.btn-nonaktif', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan me-nonaktifkan akun ini?',
          text: "Akun yang telah dinonaktifkan tidak dapat digunakan!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, nonaktifkan!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
      //event ketika tombol aktif di click
      $(document).on('click', '.btn-aktif', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan mengaktifkan akun ini?',
          text: "Akun yang telah diaktifkan dapat digunakan!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, aktifkan!'
        }).then((result) => {
          if (result.value) {
            if (result.value) {
              window.location.href = $(this).data('href');
            }
          }
        })
      });
    });
  </script>
</body>

</html>