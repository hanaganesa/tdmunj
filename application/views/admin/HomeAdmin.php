<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php')  ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url ('dashboard-admin') ?>" class="simple-text logo-normal">
          Admin
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarA.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">supervised_user_circle</i>
                  </div>
                  <p class="card-category">Mahasiswa</p>
                  <h3 class="card-title"><?= @$jml_mahasiswa ?>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="mahasiswa">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">supervisor_account</i>
                  </div>
                  <p class="card-category">Dosen</p>
                  <h3 class="card-title"><?= @$jml_dosen ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="dosen">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <p class="card-category">Admin</p>
                  <h3 class="card-title"><?= @$jml_admin ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="admin">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">folder_shared</i>
                  </div>
                  <p class="card-category">Skripsi</p>
                  <h3 class="card-title"><?= @$jml_judul_aktif ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-success">remove_red_eye</i>
                    <a href="admin-skripsi">Lihat..</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="card card-stats">
                <div class="card-body">
                  <canvas id="pieChart"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer.php')  ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
  <script>
    $(document).ready(function () {
      var canvas = document.getElementById("pieChart");
      var ctx = canvas.getContext('2d');

      // Global Options:
      Chart.defaults.global.defaultFontColor = 'black';
      Chart.defaults.global.defaultFontSize = 16;

      var dataa = <?= $data ?>;
      var dataArr = [];
      $.each(dataa, function (i, v) { 
         dataArr[i] = parseInt(v);
      });
      var data = {
          labels: <?= $label ?>,
            datasets: [
              {
                  fill: true,
                  backgroundColor: [
                      '#fb8c00',
                      '#43a047','#8e24aa'],
                  data: dataArr,
      // Notice the borderColor 
                  borderColor:	['#fb8c00', '#43a047','#8e24aa'],
                  borderWidth: [2,2]
              }
          ]
      };

      // Notice the rotation from the documentation.

      var options = {
              title: {
                        display: true,
                        text: 'Judul Skripsi Per Kategori',
                        position: 'top'
                    },
              // rotation: -0.7 * Math.PI
      };

      console.log(dataArr);
      // Chart declaration:
      var myBarChart = new Chart(ctx, {
          type: 'doughnut',
          data: data,
          options: options
      });
    });
  </script>
</body>

</html>
