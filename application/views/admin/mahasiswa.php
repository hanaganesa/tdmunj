<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('layouts/head'); ?>
</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="<?php echo base_url('dashboard-admin') ?>" class="simple-text logo-normal">
                    Admin
                </a>
            </div>
            <?php $this->load->view('layouts/sidebarA'); ?>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <?php $this->load->view('layouts/navbar'); ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title ">Akun Mahasiswa Aktif</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table border="0" align="center">
                                            <tbody><br>
                                                <tr></tr>
                                                <td align="center"><img src="<?php echo base_url('assets/foto/') . @$mahasiswa->foto ?>" width="200" class="img-responsive" alt="Image"></td>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <td align="center">
                                                    <h4><?php echo $mahasiswa->username ?></h4>
                                                </td>
                                                <tr></tr>
                                                <td align="center">
                                                    <h4><?php echo $mahasiswa->nama ?></h4>
                                                </td>
                                                <tr></tr>
                                                <td align="center">
                                                    <h4><?php echo $mahasiswa->email ?></h4>
                                                </td>
                                                <tr></tr>
                                            </tbody>
                                        </table><br>
                                        <table class="table">
                                            <thead>
                                                <th>Judul Sempro</th>
                                                <th>Kategori</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach (@$skripsi as $m) {
                                                ?>
                                                    <tr>
                                                        <td><a href="<?= base_url('admin-skripsi/detail/' . $mahasiswa->username . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $m->judul ?></a></td>
                                                        <td><?php echo $m->kategori ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php if ($histori1 != NULL) { ?>
                                            <table class="table">
                                                <thead>
                                                    <th>Skripsi Aktif</th>
                                                    <th>Kategori</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><a href="<?= base_url('admin-skripsi/detail/' . $histori1->id_histori . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $histori1->judul_histori ?></a></td>
                                                        <td><?php echo $histori1->kategori_histori ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <td>
                                            <h3>Histori Skripsi</h3>
                                        </td>
                                        <table class="table" id="dataTable">
                                            <thead>
                                                <th>No.</th>
                                                <th>Tanggal & Waktu</th>
                                                <th>Judul</th>
                                                <th>Status</th>
                                                <th>Status Pembimbing 1</th>
                                                <th>Status Pembimbing 2</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 1;
                                                foreach (@$histori as $h) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $no . '.'; ?></td>
                                                        <td><?php echo $h->tanggal_histori ?></td>
                                                        <?php if ($pembimbing == 2) { ?>
                                                            <td><a href="<?= base_url('admin-skripsi/detail/' . $h->id_histori . '/' . $m->id_skripsi) ?>" title="Detail"><?php echo $h->judul_histori ?></a></td>
                                                        <?php } elseif ($pembimbing != 2) { ?>
                                                            <td><?php echo $h->judul_histori ?></td>
                                                        <?php } ?>
                                                        <td><?php echo $h->status_histori ?></td>
                                                        <td>
                                                            <?php if ($h->status1 == 'pengajuan') {
                                                                echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status1;
                                                            } elseif ($h->status1 == 'ditolak') {
                                                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                                                            } elseif ($h->status1 == '-') {
                                                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                                                            } else {
                                                                echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status1;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($h->status2 == 'pengajuan') {
                                                                echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status2;
                                                            } elseif ($h->status2 == 'ditolak') {
                                                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                                                            } elseif ($h->status2 == '-') {
                                                                echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                                                            } else {
                                                                echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status2;
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php $no++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?= $m->status == "aktif" ? '<a href="#" data-href="' . base_url('mahasiswa/updateStatus') . '/' . $m->username . '/' . $m->status . '" class="btn btn-sm btn-danger btn-nonaktif pull-right"><i class="fa fa-times">&nbsp;</i> Nonaktifkan</a>' : '<a href="#" data-href="' . base_url('mahasiswa/updateStatus') . '/' . $m->username . '/' . $m->status . '" class="btn btn-sm btn-success btn-aktif pull-right" ><i class="fa fa-check">&nbsp;</i> Aktifkan</a>' ?>
                            <a href="#" data-href="<?= base_url('mahasiswa/delete') . '/' . $m->username ?>" class="btn btn-sm btn-warning btn-hapus pull-right"><i class="fa fa-trash">&nbsp;</i> Hapus</a></td>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <?php $this->load->view('layouts/footer'); ?>
            </footer>
        </div>
    </div>
    <!--   Core JS Files   -->
    <?php $this->load->view('layouts/js'); ?>
    <script>
        $(document).ready(function() {
            //event ketika tombol nonaktif di click
            $(document).on('click', '.btn-nonaktif', function() {
                Swal.fire({
                    title: 'Apakah anda yakin akan me-nonaktifkan akun ini?',
                    text: "Akun yang telah dinonaktifkan tidak dapat digunakan!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, nonaktifkan!'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = $(this).data('href');
                    }
                })
            });
            //event ketika tombol aktif di click
            $(document).on('click', '.btn-aktif', function() {
                Swal.fire({
                    title: 'Apakah anda yakin akan mengaktifkan akun ini?',
                    text: "Akun yang telah diaktifkan dapat digunakan!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, aktifkan!'
                }).then((result) => {
                    if (result.value) {
                        if (result.value) {
                            window.location.href = $(this).data('href');
                        }
                    }
                })
            });
            //event ketika tombol aktif di click
            $(document).on('click', '.btn-hapus', function() {
                Swal.fire({
                    title: 'Apakah anda yakin akan menghapus akun ini?',
                    text: "Akun yang telah dihapus tidak dapat dikembalikan!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus!'
                }).then((result) => {
                    if (result.value) {
                        if (result.value) {
                            window.location.href = $(this).data('href');
                        }
                    }
                })
            });
        });
    </script>
</body>

</html>