<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('index.php/C_Home/ToHomeMahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Ganti Judul</h3>
              <p class="card-category">
                Silahkan isi data dengan lengkap.
              </p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <!--  <div class="dropdown-divider"></div> garis-->
                  <!-- get data -->
                  <div class="card-body">
                    <?= form_open('mahasiswa-skripsi/store/' . $histori1->id_skripsi . '/' . $histori1->id_histori) ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Judul</label>
                          <input type="text" name="judul_histori" required="required" class="form-control" value="<?= @$histori1->judul_histori; ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Abstrak</label><br>
                          <textarea type="text" name="abstrak_histori" required="required" class="form-control" rows="5" cols="36" value=""><?= @$histori1->abstrak_histori; ?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kategori</label>
                          <div class="dropdown">
                            <select class="btn btn-primary dropdown-header select2" name="kategori_histori" id="kategori"><span class="caret"></span>
                              <?php if ($histori1->kategori_histori == 'pendidikan') { ?>
                                <option value="pendidikan" selected>Pendidikan</option>
                                <option value="linguistik">Linguistik</option>
                                <option value="komprehensif">Komprehensif</option>
                              <?php } elseif ($histori1->kategori_histori == 'linguistik') { ?>
                                <option value="pendidikan">Pendidikan</option>
                                <option value="linguistik" selected>Linguistik</option>
                                <option value="komprehensif">Komprehensif</option>
                              <?php } else { ?>
                                <option value="pendidikan">Pendidikan</option>
                                <option value="linguistik">Linguistik</option>
                                <option value="komprehensif" selected>Komprehensif</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Alasan Ganti Judul</label><br>
                          <textarea type="text" name="alasan" required="required" class="form-control" rows="3" cols="36" value="" title="Tuliskan alasan kenapa ingin mengganti judul"></textarea>
                        </div>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary pull-right">Ajukan Ganti Judul</button>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer.php')  ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>

  <script>
    $(document).ready(function() {

      var validation = <?= json_encode(explode('.', strip_tags(validation_errors()))); ?>;
      validation.pop();
      if (validation) {
        $.each(validation, function(idx, msg) {
          $.notify({
            // options
            message: msg
          }, {
            // settings
            type: 'danger'
          });
        });
      }
      var success = '<?= @$_SESSION['success'] ?>';
      if (success) {
        $.notify({
          // options
          message: success
        }, {
          // settings
          type: 'success'
        });
      }
      var error = '<?= @$_SESSION['error'] ?>';
      if (error) {
        $.notify({
          // options
          message: error
        }, {
          // settings
          type: 'danger'
        });
      }
    });
  </script>
</body>

</html>