<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head'); ?>
  <style>
    .tb-kemiripan td div {
      height: 150px;
      overflow: scroll;
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="container-fluid">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Ukur Kesamaan Skripsi</h4>
                <p class="card-category">Silahkan input judul dan abstrak skripsi anda untuk mengetahui berapa persen kecocokan skripsi anda dengan skripsi mahasiswa lain</p>
              </div>
              <?php echo form_open('mahasiswa/SkripsiController/kesamaanUkur') ?>
              <div class="card-body">
                <table class="table table-light">
                  <tbody>
                    <tr>
                      <td>Judul Skripsi</td>
                      <td>
                        <textarea name="judul" id="inputjudul" class="form-control" rows="3" required="required" placeholder="Ketik judul disini"><?php if (@$judulSkripsi != NULL) {
                                                                                                                                                    echo $judulSkripsi;
                                                                                                                                                  } ?></textarea>
                      </td>
                    </tr>
                    <tr>
                      <td>Abstrak</td>
                      <td>
                        <textarea name="abstrak" id="inputabstrak" class="form-control" rows="7" required="required" placeholder="Ketik abstrak disini"><?php if (@$abstrak != NULL) {
                                                                                                                                                          echo $abstrak;
                                                                                                                                                        } ?></textarea>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <!-- <button type="button" name="submit" id="submit" 
                class="form-submit btn btn-large btn-primary pull-right">Ukur</button> -->
                <div class="form-group form-button">
                  <input type="submit" name="submit" id="submit" class="form-submit btn btn-large btn-primary pull-right" value="Ukur" />
                </div>
              </div>
              <br><br>
              <br><br>
              <div class="card-header card-header-success">
                <h3 class="card-title">Kemiripan Skripsi yang Diajukan dengan Skripsi Mahasiswa Lain</h3>
              </div>
              <div class="card-body">
                <table class="table table-hover tb-kemiripan">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Kemiripan</th>
                      <th>NIM</th>
                      <th>Nama Mahasiswa</th>
                      <th>Judul Skripsi</th>
                      <th>Abstrak</th>
                      <th>Kategori</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if (@$skripsiMhsLain != NULL) {
                      foreach (@$skripsiMhsLain as $key => $v) {
                        if ($v->percent >= 30) { ?>
                          <tr>
                            <td style="vertical-align: top"><?= $key + 1 ?></td>
                            <td style="vertical-align: top">
                              <?php
                              echo 'Kemiripan Judul: <font color="red">' . $v->percent . '%' . '</font><br>';
                              echo 'Kemiripan Abstrak: <font color="red">' . $v->percent2 . '%' . '</font>';
                              ?>
                            </td>
                            <td style="vertical-align: top"><?= $v->username ?></td>
                            <td style="vertical-align: top"><?= $v->nama ?></td>
                            <td style="vertical-align: top">
                              <div><?= $v->judul ?></div>
                            </td>
                            <td style="vertical-align: top">
                              <div><?= $v->abstrak ?></div>
                            </td>
                            <td style="vertical-align: top"><?= $v->kategori ?></td>
                          </tr>
                        <?php
                        } else if ($v->percent2 >= 30) { ?>
                          <tr>
                            <td style="vertical-align: top"><?= $key + 1 ?></td>
                            <td style="vertical-align: top">
                              <?php
                              echo 'Kemiripan Judul: <font color="red">' . $v->percent . '%' . '</font><br>';
                              echo 'Kemiripan Abstrak: <font color="red">' . $v->percent2 . '%' . '</font>';
                              ?>
                            </td>
                            <td style="vertical-align: top"><?= $v->username ?></td>
                            <td style="vertical-align: top"><?= $v->nama ?></td>
                            <td style="vertical-align: top">
                              <div><?= $v->judul ?></div>
                            </td>
                            <td style="vertical-align: top">
                              <div><?= $v->abstrak ?></div>
                            </td>
                            <td style="vertical-align: top"><?= $v->kategori ?></td>
                          </tr>
                      <?php
                        }
                      }
                    } else { ?>
                      <tr>
                        <td colspan="7" align="center">Tidak Ada Data</td>
                      </tr>
                    <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/material-dashboard-master/assets/js/plugins/bootstrap-notify.js') ?>"></script>
</body>

</html>