<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Data Skripsi</h4>
                </div>
                <?php if ($pengajuan == 0 && $pembimbing == 2) { ?>
                  <a class="nav-link" href="<?php echo base_url('mahasiswa-skripsi/create') ?>">
                    <button type="button" class="btn btn-primary"><i class="fa fa-file">&nbsp;</i> Ganti Judul</button>
                  </a>
                <?php } elseif ($pengajuan != 0) { ?>
                  <a class="nav-link" href="#">
                    <button type="button" class="btn btn-disabled btn-pengajuan"><i class="fa fa-file">&nbsp;</i> Ganti Judul</button>
                  </a>
                <?php } elseif ($pembimbing != 2) { ?>
                  <a class="nav-link" href="#">
                    <button type="button" class="btn btn-disabled btn-dosen"><i class="fa fa-file">&nbsp;</i> Ganti Judul</button>
                  </a>
                <?php } ?>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <th>Judul Sempro</th>
                        <th>Kategori</th>
                        <th width="5%">Aksi</th>
                      </thead>
                      <tbody>
                        <?php
                        foreach (@$skripsi as $m) {
                        ?>
                          <tr>
                            <td><a href="<?= ('mahasiswa-skripsi/detailSkripsi/' . $m->id_skripsi) ?>" title="Detail"><?php echo $m->judul ?></a></td>
                            <td><?php echo $m->kategori ?></td>
                            <td width="5%">
                              <a href="<?php echo ('mahasiswa-skripsi/edit/' . $m->id_skripsi) ?>" title="Edit" class="btn btn-sm btn-primary btn-edit"><i class="fa fa-edit"></i></a>
                              <!-- <a href="#" data-href="'.base_url('mahasiswa-skripsi/delete').'/'.$m->username'" title="Hapus" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-trash"></i></a> -->
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                      <?php if ($pembimbing == 2) { ?>
                        <thead>
                          <th>Skripsi Aktif</th>
                          <th>Kategori</th>
                          <th width="5%">Aksi</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td><a href="<?= ('mahasiswa-skripsi/detailHistori/' . @$histori1->id_histori) ?>" title="Detail"><?php echo @$histori1->judul_histori ?></a></td>
                            <td><?php echo @$histori1->kategori_histori ?></td>
                            <td width="5%">
                              <a href="<?php echo ('mahasiswa-skripsi/editH/' . @$histori1->id_histori) ?>" title="Edit" class="btn btn-sm btn-primary btn-edit"><i class="fa fa-edit"></i></a>
                              <!-- <a href="#" data-href="'.base_url('mahasiswa-skripsi/delete').'/'.$m->username'" title="Hapus" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-trash"></i></a> -->
                            </td>
                          </tr>
                        </tbody>
                      <?php } ?>
                      <td>
                        <h3>Histori Skripsi</h3>
                      </td>
                      <table class="table" id="dataTable">
                        <thead>
                          <th>No.</th>
                          <th>Tanggal & Waktu</th>
                          <th>Judul</th>
                          <th>Status</th>
                          <th>Status Pembimbing 1</th>
                          <th>Status Pembimbing 2</th>
                          <?php if ($pembimbing == 2) { ?>
                            <th>Aksi</th>
                          <?php } ?>
                        </thead>
                        <tbody>
                          <?php
                          $no = 1;
                          foreach (@$histori as $h) {
                          ?>
                            <tr>
                              <td><?php echo $no . '.'; ?></td>
                              <td><?php echo $h->tanggal_histori ?></td>
                              <?php if ($pembimbing == 2) { ?>
                                <td><a href="<?= ('mahasiswa-skripsi/detailHistori/' . $h->id_histori) ?>" title="Detail"><?php echo $h->judul_histori ?></a></td>
                              <?php } elseif ($pembimbing != 2) { ?>
                                <td><?php echo $h->judul_histori ?></td>
                              <?php } ?>
                              <td><?php echo $h->status_histori ?></td>
                              <td>
                                <?php if ($h->status1 == 'pengajuan') {
                                  echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status1;
                                } elseif ($h->status1 == 'ditolak') {
                                  echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                                } elseif ($h->status1 == '-') {
                                  echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status1;
                                } else {
                                  echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status1;
                                }
                                ?>
                              </td>
                              <td>
                                <?php if ($h->status2 == 'pengajuan') {
                                  echo '<i class="badge badge-warning">&nbsp;</i>&nbsp;' . $h->status2;
                                } elseif ($h->status2 == 'ditolak') {
                                  echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                                } elseif ($h->status2 == '-') {
                                  echo '<i class="badge badge-danger">&nbsp;</i>&nbsp;' . $h->status2;
                                } else {
                                  echo '<i class="badge badge-success">&nbsp;</i>&nbsp;' . $h->status2;
                                }
                                ?>
                              </td>
                              <?php if ($pembimbing == 2) { ?>
                                <td><?php if ($h->status_histori == 'pengajuan' && $h->visibility == '0') { ?>
                                    <a href="#" data-href="<?= base_url('mahasiswa-skripsi/delete/' . $h->id_histori) ?>" title="Batal" class="btn btn-sm btn-danger btn-cancel"><i class="fa fa-times"></i></a>
                                    <a href="<?= base_url('mahasiswa-skripsi/editH/' . $h->id_histori) ?>" title="Edit" class="btn btn-sm btn-primary btn-edit"><i class="fa fa-edit"></i></a>
                                  <?php } ?>
                                </td>
                              <?php } ?>
                            </tr>
                          <?php $no++;
                          } ?>
                        </tbody>
                      </table>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol hapus di click
      $(document).on('click', '.btn-cancel', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan membatalkan pengajuan ini?',
          text: "Data akan terhapus di request ganti judul pada dosen pembimbing",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, batalkan!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
      $(document).on('click', '.btn-hapus', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menghapus data ini?',
          text: "Data yang terhapus tidak dapat dikembalikan",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
      $(document).on('click', '.btn-dosen', function() {
        Swal.fire({
          title: 'Pengajuan ganti judul tidak dapat dilakukan!',
          text: "Pastikan anda sudah memiliki dosen pembimbing 1 dan 2",
          icon: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tutup'
        })
      });
      $(document).on('click', '.btn-pengajuan', function() {
        Swal.fire({
          title: 'Pengajuan ganti judul tidak dapat dilakukan!',
          text: "Pastikan anda tidak sedang melakukan pengajuan judul",
          icon: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tutup'
        })
      });
    });
  </script>
</body>

</html>