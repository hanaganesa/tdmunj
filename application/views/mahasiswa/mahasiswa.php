<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-5">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Profile</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <?php echo form_open_multipart(@$mahasiswa ? 'mahasiswa-profile/update/' . $mahasiswa->username : 'dashboard-mahasiswa') ?>
                  <div class="row">
                    <div class="col-md-6">
                      <center>
                        <img src="<?php echo base_url('assets/foto/') . @$mahasiswa->foto ?>" width="200" class="img-responsive">
                        <input type="file" name="foto" id="foto" class="form-control" title="Klik untuk ubah foto">
                      </center>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="bmd-label-floating">NIM</label>
                        <input type="number" name="username" required="required" class="form-control" value="<?= @$mahasiswa->username ?>">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Nama Mahasiswa</label>
                        <input type="text" name="nama" required="required" class="form-control" value="<?= @$mahasiswa->nama ?>">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Password</label>
                        <input type="password" name="password" required="required" class="form-control" value="">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Konfirmasi Password</label>
                        <input type="password" name="confirm_password" required="required" class="form-control" value="">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Email</label>
                        <input type="email" name="email" required="required" class="form-control" value="<?= @$mahasiswa->email ?>">
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Simpan Perubahan</button>
                  <!-- <a href="<?php base_url('dashboard-mahasiswa'); ?>"><button class="btn btn-danger pull-right">Cancel</button></a> -->
                  <div class="clearfix"></div>
                  <?php form_close(); ?>
                  <!-- </form> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer') ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
  <script>
    $(document).ready(function() {

      var validation = <?= json_encode(explode('.', strip_tags(validation_errors()))); ?>;
      validation.pop();
      if (validation) {
        $.each(validation, function(idx, msg) {
          $.notify({
            // options
            message: msg
          }, {
            // settings
            type: 'danger'
          });
        });
      }
    });
  </script>
</body>

</html>