<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Edit Data</h3>
              <p class="card-category">
                Silahkan isi data dengan lengkap.
              </p>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <!--  <div class="dropdown-divider"></div> garis-->
                  <!-- get data -->
                  <div class="card-body">
                    <?php echo form_open('mahasiswa-skripsi/updateH/' . $histori->id_histori) ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Judul</label>
                          <input type="text" name="judul_histori" class="form-control" value="<?= @$histori->judul_histori ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Abstrak</label><br>
                          <textarea type="text" name="abstrak_histori" rows="5" cols="36" class="form-control"><?= @$histori->abstrak_histori ?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kategori</label>
                          <div class="dropdown">
                            <select class="dropdown-header select2" name="kategori_histori" id="kategori"><span class="caret"></span>
                              <?php if ($histori->kategori_histori == 'pendidikan') { ?>
                                <option value="pendidikan" selected>Pendidikan</option>
                                <option value="linguistik">Linguistik</option>
                                <option value="komprehensif">Komprehensif</option>
                              <?php } elseif ($histori->kategori_histori == 'linguistik') { ?>
                                <option value="pendidikan">Pendidikan</option>
                                <option value="linguistik" selected>Linguistik</option>
                                <option value="komprehensif">Komprehensif</option>
                              <?php } else { ?>
                                <option value="pendidikan">Pendidikan</option>
                                <option value="linguistik">Linguistik</option>
                                <option value="komprehensif" selected>Komprehensif</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Alasan</label><br>
                          <textarea type="text" name="alasan" rows="3" cols="36" class="form-control"><?= @$histori->alasan ?></textarea>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <a class="btn btn-large btn-default pull-right" href="<?= base_url('mahasiswa-skripsi') ?>" role="button">Cancel</a>
                    <div class="clearfix"></div>
                    <!-- </form> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>


</body>

</html>