<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Detail Skripsi</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="dataTable">
                      <?php
                      $no = 1;
                      foreach (@$skripsi as $m) {
                      ?>
                        <tbody>
                          <tr></tr>
                          <th>Judul</th>
                          <td class="text-primary" align="justify"><?php echo $m->judul ?></td>
                          <tr></tr>
                          <th>Abstrak</th>
                          <td align="justify"><?php echo $m->abstrak ?></td>
                          <tr></tr>
                          <th>Status</th>
                          <td align="left"><?php echo $m->status ?></td>
                          <tr></tr>
                          <th>Kategori</th>
                          <td align="left"><?php echo $m->kategori ?></td>
                          <tr></tr>
                          <th width="170">Aksi</th>
                          <td>
                            <a href="<?php echo base_url('mahasiswa-skripsi/edit/' . $m->id_skripsi) ?>" title="Edit" class="btn btn-sm btn-primary btn-edit"><i class="fa fa-edit"></i></a>
                            <!-- <a href="#" data-href="'.base_url('mahasiswa-skripsi/delete').'/'.$m->username'" title="Hapus" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-trash"></i></a> -->
                          </td>
                        </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <a class="btn btn-default pull-right" href="<?php echo base_url('mahasiswa-skripsi') ?>" role="button"><i class="fa fa-hand-o-left">&nbsp;</i></i>Back</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol hapus di click
      $(document).on('click', '.btn-cancel', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan membatalkan pengajuan ini?',
          text: "Data akan terhapus di request ganti judul pada dosen pembimbing",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
    });
  </script>
</body>

</html>