<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

 <!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url ('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM.php')  ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar.php')  ?>
      <!-- End Navbar -->
      <div class="content">
      <div class="container-fluid">
        <div class="container-fluid">
            <div class="card">
                <!--awal-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Berikut adalah manual yang ada di halaman ini</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Cara Submit Dosen Pembimbing</p>
                        <p>2. Cara Mengajukan Pergantian Judul</p>
                        <p>3. Ukur Kesamaan Skripsi</p>
                        <p>4. Cara Melakukan Edit Profile</p>
                        <p>Jika ada error silahkan laporkan melalui email ke: hanafitriaganesa@gmail.com (dengan screenshot lebih baik)</p>
                        <p>Terima kasih</p>
                    </table>
                </div>
                <!--akhir awal-->
                <!--pbb-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Submit Dosen Pembimbing</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik menu "Pembimbing"</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pbb1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Klik tombol "Pilih Pembimbing 1" atau "Pilih Pembimbing 2."</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pbb2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Cari dosen pembimbing anda dan klik "Simpan".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pbb3.png') ?>" alt="" width="100%"><br><br>
                        <p>4. Pilih kedua pembimbing anda. Sebelum pembimbing melakukan acc, status pembimbing yang anda pilih adalah "pengajuan".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pbb4.png') ?>" alt="" width="100%"><br><br>
                        <p>5. Setelah pembimbing melakukan acc, status pembimbing yang anda pilih akan berubah menjadi "diterima".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pbb5.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir pbb-->
                <!--ganti judul-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Mengajukan Pergantian Judul</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Sebelum melakukan pengajuan, pastikan anda telah memiliki pembimbing 1 & 2.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Jika belum, saat menekan tombol "Ganti Judul" maka akan muncul instruksi seperti gambar dibawah.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Jika sudah, maka klik tombol "Ganti Judul".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj3.png') ?>" alt="" width="100%"><br><br>
                        <p>4. Anda akan diarahkan untuk mengisi data skripsi yang akan diajukan, mohon isi secara lengkap lalu klik "Ajukan Ganti Judul".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj4.png') ?>" alt="" width="100%"><br><br>
                        <p>5. Pengajuan ganti judul akan tersimpan. Sebelum dosen melakukan acc maka status akan tetap "pengajuan".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj5.png') ?>" alt="" width="100%"><br><br>
                        <p>6. Jika masih ada pengajuan ganti judul, maka harus menunggu proses pengajuan selesai sebelum melakukan pengajuan lainnya.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj6.png') ?>" alt="" width="100%"><br><br>
                        <p>7. Saat menekan tombol "Ganti Judul" maka instruksi seperti gambar dibawah akan muncul jika masih ada pengajuan yang belum selesai.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj7.png') ?>" alt="" width="100%"><br><br>
                        <p>8. Setelah dosen melakukan acc, status akan berubah menjadi hijau jika diterima atau merah jika ditolak.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/gj8.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir ganti judul-->
                <!--ukur-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Ukur Kesamaan Skripsi</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik "Ukur Kesamaan Skrpisi".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/ukur1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Isi judul dan abstrak skripsi, lalu klik "Ukur".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/ukur2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Akan muncul skripsi yang memiliki kemiripan diatas 30%.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/ukur3.png') ?>" alt="" width="100%"><br><br>
                        <p>4. Jika tidak ada skripsi yang mirip maka tidak akan keluar data apapun.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/ukur4.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir ukur-->
                <!--pro-->
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Cara Melakukan Edit Profile</h4>
                </div>
                <div class="card-body">
                    <table class="table table-light">
                        <p>1. Klik icon di pojok kanan atas seperti gambar di bawah.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pro1.png') ?>" alt="" width="100%"><br><br>
                        <p>2. Lalu pilih menu "Profile".</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pro2.png') ?>" alt="" width="100%"><br><br>
                        <p>3. Anda dapat melakukan edit profile. Klik "Submit" jika ingin menyimpan perubahan.</p>
                        <img src="<?php echo base_url('assets/manual/mahasiswa/pro3.png') ?>" alt="" width="100%"><br><br>
                    </table>
                </div>
                <!--akhir pro-->
            </div>
        </div>
    </div>
    </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer') ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js.php')  ?>
</body>

</html>
