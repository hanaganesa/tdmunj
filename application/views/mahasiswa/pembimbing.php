<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="card col-md-12">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Dosen Pembimbing</h3>
            </div>
            <div class="card-body">
              <table class="table table-light">
                <thead>
                  <tr>
                    <th>Pembimbing 1</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($jml_pbb1 == 0) { ?>
                    <tr>
                      <td colspan="3">
                        <h4 align="center">Tidak ada data</h4>
                      </td>
                      <td>
                        <div>
                          <a class="btn btn-large btn-primary pull-right" href="pembimbing/create" role="button">Pilih Pembimbing 1</a>
                        </div>
                      </td>
                    </tr>
                  <?php } else { ?>
                    <?php foreach ($pbb1 as $key => $value) { ?>
                      <tr>
                        <td><?= $value->nama_dosen ?></td>
                        <td><?= $value->status_pengajuan ?></td>
                        <td></td>
                        <td>
                          <a class="btn btn-large btn-danger btn-delete pull-right" href="#" data-href="<?php echo base_url('pembimbing/delete/' . $value->id_bimbingan) ?>" role="button">
                            <?php if ($value->status_pengajuan == "pengajuan") { ?>
                              Cancel
                            <?php } else { ?>
                              Delete
                            <?php } ?></a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>
                </tbody>
                <thead>
                  <tr>
                    <th>Pembimbing 2</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($jml_pbb2 == 0) { ?>
                    <tr>
                      <td colspan="3">
                        <h4 align="center">Tidak ada data</h4>
                      </td>
                      <td>
                        <div>
                          <a class="btn btn-large btn-primary pull-right" href="pembimbing/createD" role="button">Pilih Pembimbing 2</a>
                        </div>
                      </td>
                    </tr>
                  <?php } else { ?>
                    <?php foreach ($pbb2 as $key2 => $value2) { ?>
                      <tr>
                        <td><?= $value2->nama_dosen ?></td>
                        <td><?= $value2->status_pengajuan ?></td>
                        <td></td>
                        <td>
                          <a class="btn btn-large btn-danger btn-delete pull-right" href="#" data-href="<?php echo base_url('pembimbing/delete/' . $value2->id_bimbingan) ?>" role="button">
                            <?php if ($value2->status_pengajuan == "pengajuan") { ?>
                              Cancel
                            <?php } else { ?>
                              Delete
                            <?php } ?>
                          </a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
  <script>
    $(document).ready(function() {
      //event ketika tombol hapus di click
      $(document).on('click', '.btn-delete', function() {
        Swal.fire({
          title: 'Apakah anda yakin akan menghapus dosen pembimbing ini?',
          text: "Jika data dihapus, anda dapat melakukan pengajuan ulang",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).data('href');
          }
        })
      });
    });
  </script>
</body>

</html>