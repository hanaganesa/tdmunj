<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

 <!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('layouts/head.php') ?>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url('assets/material-dashboard-master/assets/img/sidebar-1.jpg') ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo base_url ('dashboard-mahasiswa') ?>" class="simple-text logo-normal">
          Mahasiswa
        </a>
      </div>
      <?php $this->load->view('layouts/sidebarM'); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view('layouts/navbar'); ?>
      <!-- End Navbar -->
      <div class="content col-md-9 d-flex justify-content-center">
      <div class="content col-md-3 d-flex justify-content-center"></div>
        <div class="container-fluid">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h3 class="card-title">Dosen Pembimbing 1</h3>
                </div>
                <div class="card-body">
                    <form action="<?= base_url('pembimbing/store') ?>" method="post">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="pembimbing1">Pilih</label>
                          <select id="pembimbing1" class="form-control select2" name="pembimbing1">
                            <?php foreach ($dosen as $key => $value) { ?>
                              <option value="<?= $value->username ?>"><?= $value->nama_dosen ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="footer">
      <?php $this->load->view('layouts/footer'); ?>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <?php $this->load->view('layouts/js'); ?>
</body>

</html>
