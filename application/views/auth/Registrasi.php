<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">

    <!-- Drop Down Menu -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <title>Registrasi</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/colorlib-regform-7/fonts/material-icon/css/material-design-iconic-font.min.css') ?>">
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/colorlib-regform-7/css/style.css') ?>">

    <!-- dropify -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dropify/dist/css/dropify.min.css') ?>">
</head>

<body>
    <br>
    <!-- <div class="main"> -->
    <!-- Sign up form -->
    <section class="signup">
        <div class="container">
            <div class="signup-content">
                <div class="signup-form">
                    <h2 class="form-title">Registrasi</h2>
                    <?php echo form_open_multipart('auth/RegistrasiController/registrasi') ?>
                    <!-- nim -->
                    <div class="form-group">
                        <input type="number" name="username" class="form-control" required="required" id="username" placeholder="NIM" min="10" title="NIM" />
                    </div>
                    <!-- nama -->
                    <div class="form-group">
                        <input type="text" name="nama" class="form-control" required="required" id="nama" placeholder="Nama" title="Nama" />
                    </div>
                    <!-- email -->
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" required="required" id="email" placeholder="Email" title="Email" />
                    </div>
                    <!-- password -->
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" required="required" id="password" placeholder="Password" pattern=".{8,}" required title="Password minimal 8 huruf" />
                    </div>
                    <!-- confirm password -->
                    <div class="form-group">
                        <input type="password" name="confirm_password" class="form-control" required="required" id="confirm_password" placeholder="Konfirmasi Password" pattern=".{8,}" required title="Password minimal 8 huruf" />
                    </div>
                    <!-- picture -->
                    <p>Unggah Foto</p>
                    <div class="form-group">
                        <input type="file" name="foto" id="foto" class="form-control">
                    </div>
                    <!-- judul -->
                    <div class="form-judul">
                        <label for="judul"></i></label>
                        <textarea class="form-control" name="judul" id="judul" required="required" placeholder="Judul" rows="3" cols="36" title="Judul Skripsi"></textarea>
                    </div>
                    <!-- abstrak -->
                    <div class="form-judul">
                        <textarea class="form-control" name="abstrak" id="abstrak" required="required" placeholder="Abstrak" rows="5" cols="36" title="Abstrak Skripsi"></textarea>
                    </div>
                    <!-- kategori -->
                    <p>Pilih Kategori</p>
                    <div class="form-group">
                        <select class="form-control select2" name="kategori" id="kategori" title="Kategori Skripsi">
                            <option value="pendidikan">Pendidikan</option>
                            <option value="linguistik">Linguistik</option>
                            <option value="komprehensif">Komprehensif</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-large btn-block btn-info" name="submit" id="submit" class="form-submit">Daftar</button>
                    <!-- </div> -->
                    <?php form_close(); ?>

                </div>
                <div class="signup-image">
                    <figure><img src="<?php echo base_url('assets/colorlib-regform-7/images/signup-image.png') ?>" alt="sing up image"></figure>
                    <a href="<?php echo base_url('signin') ?>" class="signup-image-link">Sudah punya akun?</a>
                    <a href="<?php echo base_url('home') ?>" class="signup-image-link">Halaman Utama</a>
                </div>
            </div>
        </div>
    </section>
    <!-- </div> -->
    <!-- JS -->
    <script src="<?php echo base_url('assets/colorlib-regform-7/vendor/jquery/jquery.min.js') ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/material-dashboard-master/assets/js/plugins/bootstrap-notify.js') ?>"></script>
    <script>
        $(document).ready(function() {

            var validation = <?= json_encode(explode('.', strip_tags(validation_errors()))); ?>;
            validation.pop();
            if (validation) {
                $.each(validation, function(idx, msg) {
                    $.notify({
                        // options
                        message: msg
                    }, {
                        // settings
                        type: 'danger'
                    });
                });
            }
            var success = '<?= @$_SESSION['success'] ?>';
            if (success) {
                $.notify({
                    // options
                    message: success
                }, {
                    // settings
                    type: 'success'
                });
            }
            var error = '<?= @$_SESSION['error'] ?>';
            if (error) {
                $.notify({
                    // options
                    message: error
                }, {
                    // settings
                    type: 'danger'
                });
            }
        });
    </script>
    <script>
        $('.dropify').dropify();
    </script>
</body>

</html>