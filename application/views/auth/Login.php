<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/material-dashboard-master/assets/img/hon.png') ?>">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- Font Icon -->
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link re l="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/colorlib-regform-7/fonts/material-icon/css/material-design-iconic-font.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/colorlib-regform-7/css/style.css') ?>">
</head>

<body>
    <br>
    <!-- <div class="main"> -->
    <!-- Sing in  Form -->
    <section class="sign-in">
        <div class="container">
            <div class="signin-content">
                <div class="signin-image">
                    <figure><img src="<?php echo base_url('assets/colorlib-regform-7/images/signin-image.png') ?>" alt="sing up image"></figure>
                    <a href="<?php echo base_url('home') ?>" class="signup-image-link">Halaman Utama</a>
                </div>

                <div class="signin-form">
                    <h2 class="form-title">Login</h2>
                    <?php echo form_open('auth/LoginController/login') ?>
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" required="required" id="username" placeholder="NIM / Username / NIDN" />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" required="required" id="password" placeholder="Password" />
                    </div><br>
                    <button type="submit" class="btn btn-large btn-block btn-info" name="submit" id="submit" class="form-submit">Masuk</button>
                    <br>
                    <a href="<?php echo base_url('signup') ?>" class="signup-image-link">Buat akun</a>
                    <a href="<?php echo base_url('lupa-password') ?>" class="signup-image-link">Lupa Password?</a>
                </div>
            </div>
        </div>
    </section>
    <!-- </div> -->
    <!-- JS -->
    <script src="<?php echo base_url('assets/colorlib-regform-7/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/colorlib-regform-7/js/main.js') ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/material-dashboard-master/assets/js/plugins/bootstrap-notify.js') ?>"></script>
    <script>
        $(document).ready(function() {

            var validation = <?= json_encode(explode('.', strip_tags(validation_errors()))); ?>;
            validation.pop();
            if (validation) {
                $.each(validation, function(idx, msg) {
                    $.notify({
                        // options
                        message: msg
                    }, {
                        // settings
                        type: 'danger'
                    });
                });
            }
            var success = '<?= @$_SESSION['success'] ?>';
            if (success) {
                $.notify({
                    // options
                    message: success
                }, {
                    // settings
                    type: 'success'
                });
            }
            var error = '<?= @$_SESSION['error'] ?>';
            if (error) {
                $.notify({
                    // options
                    message: error
                }, {
                    // settings
                    type: 'danger'
                });
            }
        });
    </script>
</body>

</html>