<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']        = 'Welcome';
$route['home']                      = 'frontend/UmumController/Index';
$route['404_override']              = '';
$route['signup']                    = 'auth/RegistrasiController/index';
$route['signin']                    = 'auth/LoginController/index';
$route['logout']                    = 'auth/LoginController/logout';
$route['dashboard-mahasiswa']       = 'mahasiswa/DashboardController/index';
$route['dashboard-admin']           = 'admin/DashboardController/index';
$route['dashboard-dosen']           = 'dosen/DashboardController/index';

$route['umum/help']                 = 'frontend/UmumController/help';
$route['mahasiswa/help']            = 'mahasiswa/DashboardController/help';
$route['dosen/help']                = 'dosen/DashboardController/help';
$route['admin/help']                = 'admin/DashboardController/help';

$route['umum-skripsi']                       = 'frontend/UmumController/listSkripsi';
$route['umum-skripsi/detail/(:any)/(:any)']  = 'frontend/UmumController/detailSkripsi/$1/$2';
$route['umum-skripsi/kesamaan']              = 'frontend/UmumController/kesamaanSkripsi';
$route['umum-skripsi/ukur/(:any)/(:any)']    = 'frontend/UmumController/kesamaanUkur/$1/$2';
$route['umum-skripsi/filter']                = 'frontend/UmumController/filterKategori';

$route['post']                      = 'admin/PostController/index';
$route['post/create']               = 'admin/PostController/create';
$route['post/edit/(:any)']          = 'admin/PostController/edit/$1';
$route['post/store']                = 'admin/PostController/store';
$route['post/update/(:any)']        = 'admin/PostController/update/$1';
$route['post/delete/(:any)']        = 'admin/PostController/delete/$1';

$route['mahasiswa']                             = 'admin/MahasiswaController/index';
$route['mahasiswa/detail/(:any)']               = 'admin/MahasiswaController/detail/$1';
$route['mahasiswa/create']                      = 'admin/MahasiswaController/create';
$route['mahasiswa/edit/(:any)']                 = 'admin/MahasiswaController/edit/$1';
$route['mahasiswa/store']                       = 'admin/MahasiswaController/store';
$route['mahasiswa/update/(:any)']               = 'admin/MahasiswaController/update/$1';
$route['mahasiswa/updateStatus/(:any)/(:any)']  = 'admin/MahasiswaController/updateStatus/$1/$2';
$route['mahasiswa/delete/(:any)']               = 'admin/MahasiswaController/delete/$1';

$route['dosen']                             = 'admin/DosenController/index';
$route['dosen/create']                      = 'admin/DosenController/create';
$route['dosen/edit/(:any)']                 = 'admin/DosenController/edit/$1';
$route['dosen/store']                       = 'admin/DosenController/store';
$route['dosen/update/(:any)']               = 'admin/DosenController/update/$1';
$route['dosen/updateStatus/(:any)/(:any)']  = 'admin/DosenController/updateStatus/$1/$2';
$route['dosen/delete/(:any)']               = 'admin/DosenController/delete/$1';

$route['admin']                             = 'admin/AdminController/index';
$route['admin/create']                      = 'admin/AdminController/create';
$route['admin/edit/(:any)']                 = 'admin/AdminController/edit/$1';
$route['admin/store']                       = 'admin/AdminController/store';
$route['admin/update/(:any)']               = 'admin/AdminController/update/$1';
$route['admin/updateStatus/(:any)/(:any)']  = 'admin/AdminController/updateStatus/$1/$2';
$route['admin/delete/(:any)']               = 'admin/AdminController/delete/$1';

$route['admin-skripsi']               = 'admin/SkripsiController/index';
$route['admin-skripsi/filter']        = 'admin/SkripsiController/filterKategori';
$route['admin-skripsi/create']        = 'admin/SkripsiController/create';
$route['admin-skripsi/edit/(:any)']   = 'admin/SkripsiController/edit/$1';
$route['admin-skripsi/store']         = 'admin/SkripsiController/store';
$route['admin-skripsi/update/(:any)'] = 'admin/SkripsiController/update/$1';
$route['admin-skripsi/delete/(:any)'] = 'admin/SkripsiController/delete/$1';
$route['admin-skripsi/detail/(:any)/(:any)'] = 'admin/SkripsiController/detail/$1/$2';

$route['mahasiswa-skripsi']                    = 'mahasiswa/SkripsiController/index';
$route['mahasiswa-listSkripsi']                = 'mahasiswa/SkripsiController/listSkripsi';
$route['mahasiswa-skripsi/filter']             = 'mahasiswa/SkripsiController/filterKategori';
$route['mahasiswa-skripsi/create']             = 'mahasiswa/SkripsiController/create';
$route['mahasiswa-skripsi/edit/(:any)']        = 'mahasiswa/SkripsiController/edit/$1';
$route['mahasiswa-skripsi/editH/(:any)']       = 'mahasiswa/SkripsiController/editH/$1';
$route['mahasiswa-skripsi/update/(:any)']      = 'mahasiswa/SkripsiController/update/$1';
$route['mahasiswa-skripsi/updateH/(:any)']     = 'mahasiswa/SkripsiController/updateH/$1';
$route['mahasiswa-skripsi/delete/(:any)']      = 'mahasiswa/SkripsiController/delete/$1';
$route['mahasiswa-skripsi/kesamaan']           = 'mahasiswa/SkripsiController/kesamaanSkripsi';
$route['mahasiswa-skripsi/ukur/(:any)/(:any)'] = 'mahasiswa/SkripsiController/kesamaanUkur/$1/$2';
$route['mahasiswa-skripsi/store/(:any)/(:any)']  = 'mahasiswa/SkripsiController/store/$1/$2';
$route['mahasiswa-skripsi/detail/(:any)/(:any)'] = 'mahasiswa/SkripsiController/detail/$1/$2';
$route['mahasiswa-skripsi/detailSkripsi/(:any)'] = 'mahasiswa/SkripsiController/detailSkripsi/$1';
$route['mahasiswa-skripsi/detailHistori/(:any)'] = 'mahasiswa/SkripsiController/detailHistori/$1';

$route['dosen-skripsi']               = 'dosen/SkripsiController/index';
$route['dosen-skripsi/filter']        = 'dosen/SkripsiController/filterKategori';
$route['dosen-skripsi/create']        = 'dosen/SkripsiController/create';
$route['dosen-skripsi/edit/(:any)']   = 'dosen/SkripsiController/edit/$1';
$route['dosen-skripsi/store']         = 'dosen/SkripsiController/store';
$route['dosen-skripsi/update/(:any)'] = 'dosen/SkripsiController/update/$1';
$route['dosen-skripsi/delete/(:any)'] = 'dosen/SkripsiController/delete/$1';
$route['dosen-skripsi/detail/(:any)/(:any)'] = 'dosen/SkripsiController/detail/$1/$2';

$route['pembimbing']               = 'mahasiswa/PembimbingController/index';
$route['pembimbing/create']        = 'mahasiswa/PembimbingController/create';
$route['pembimbing/createD']       = 'mahasiswa/PembimbingController/createD';
$route['pembimbing/edit/(:any)']   = 'mahasiswa/PembimbingController/edit/$1';
$route['pembimbing/store']         = 'mahasiswa/PembimbingController/store';
$route['pembimbing/storeD']        = 'mahasiswa/PembimbingController/storeD';
$route['pembimbing/update/(:any)'] = 'mahasiswa/PembimbingController/update/$1';
$route['pembimbing/delete/(:any)'] = 'mahasiswa/PembimbingController/delete/$1';

$route['admin-bimbingan']               = 'admin/BimbinganController/index';
$route['admin-bimbingan/detail/(:any)'] = 'admin/BimbinganController/detail/$1';
$route['admin-bimbingan/create']        = 'admin/BimbinganController/create';
$route['admin-bimbingan/edit/(:any)']   = 'admin/BimbinganController/edit/$1';
$route['admin-bimbingan/store']         = 'admin/BimbinganController/store';
$route['admin-bimbingan/update/(:any)'] = 'admin/BimbinganController/update/$1';
$route['admin-bimbingan/delete/(:any)'] = 'admin/BimbinganController/delete/$1';

$route['dosen-bimbingan']                             = 'dosen/BimbinganController/index';
$route['dosen-bimbingan/create']                      = 'dosen/BimbinganController/create';
$route['dosen-bimbingan/acc/(:any)/(:any)/(:any)']    = 'dosen/BimbinganController/acc/$1/$2/$3';
$route['dosen-bimbingan/decl/(:any)/(:any)/(:any)']   = 'dosen/BimbinganController/decl/$1/$2/$3';
$route['dosen-bimbingan/store']                       = 'dosen/BimbinganController/store';
$route['dosen-bimbingan/show/(:any)']                 = 'dosen/BimbinganController/show/$1';
$route['dosen-bimbingan/update/(:any)']               = 'dosen/BimbinganController/update/$1';
$route['dosen-bimbingan/delete/(:any)']               = 'dosen/BimbinganController/delete/$1';
$route['dosen-bimbingan/detailGantiJudul/(:any)/(:any)'] = 'dosen/BimbinganController/detailGantiJudul/$1/$2';
$route['dosen-bimbingan/tolakJudul/(:any)/(:any)/(:any)'] = 'dosen/BimbinganController/tolakJudul/$1/$2/$3';
$route['dosen-bimbingan/acceptJudul/(:any)/(:any)/(:any)'] = 'dosen/BimbinganController/acceptJudul/$1/$2/$3';

$route['admin-profile']               = 'admin/AdminController/profile';
$route['admin-profile/create']        = 'admin/AdminController/create';
$route['admin-profile/edit/(:any)']   = 'admin/AdminController/edit/$1';
$route['admin-profile/store']         = 'admin/AdminController/store';
$route['admin-profile/update/(:any)'] = 'admin/AdminController/update/$1';
$route['admin-profile/delete/(:any)'] = 'admin/AdminController/delete/$1';

$route['mahasiswa-profile']               = 'mahasiswa/MahasiswaController/index';
$route['mahasiswa-profile/create']        = 'mahasiswa/MahasiswaController/create';
$route['mahasiswa-profile/edit/(:any)']   = 'mahasiswa/MahasiswaController/edit/$1';
$route['mahasiswa-profile/store']         = 'mahasiswa/MahasiswaController/store';
$route['mahasiswa-profile/update/(:any)'] = 'mahasiswa/MahasiswaController/update/$1';
$route['mahasiswa-profile/delete/(:any)'] = 'mahasiswa/MahasiswaController/delete/$1';

$route['dosen-profile']               = 'dosen/DosenController/index';
$route['dosen-profile/create']        = 'dosen/DosenController/create';
$route['dosen-profile/edit/(:any)']   = 'dosen/DosenController/edit/$1';
$route['dosen-profile/store']         = 'dosen/DosenController/store';
$route['dosen-profile/update/(:any)'] = 'dosen/DosenController/update/$1';
$route['dosen-profile/delete/(:any)'] = 'dosen/DosenController/delete/$1';

$route['laporan']               = 'admin/LaporanController/index';
$route['laporan/create']        = 'admin/LaporanController/create';
$route['laporan/edit/(:any)']   = 'admin/LaporanController/edit/$1';
$route['laporan/store']         = 'admin/LaporanController/store';
$route['laporan/update/(:any)'] = 'admin/LaporanController/update/$1';
$route['laporan/delete/(:any)'] = 'admin/LaporanController/delete/$1';

$route['unggah']               = 'admin/UnggahController/index';
$route['unggah/create']        = 'admin/UnggahController/create';
$route['unggah/edit/(:any)']   = 'admin/UnggahController/edit/$1';
$route['unggah/store']         = 'admin/UnggahController/store';
$route['unggah/update/(:any)'] = 'admin/UnggahController/update/$1';
$route['unggah/delete/(:any)'] = 'admin/UnggahController/delete/$1';
//$route['translate_uri_dashes'] = FALSE;
//NOTIFICATIONS
$route['get-notification'] = 'notif/NotifikasiController/getNotification';
$route['notifikasi/(:any)'] = 'notif/NotifikasiController/notifikasi/$1';

$route['lupa-password']                 = 'auth/ForgotController/index';
$route['lupa-password/reset']           = 'auth/ForgotController/reset';
$route['lupa-password/reset-password']  = 'auth/ForgotController/resetPassword';
$route['lupa-password/reset/(:any)']    = 'auth/ForgotController/formReset/$1';

$route['lupa-password/edit/(:any)']     = 'auth/ForgotController/edit/$1';
$route['lupa-password/store']           = 'auth/ForgotController/store';
$route['lupa-password/update/(:any)']   = 'auth/ForgotController/update/$1';
$route['lupa-password/delete/(:any)']   = 'auth/ForgotController/delete/$1';
