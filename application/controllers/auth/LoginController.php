<?php 

class LoginController extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MahasiswaModel');
        $this->load->model('AdminModel');
		$this->load->model('DosenModel');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
    }

    public function index()
    {
        $this->load->view('auth/Login');
    }

    public function login()
    {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run() == FALSE){
            $this->load->view('auth/Login');
        } else
        {
			$data_admin			= $this->AdminModel->login();
			$data_mahasiswa		= $this->MahasiswaModel->login();
			$data_dosen			= $this->DosenModel->login();
			if($data_admin->num_rows() > 0)
			{
				$res = $data_admin->row_array();
				$this->session->username = $res['username'];
				foreach ($data_admin->result() as $res) 
				{
					$username ['username'] = $res->username;
					$this->session->set_userdata($username);
				}
				redirect('dashboard-admin');
			} else if ($data_mahasiswa->num_rows() > 0)
			{
				$res = $data_mahasiswa->row_array();
				$this->session->username = $res['username'];
				foreach ($data_mahasiswa->result() as $res) 
				{
					$username ['username'] = $res->username;
					if ($res->status=='aktif') {
						$this->session->set_userdata($username);
						$data = array('username'=>$username);
						redirect('dashboard-mahasiswa');
					} else
					{
						$this->session->set_flashdata('success', "User belum aktif!");
						$this->session->set_flashdata('error', "Mohon untuk meminta verifikasi akun ke koordinator skripsi!");
						redirect('signin');
					}
				}
			} else if ($data_dosen->num_rows() > 0)
			{
				$res = $data_dosen->row_array();
				$this->session->username = $res['username'];
				foreach ($data_dosen->result() as $res) 
				{
					$username ['username'] = $res->username;
					if ($res->status=='aktif') {
						$this->session->set_userdata($username);
						$data = array('username'=>$username);
						redirect('dashboard-dosen');
					} else
					{
						$this->session->set_flashdata('success', "User belum aktif!");
						$this->session->set_flashdata('error', "Mohon untuk meminta verifikasi akun ke koordinator skripsi!");
						redirect('signin');
					}
				}
				$data = array('username'=>$username);
			} else
			{
				$this->session->set_flashdata('error', "Pastikan user dan password sudah benar!");
				redirect('signin');
			}
		}
    }
    
    public function logout()
    {
        session_destroy();
        redirect('signin');
    }
}
