<?php

class ForgotController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MahasiswaModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $this->load->view('auth/resetPassEmail');
    }

    public function reset()
    {
        $email = $this->input->post('email');
        $reset_key = random_string('alnum', 50);

        if ($this->MahasiswaModel->updateResetKey($email, $reset_key)) {
            $this->load->library('email');
            $config = array();
            $config['charset'] = 'utf-8';
            $config['useragent'] = 'Codeigniter';
            $config['protocol'] = "smtp";
            $config['mailtype'] = "html";
            $config['smtp_host'] = "ssl://smtp.gmail.com"; //pengaturan smtp
            $config['smtp_port'] = "465";
            $config['smtp_timeout'] = "5";
            $config['smtp_user'] = "yuigahamayuri@gmail.com"; // isi dengan email kamu
            $config['smtp_pass'] = "senjougahara"; // isi dengan password kamu
            $config['crlf'] = "\r\n";
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;
            //memanggil library email dan set konfigurasi untuk pengiriman email

            $this->email->initialize($config);
            //konfigurasi pengiriman
            $this->email->from($config['smtp_user']);
            $this->email->to($this->input->post('email'));
            $this->email->subject("Reset your password");

            $message = "<p>Anda melakukan permintaan reset password</p>";
            $message .= "<a href='" . base_url('lupa-password/reset/' . $reset_key) . "'>klik reset password</a>";
            $this->email->message($message);

            if ($this->email->send()) {
                echo "silahkan cek email <b>" . $this->input->post('email') . '</b> untuk melakukan reset password';
            } else {
                echo "<pre>";
                print_r($this->email->print_debugger());
                die();
                echo "Berhasil melakukan registrasi, gagal mengirim verifikasi email";
            }

            echo "<br><br><a href='" . base_url('lupa-password') . "'>Kembali</a>";
            echo "<br><a href='" . base_url('signin') . "'>Kembali ke Menu Login</a>";
        } else if ($this->AdminModel->updateResetKey($email, $reset_key)) {
            var_dump("admin ada");
        } else {
            $this->session->set_flashdata('error', "Email yang anda masukkan belum terdaftar!");
            redirect('lupa-password');
        }
    }

    public function formReset($key)
    {
        $cekMhs = $this->MahasiswaModel->get([
            'reset_password' => $key
        ])->num_rows();
        $cekAdmin = $this->AdminModel->get([
            'reset_password' => $key
        ])->num_rows();
        if ($cekMhs > 0 || $cekAdmin > 0) {
            $data['key'] = $key;
            $this->load->view('auth/formReset', $data);
        } else {
            echo "Link sudah kadaluarsa!";
            echo "<br><a href='" . base_url('signin') . "'>Kembali ke Menu Login</a>";
        }
    }
    public function resetPassword()
    {
        $key = $this->input->post('key');
        $password = $this->input->post('password');
        $password_confirmation = $this->input->post('password_confirmation');
        if ($password == $password_confirmation) {
            $cekMhs = $this->MahasiswaModel->get([
                'reset_password' => $key
            ])->num_rows();
            $cekAdmin = $this->AdminModel->get([
                'reset_password' => $key
            ])->num_rows();
            $data = [
                'password' => md5($password),
                'reset_password' => ''
            ];
            $ok = '';
            if ($cekMhs > 0) {
                $ok = $this->MahasiswaModel->updatePassword($key, $data);
            }
            if ($cekAdmin > 0) {
                $ok = $this->AdminModel->updatePassword($key, $data);
            }
            if ($ok) {
                $this->session->set_flashdata('success', "Password anda berhasil diperbarui!");
                redirect('signin');
            }
        } else {
            $this->session->set_flashdata('error', "Password yang anda masukkan tidak sama!");
            redirect('lupa-password/reset/' . $key);
        }
    }
}
