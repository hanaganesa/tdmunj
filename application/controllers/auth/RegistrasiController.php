<?php

class RegistrasiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SkripsiModel');
        $this->load->model('MahasiswaModel');
        $this->load->model('HistoriModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->load->view('auth/Registrasi');
    }

    public function registrasi()
    {
        $username             = $this->input->post('username');
        $nama                 = $this->input->post('nama');
        $email                = $this->input->post('email');
        $pass                 = $this->input->post('password');
        $conf_pass            = $this->input->post('confirm_password');
        $password             = md5($pass);
        $confirm_password     = md5($conf_pass);
        $judul                = $this->input->post('judul');
        $abstrak              = $this->input->post('abstrak');
        $status_mahasiswa     = 'aktif';
        $kategori             = $this->input->post('kategori');
        $foto                 = $_FILES['foto'];
        if ($foto['name'] != NULL) {
            $config['upload_path']   = './assets/foto/';
            $config['allowed_types'] = 'jpg|png|gif';
            $config['file_name']     = $username;
            $config['overwrite']     = true;
            $config['max_size']      = 5000000;
            $path = $_FILES['foto']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                $this->session->set_flashdata('error', "Upload foto gagal!");
                redirect('signup');
            } else {
                $foto = $this->upload->data('foto');
            }
            $foto = $username . '.' . $ext;
        } else {
            $foto = 'profile.png';
        }

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[10]|max_length[10]'
        );
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('abstrak', 'Abstrak', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('auth/Registrasi');
        } else {
            $data_mahasiswa = array(
                'username' => $username,
                'nama' => $nama,
                'email' => $email,
                'password' => $password,
                'foto' => $foto,
                'status' => $status_mahasiswa
            );
            $data_skripsi = array(
                'username' => $username,
                'judul' => $judul,
                'abstrak' => $abstrak,
                'kategori' => $kategori
            );
            $cek = $this->db->select('username')->where(['username' => $username])->get('tb_mahasiswa');
            if ($cek->num_rows() > 0) {
                $insert  = $this->MahasiswaModel->update($username, $data_mahasiswa);
            } else {
                $insert = 0;
            }
            if ($insert == 1) {
                $insert1 = $this->SkripsiModel->add($data_skripsi);
                if ($insert1 == 1) {
                    $data['id'] = $this->SkripsiModel->get(['tb_skripsi.username' => $username])->row();
                    $d = $data['id']->id_skripsi;
                    $data_histori = array(
                        'id_skripsi' => $d,
                        'judul_histori' => $judul,
                        'abstrak_histori' => $abstrak,
                        'status_histori' => 'diterima',
                        'kategori_histori' => $kategori,
                        'status1' => 'diterima',
                        'status2' => 'diterima'
                    );
                    $insert2 = $this->HistoriModel->add($data_histori);
                    if ($insert2 == 1) {
                        $this->session->set_flashdata('success', "Registrasi berhasil!");
                        redirect('signin');
                    } else {
                        $this->session->set_flashdata('error', "Mohon maaf, terjadi kesalahan saat melakukan registrasi.");
                        redirect('signup');
                    }
                } else {
                    $this->session->set_flashdata('error', "Mohon maaf, terjadi kesalahan saat melakukan registrasi.");
                    redirect('signup');
                }
            } else {
                $this->session->set_flashdata('error', "Mohon maaf, nim anda tidak terdaftar.");
                redirect('signup');
            }
        }
    }
}
