<?php

class MahasiswaController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('MahasiswaModel');
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
        $this->load->model('BimbinganModel');
        $this->load->model('AdminModel');
        $this->load->helper('url');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
    }

    public function index()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Akun Mahasiswa";
        $data['mahasiswa'] = $this->MahasiswaModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/verifikasiAkun', $data);
    }

    public function detail($nim)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "List Mahasiswa";
        $data['mahasiswa'] = $this->MahasiswaModel->getbyId($nim);
        $data['skripsi']   = $this->SkripsiModel->get(['tb_skripsi.username' => $nim])->result();
        $data['histori1']  = $this->HistoriModel->get(['tb_mahasiswa.username' => $nim, 'status_histori' => 'diterima'], 'desc', 1)->row();
        $data['histori']   = $this->HistoriModel->getWhere(['tb_mahasiswa.username' => $nim], 'desc')->result();
        $data['pembimbing'] = $this->BimbinganModel->get(['tb_bimbingan.nim' => $nim, 'status_pengajuan' => 'diterima'])->num_rows();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/mahasiswa', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Create Akun";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/CreateMahasiswa', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Edit Akun";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/EditMahasiswa', $data);
    }

    public function store()
    {
        $file = $_FILES['excel']['tmp_name'];
        try {
            $load = IOFactory::load($file);
        } catch (PHPExcel_Reader_Exception $e) {
            die('File yang di-upload mengalami kesalahan. <br><a href="' . base_url('mahasiswa') . '" >Kembali</a>');
        }
        $sheets = $load->getActiveSheet()->toArray(null, true, true, true);

        $i = 0;
        foreach ($sheets as $key => $val) {
            $nim = $val['A'];
            $data = ['username' => $nim];
            $cek = $this->db->select('username')->where(['username' => $nim])->get('tb_mahasiswa');
            if ($cek->num_rows() == 0) {
                $this->db->insert('tb_mahasiswa', $data);
                $i++;
            }
        }

        if ($i > 0) {
            $this->session->set_flashdata('success', "Data berhasil dimasukan!");
            redirect('mahasiswa');
        }
    }

    public function updateStatus($id, $status)
    {
        if ($status == "aktif") {
            $status = "nonaktif";
        } else {
            $status = "aktif";
        }

        $statusD = $this->MahasiswaModel->updateStatus($id, $status);
        $msgSucceed = $status == "aktif" ? "Akun berhasil diaktifkan!" : "Akun berhasil dinonaktifkan!";
        $msgFailed = $status == "aktif" ? "Akun gagal dinonaktifkan!" : "Akun gagal diaktifkan!";
        if ($statusD == 1) {
            $this->session->set_flashdata('success', $msgSucceed);
            redirect('mahasiswa');
        } else {
            $this->session->set_flashdata('error', $msgFailed);
            redirect('mahasiswa');
        }
    }

    public function delete($nim)
    {
        $status = $this->MahasiswaModel->delete($nim);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil dihapus!");
            redirect('mahasiswa');
        } else {
            $this->session->set_flashdata('error', "Data gagal dihapus!");
            redirect('mahasiswa');
        }
    }
}
