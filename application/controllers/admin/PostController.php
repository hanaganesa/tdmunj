<?php

class PostController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('PostinganModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Postingan";
        $data['postingan'] = $this->PostinganModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/Post', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Buat Postingan";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/CreatePost', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Edit Postingan";
        $data['postingan'] = $this->PostinganModel->getById($id);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/CreatePost', $data);
    }

    public function store()
    {
        $judul_postingan    = $this->input->post('judul_postingan');
        $isi_postingan         = $this->input->post('isi_postingan');
        $link                 = $this->input->post('link');
        $username           = $this->session->userdata('username');
        $data               = array(
            'judul_postingan' => $judul_postingan,
            'isi_postingan' => $isi_postingan,
            'link' => $link,
            'username' => $username
        );
        $status = $this->PostinganModel->add($data);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil diposting!");
            redirect('post');
        } else {
            $this->session->set_flashdata('error', "Data gagal diposting!");
            redirect('post');
        }
    }

    public function update($id)
    {
        $data = $this->input->post(NULL, true);
        $status = $this->PostinganModel->update($id, $data);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil diupdate!");
            redirect('post');
        } else {
            $this->session->set_flashdata('error', "Data gagal diupdate!");
            redirect('post/edit/' . $id);
        }
    }

    public function delete($id)
    {
        $status = $this->PostinganModel->delete($id);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil dihapus!");
            redirect('post');
        } else {
            $this->session->set_flashdata('error', "Data gagal dihapus!");
            redirect('post');
        }
    }
}
