<?php

class DosenController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('DosenModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Akun Dosen";
        $data['dosen'] = $this->DosenModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/dosen', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Buat Akun Dosen";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/createDosen', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Edit Akun Dosen";
        $data['dosen'] = $this->DosenModel->getById($id);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/createDosen', $data);
    }

    public function store()
    {
        $username   = $this->input->post('username');
        $nama_dosen = $this->input->post('nama_dosen');
        $pass       = $this->input->post('username');
        $password   = md5($pass);
        $data       = array(
            'username' => $username,
            'nama_dosen' => $nama_dosen,
            'password' => $password
        );

        $this->form_validation->set_rules(
            'username',
            'NIDN',
            'required|min_length[10]|max_length[10]|is_unique[tb_dosen.username]'
        );
        $this->form_validation->set_rules('nama_dosen', 'Nama Dosen', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/createDosen');
        } else {
            $status = $this->DosenModel->add($data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Akun berhasil dibuat!");
                redirect('dosen');
            } else {
                $this->session->set_flashdata('error', "Akun gagal dibuat!");
                redirect('dosen');
            }
        }
    }

    public function update($id)
    {
        $username = $this->input->post('username');
        $nama     = $this->input->post('nama_dosen');
        $pass     = $this->input->post('password');
        if ($pass == NULL) {
            $pass = $this->input->post('username');
        } else {
            $this->form_validation->set_rules('password', 'Password', 'required');
        }
        $password = md5($pass);
        $email    = '';
        $data     = array(
            'username' => $username,
            'nama_dosen' => $nama,
            'password' => $password,
            'email' => $email
        );

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[10]|max_length[10]'
        );
        $this->form_validation->set_rules('nama_dosen', 'Nama Admin', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['jenisAkun'] = "admin";
            $data['judul'] = "Edit Akun Dosen";
            $data['dosen'] = $this->DosenModel->getById($id);
            $this->load->view('admin/createDosen', $data);
        } else {
            $status = $this->DosenModel->update($id, $data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Data berhasil diupdate!");
                redirect('dosen');
            } else {
                $this->session->set_flashdata('error', "Data gagal diupdate!");
                redirect('dosen/edit/' . $id);
            }
        }
    }

    public function updateStatus($id, $status)
    {
        if ($status == "aktif") {
            $status = "nonaktif";
        } else {
            $status = "aktif";
        }

        $statusD = $this->DosenModel->updateStatus($id, $status);
        $msgSucceed = $status == "aktif" ? "Akun berhasil diaktifkan!" : "Akun berhasil dinonaktifkan!";
        $msgFailed = $status == "aktif" ? "Akun gagal dinonaktifkan!" : "Akun gagal diaktifkan!";
        if ($statusD == 1) {
            $this->session->set_flashdata('success', $msgSucceed);
            redirect('dosen');
        } else {
            $this->session->set_flashdata('error', $msgFailed);
            redirect('dosen');
        }
    }
}
