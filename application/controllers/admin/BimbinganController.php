<?php

class BimbinganController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('BimbinganModel');
        $this->load->model('DosenModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['judul'] = "Bimbingan";
        $data['jenisAkun'] = "admin";
        $data['dosen']     = $this->DosenModel->get()->result();
        $data['bimbingan'] = $this->BimbinganModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/bimbingan', $data);
    }

    public function detail($nidn)
    {
        $data['judul'] = "Bimbingan";
        $data['jenisAkun'] = "admin";
        $data['dosen']     = $this->DosenModel->get(['username' => $nidn])->row();
        $data['bimbingan'] = $this->BimbinganModel->get(['nidn' => $nidn])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/detailBimbingan', $data);
    }
}
