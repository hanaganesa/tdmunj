<?php

class SkripsiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "List Skripsi";
        $data['skripsi'] = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/skripsi', $data);
    }

    public function detail($id, $id_skripsi)
    {
        $data['jenisAkun']  = "admin";
        $data['judul']      = "Skripsi";
        $data['histori']    = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->result();
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.id_skripsi' => $id_skripsi])->result();
        $data['histori_count'] = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->num_rows();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/detail', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Create Data Skrpisi";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/CreateSkripsi', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Edit Data Skripsi";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/EditSkripsi', $data);
    }
}
