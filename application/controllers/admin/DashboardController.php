<?php

class DashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }

        $this->load->model('MahasiswaModel');
        $this->load->model('SkripsiModel');
        $this->load->model('DosenModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Dashboard";
        $data['jml_mahasiswa'] = $this->MahasiswaModel->getNotNull()->num_rows();
        $data['jml_dosen'] = $this->DosenModel->get()->num_rows();
        $data['jml_admin'] = $this->AdminModel->get()->num_rows();
        $data['jml_judul_aktif'] = $this->SkripsiModel->get(['tb_skripsi'])->num_rows();
        $judul_summary = $this->SkripsiModel->getSummary()->result();
        $jml = [];
        $label = [];
        $i = 0;
        foreach ($judul_summary as $key => $v) {
            $jml[$key] = $v->total;
            $label[$key] = $v->kategori;
        }
        $data['data'] = json_encode($jml);
        $data['label'] = json_encode($label);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/HomeAdmin', $data);
    }

    public function help()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Instruksi Manual";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/help', $data);
    }
}
