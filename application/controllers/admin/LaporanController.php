<?php

class LaporanController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('MahasiswaModel');
        $this->load->model('HistoriModel');
        $this->load->model('BimbinganModel');
        $this->load->model('DosenModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun']  = "admin";
        $data['judul']      = "Laporan";
        if (isset($_GET['tahun'])) {
            $tahun = $_GET['tahun'];
            $data['tahun'] = $tahun;
            $data['labels'] = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
            $mahasiswa      = $this->db->select('MONTH(tanggal) as bulan, count(*) as jumlah')->where(['YEAR(tanggal)' => $tahun])->group_by('bulan')->get('tb_mahasiswa')->result();
            $pembimbing     = $this->db->select('MONTH(waktu) as bulan, count(*) as jumlah')->where(['YEAR(waktu)' => $tahun])->group_by('bulan')->get('tb_bimbingan')->result();
            $judul          = $this->db->select('MONTH(tanggal_histori) as bulan, count(*) as jumlah')->where(['YEAR(tanggal_histori)' => $tahun, 'status_histori' => 'pengajuan'])->group_by('bulan')->get('tb_histori')->result();
            $data['dosen']  = $this->db->select('nama_dosen')->get('tb_dosen')->result();
            $jmlMhs         = $this->DosenModel->get()->result();
            $arrData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($mahasiswa as $k => $m) {
                $arrData[$m->bulan - 1] = $m->jumlah;
            }
            $arrData2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($pembimbing as $p => $n) {
                $arrData2[$n->bulan - 1] = $n->jumlah;
            }
            $arrData3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            foreach ($judul as $j => $o) {
                $arrData3[$o->bulan - 1] = $o->jumlah;
            }
            $arrData4 = [];
            foreach ($jmlMhs as $key => $p) {
                $p = $this->BimbinganModel->get(['nidn' => $p->username])->num_rows();
                $arrData4[$key] = $p;
            }
            $data['data'] = json_encode($arrData);
            $data['data2'] = json_encode($arrData2);
            $data['data3'] = json_encode($arrData3);
            $data['data4'] = json_encode($arrData4);
            // nama
            $nim = $this->session->userdata('username');
            $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
            $data['nama'] = $data['namaRaw']->nama;
            //endNama
            $this->load->view('admin/laporan', $data);
        }
    }
}
