<?php

class AdminController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $data['jenisAkun']  = "admin";
        $data['judul']      = "Kelola Admin";
        $data['username']   = $this->session->userdata('username');
        $data['admin']      = $this->AdminModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/admin', $data);
    }

    public function profile()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Profile";
        $username = $this->session->userdata('username');
        $data['admin'] = $this->AdminModel->getById($username);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/profile', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Buat Akun Admin";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/createAdmin', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "admin";
        $data['judul'] = "Edit Akun Admin";
        $data['admin'] = $this->AdminModel->getById($id);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->AdminModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('admin/createAdmin', $data);
    }

    public function store()
    {
        $username   = $this->input->post('username');
        $nama       = $this->input->post('nama');
        $password   = md5($username);
        $data       = array(
            'username' => $username,
            'nama' => $nama,
            'password' => $password
        );

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[6]|is_unique[tb_admin.username]'
        );
        $this->form_validation->set_rules('nama', 'Nama Admin', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/createAdmin');
            // die();
        } else {
            $status = $this->AdminModel->add($data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Akun berhasil dibuat!");
                redirect('admin');
            } else {
                $this->session->set_flashdata('error', "Akun gagal dibuat!");
                redirect('admin');
            }
        }
    }

    public function update($id)
    {
        $username = $this->input->post('username');
        $nama     = $this->input->post('nama');
        $pass     = $this->input->post('password');
        $conf_pass = $this->input->post('confirm_password');
        if ($pass == NULL) {
            $pass = $this->input->post('username');
        } else {
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');
        }
        $password = md5($pass);
        $confirm_password = md5($conf_pass);
        $email    = $this->input->post('email');
        $data     = array(
            'username' => $username,
            'nama' => $nama,
            'password' => $password,
            'email' => $email
        );

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[6]'
        );
        $this->form_validation->set_rules('nama', 'Nama Admin', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['jenisAkun'] = "admin";
            $data['judul'] = "Edit Akun Admin";
            $data['admin'] = $this->AdminModel->getById($id);
            $this->load->view('admin/profile', $data);
            // die();
        } else {
            $status = $this->AdminModel->update($id, $data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Data berhasil disimpan!");
                redirect('admin');
            } else {
                $this->session->set_flashdata('error', "Data gagal disimpan!");
                redirect('admin-profile/edit/' . $id);
            }
        }
    }

    public function updateStatus($id, $status)
    {
        if ($status == "aktif") {
            $status = "nonaktif";
        } else {
            $status = "aktif";
        }

        $statusD = $this->AdminModel->updateStatus($id, $status);
        $msgSucceed = $status == "aktif" ? "Akun berhasil diaktifkan!" : "Akun berhasil dinonaktifkan!";
        $msgFailed = $status == "aktif" ? "Akun gagal dinonaktifkan!" : "Akun gagal diaktifkan!";
        if ($statusD == 1) {
            $this->session->set_flashdata('success', $msgSucceed);
            redirect('admin');
        } else {
            $this->session->set_flashdata('error', $msgFailed);
            redirect('admin');
        }
    }

    public function delete($id)
    {
        $status = $this->AdminModel->delete($id);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Akun berhasil dihapus!");
            redirect('admin');
        } else {
            $this->session->set_flashdata('error', "Data gagal dihapus!");
            redirect('admin');
        }
    }
}
