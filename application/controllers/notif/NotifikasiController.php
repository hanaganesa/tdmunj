<?php

class NotifikasiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('HistoriModel');
        $this->load->model('NotifikasiModel');
        $this->load->model('DosenModel');
        $this->load->model('MahasiswaModel');
    }

    public function getNotification()
    {
        $notifications = $this->NotifikasiModel->get(['user_to' => $this->session->userdata('username')], 10)->result();
        $notifs = [];
        foreach ($notifications as $key => $notif) {
            $fromDosen = $this->DosenModel->getById($notif->user_from);
            $toDosen = $this->DosenModel->getById($notif->user_to);
            $fromMahasiswa = $this->MahasiswaModel->getById($notif->user_from);
            $toMahasiswa = $this->MahasiswaModel->getById($notif->user_to);
            $icon = '';
            if ($notif->action == 'request') {
                $icon = '<span class="fa fa-paper-plane text-info"></span>';
            } else if ($notif->action == 'acc') {
                $icon = '<span class="fa fa-check text-success"></span>';
            } else if ($notif->action == 'tolak') {
                $icon = '<span class="fa fa-times text-danger"></span>';
            }
            if ($fromDosen != null) {
                $url = base_url('notifikasi/' . $notif->id);

                $notifs[$key] = [
                    'notif' => $icon . '&nbsp;&nbsp;&nbsp;&nbsp;' . $notif->description . ' [' . $fromDosen->nama_dosen . ']',
                    'url' => $url,
                    'is_read' => $notif->is_read,
                ];
            } else {
                $url = base_url('notifikasi/' . $notif->id);
                $notifs[$key] = [
                    'notif' => $icon . '&nbsp;&nbsp;&nbsp;&nbsp;' . $notif->description . ' [' . $fromMahasiswa->nama . ']',
                    'url' => $url,
                    'is_read' => $notif->is_read,
                ];
            }
        }
        $unread = $this->NotifikasiModel->get(['user_to' => $this->session->userdata('username'), 'is_read' => 0])->result();
        if ($unread) {
            $unread = count($unread);
        } else {
            $unread = 0;
        }
        $data = [
            'notifications' => $notifs,
            'countall' => count($notifications),
            'unread' => $unread,
        ];
        echo json_encode($data);
    }

    public function notifikasi($id)
    {
        $notifikasi = $this->NotifikasiModel->getById($id);
        $update = [
            'is_read' => 1
        ];
        $this->NotifikasiModel->update($id, $update);
        $fromDosen = $this->DosenModel->getById($notifikasi->user_from);
        $toDosen = $this->DosenModel->getById($notifikasi->user_to);
        $fromMahasiswa = $this->MahasiswaModel->getById($notifikasi->user_from);
        $toMahasiswa = $this->MahasiswaModel->getById($notifikasi->user_to);

        $id_histori = $notifikasi->ref_id;
        $get_id_skripsi = $this->HistoriModel->get(['tb_histori.id_histori' => $id_histori], 'desc', 1)->row();
        $id_skripsi = $get_id_skripsi->id_skripsi;
        $lastGantiJudul = $this->HistoriModel->get(['tb_histori.id_skripsi' => $id_skripsi], 'desc', 1)->row();

        if ($fromDosen != null) {
            if ($notifikasi->type == 1) {
                redirect('pembimbing');
            } else {
                redirect('mahasiswa-skripsi');
            }
        } else {
            if ($notifikasi->type == 1) {
                redirect('dosen-bimbingan/show/' . $fromMahasiswa->username);
            } else {
                redirect('dosen-bimbingan');
            }
        }
    }
}
