<?php

class DosenController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('DosenModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "Profile";
        $username = $this->session->userdata('username');
        $data['dosen'] = $this->DosenModel->getById($username);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/dosen', $data);
    }

    public function update($id)
    {
        $username = $this->input->post('username');
        $nama     = $this->input->post('nama_dosen');
        $pass     = $this->input->post('password');
        $password = md5($pass);
        $conf_pass = $this->input->post('confirm_password');
        $confirm_password = md5($conf_pass);
        $email    = $this->input->post('email');

        $data     = array(
            'username' => $username,
            'nama_dosen' => $nama,
            'password' => $password,
            'email' => $email
        );

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[10]|max_length[10]'
        );
        $this->form_validation->set_rules('nama_dosen', 'Nama Dosen', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['jenisAkun'] = "dosen";
            $data['judul'] = "Profile";
            $data['dosen'] = $this->DosenModel->getById($id);
            $this->load->view('dosen/dosen', $data);
        } else {
            $status = $this->DosenModel->update($id, $data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Data berhasil diupdate!");
                redirect('dashboard-dosen');
            } else {
                $this->session->set_flashdata('error', "Data gagal diupdate!");
                redirect('dosen-profil' . $id);
            }
        }
    }
}
