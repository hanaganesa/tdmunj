<?php

class DashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('BimbinganModel');
        $this->load->model('PostinganModel');
        $this->load->model('HistoriModel');
        $this->load->model('DosenModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "Dashboard";
        $nidn = $this->session->userdata('username');
        $data['jml_mahasiswa_bimbingan'] = $this->BimbinganModel->get(['nidn' => $nidn, 'tb_bimbingan.status_pengajuan' => 'diterima'])->num_rows();
        $data['jml_req_mhs'] = $this->BimbinganModel->get(['nidn' => $nidn, 'tb_bimbingan.status_pengajuan' => 'pengajuan'])->num_rows();
        $jml1 = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '1', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status1' => 'pengajuan', 'tb_histori.visibility' => '0'])->num_rows();
        $jml2 = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '2', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status2' => 'pengajuan', 'tb_histori.visibility' => '0'])->num_rows();
        $data['jml_req_ganti_judul'] = $jml1 + $jml2;
        $data['postingan'] = $this->PostinganModel->get()->result();
        $data['bimbingan'] = $this->BimbinganModel->get(['nidn' => $nidn])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/homeDosen', $data);
    }

    public function help()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "Instruksi Manual";

        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/help', $data);
    }
}
