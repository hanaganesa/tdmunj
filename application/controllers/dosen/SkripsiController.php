<?php

class SkripsiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
        $this->load->model('DosenModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "List Skripsi";
        $data['skripsi'] = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/skripsi', $data);
    }

    public function detail($id, $id_skripsi)
    {
        $data['jenisAkun']  = "dosen";
        $data['judul']      = "Skripsi";
        $data['histori']    = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->result();
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.id_skripsi' => $id_skripsi])->result();
        $data['histori_count'] = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->num_rows();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/detail', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "Create Data Skrpisi";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/CreateSkripsi', $data);
    }

    public function edit()
    {
        $data['jenisAkun'] = "dosen";
        $data['judul'] = "Edit Data Skripsi";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/EditSkripsi', $data);
    }
}
