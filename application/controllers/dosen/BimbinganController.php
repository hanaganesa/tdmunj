<?php

class BimbinganController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('BimbinganModel');
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
        $this->load->model('MahasiswaModel');
        $this->load->model('NotifikasiModel');
        $this->load->model('DosenModel');
    }

    public function index()
    {
        $data['jenisAkun']   = "dosen";
        $data['judul']       = "Bimbingan";
        $nidn                = $this->session->userdata('username');
        $data['bimbingan']   = $this->BimbinganModel->get(['nidn' => $nidn, 'status_pengajuan' => 'diterima'])->result();
        $data['request']     = $this->BimbinganModel->get(['nidn' => $nidn, 'status_pengajuan' => 'pengajuan'])->result();
        $data['gantiJudul']  = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '1', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status1' => 'pengajuan', 'tb_histori.visibility' => '0'])->result();
        $data['gantiJudul2'] = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '2', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status2' => 'pengajuan', 'tb_histori.visibility' => '0'])->result();
        $data['jml_request'] = $this->BimbinganModel->get(['nidn' => $nidn, 'status_pengajuan' => 'pengajuan'])->num_rows();
        $jml1 = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '1', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status1' => 'pengajuan', 'tb_histori.visibility' => '0'])->num_rows();
        $jml2 = $this->HistoriModel->get(['tb_bimbingan.nidn' => $nidn, 'tb_bimbingan.pembimbing' => '2', 'tb_histori.status_histori' => 'pengajuan', 'tb_histori.status2' => 'pengajuan', 'tb_histori.visibility' => '0'])->num_rows();
        $data['jml_ganti_judul'] = $jml1 + $jml2;
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/bimbingan', $data);
    }

    public function show($nim)
    {
        $data['jenisAkun'] = "dosen";
        $data['judul']     = "Detail Mahasiswa";
        $data['mahasiswa'] = $this->MahasiswaModel->getbyId($nim);
        $data['skripsi']   = $this->SkripsiModel->get(['tb_skripsi.username' => $nim])->result();
        $nidn              = $this->session->userdata('username');
        $data['request']   = $this->BimbinganModel->get(['nim' => $nim, 'nidn' => $nidn, 'status_pengajuan' => 'pengajuan'])->row();
        $data['request_c'] = $this->BimbinganModel->get(['nim' => $nim, 'nidn' => $nidn, 'status_pengajuan' => 'pengajuan'])->num_rows();
        $data['histori1']  = $this->HistoriModel->get(['tb_mahasiswa.username' => $nim, 'status_histori' => 'diterima'], 'desc', 1)->row();
        $data['histori']   = $this->HistoriModel->getWhere(['tb_mahasiswa.username' => $nim], 'desc')->result();
        $data['pembimbing1'] = $this->BimbinganModel->get(['tb_bimbingan.nim' => $nim, 'pembimbing' => '1', 'status_pengajuan' => 'diterima'])->row();
        $data['pembimbing2'] = $this->BimbinganModel->get(['tb_bimbingan.nim' => $nim, 'pembimbing' => '2', 'status_pengajuan' => 'diterima'])->row();
        $data['pembimbing'] = $this->BimbinganModel->get(['tb_bimbingan.nim' => $nim, 'status_pengajuan' => 'diterima'])->num_rows();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/bimbinganDetail', $data);
    }

    public function detailGantiJudul($id, $nim)
    {
        $data['jenisAkun']  = "dosen";
        $data['judul']      = "Detail Ganti Judul";
        $nidn               = $this->session->userdata('username');
        $data['bimbingan']  = $this->BimbinganModel->get(['nim' => $nim, 'nidn' => $nidn, 'status_pengajuan' => 'diterima'])->row();
        $data['histori1']   = $this->HistoriModel->get(['tb_mahasiswa.username' => $nim, 'status_histori' => 'diterima'], 'desc', 1)->row();
        $data['skripsi']    = $this->HistoriModel->get(['id_histori' => $id])->row();
        $data['skripsiMhsLain'] = $this->HistoriModel->getWhereNot(['status_histori' => 'diterima', 'visibility' => '0'], $nim)->result();
        $newSkripsiMhsLain = [];
        foreach ($data['skripsiMhsLain'] as $key => $m) {
            similar_text($data['histori1']->judul_histori, $m->judul, $percent);
            similar_text($data['histori1']->abstrak_histori, $m->abstrak, $percent2);
            $m->percent = round($percent);
            $m->percent2 = round($percent2);
            $m->average = ($percent + $percent2) * 0.5;
            $newSkripsiMhsLain[$key] = $m;
        }
        usort($newSkripsiMhsLain, function ($a, $b) {
            if ($a->average == $b->average) {
                return 0;
            }
            return ($a->average > $b->average) ? -1 : 1;
        });
        $data['skripsiMhsLain'] = $newSkripsiMhsLain;
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->DosenModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama_dosen;
        //endNama
        $this->load->view('dosen/gantiJudulDetail', $data);
    }

    public function acc($id, $nim, $pembimbing)
    {
        $nidn               = $this->session->userdata('username');
        $nim                = $nim;
        $pembimbing         = $pembimbing;
        $status_pengajuan   = 'diterima';
        $data               = array(
            'nidn' => $nidn,
            'nim' => $nim,
            'pembimbing' => $pembimbing,
            'status_pengajuan' => $status_pengajuan
        );
        $status = $this->BimbinganModel->update($id, $data);
        if ($status == 1) {
            $notif = [
                'type' => '1', //pengajuan
                'action' => 'acc',
                'description' => 'Request Dosen Pembimbing',
                'user_from' => $nidn,
                'user_to' => $nim,
                'ref_id_dos' => $id
            ];
            $ok = $this->NotifikasiModel->add($notif);
            $this->session->set_flashdata('success', "Mahasiswa bimbingan berhasil ditambahkan!");
            redirect('dosen-bimbingan');
        } else {
            $this->session->set_flashdata('error', "Mahasiswa bimbingan gagal ditambahkan!");
            redirect('dosen-bimbingan');
        }
    }

    public function decl($id, $nim, $pembimbing)
    {
        $nidn               = $this->session->userdata('username');
        $nim                = $nim;
        $pembimbing         = $pembimbing;
        $status_pengajuan   = 'ditolak';
        $data               = array(
            'nidn' => $nidn,
            'nim' => $nim,
            'pembimbing' => $pembimbing,
            'status_pengajuan' => $status_pengajuan
        );
        $status = $this->BimbinganModel->update($id, $data);
        if ($status == 1) {
            $notif = [
                'type' => '1', //pengajuan
                'action' => 'tolak',
                'description' => 'Request Dosen Pembimbing',
                'user_from' => $nidn,
                'user_to' => $nim,
                'ref_id_dos' => $id
            ];
            $ok = $this->NotifikasiModel->add($notif);
            $this->session->set_flashdata('success', "Mahasiswa bimbingan berhasil ditolak!");
            redirect('dosen-bimbingan');
        } else {
            $this->session->set_flashdata('error', "Mahasiswa bimbingan gagal ditolak!");
            redirect('dosen-bimbingan');
        }
    }

    public function acceptJudul($pbb, $id_histori, $id_skripsi)
    {
        $komentar = $this->input->get('komentar');
        $data['histori'] = $this->HistoriModel->get(['id_histori' => $id_histori])->row();
        $judul      = $data['histori']->judul_histori;
        $abstrak    = $data['histori']->abstrak_histori;
        $status_h   = $data['histori']->status_histori;
        $kategori   = $data['histori']->kategori_histori;
        $status1    = $data['histori']->status1;
        $status2    = $data['histori']->status2;
        $alasan     = $data['histori']->alasan;
        if ($pbb == 1) {
            if ($status2 == 'pengajuan') {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => $status_h,
                    'status1' => 'diterima',
                    'status2' => $status2,
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            } else {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'diterima',
                    'status1' => 'diterima',
                    'status2' => $status2,
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
                $dataUpdateAll = array('visibility' => '1');
                $statusUpdate = $this->HistoriModel->updateAll($id_skripsi, $dataUpdateAll);
            }
        } else {
            if ($status1 == 'pengajuan') {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => $status_h,
                    'status1' => $status1,
                    'status2' => 'diterima',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            } else {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'diterima',
                    'status1' => $status1,
                    'status2' => 'diterima',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
                $dataUpdateAll = array('visibility' => '1');
                $statusUpdate = $this->HistoriModel->updateAll($id_skripsi, $dataUpdateAll);
            }
        }

        $dataUpdate = array('visibility' => '1');
        $statusUpdate = $this->HistoriModel->update($id_histori, $dataUpdate);
        if ($statusUpdate == 1) {
            $status = $this->HistoriModel->add($data);
            if ($status == 1) {
                $nidn      = $this->session->userdata('username');
                $skripsi   = $this->SkripsiModel->getById($id_skripsi);
                $bimbingan = $this->BimbinganModel->get(['nim' => $skripsi->username, 'status_pengajuan' => 'diterima'])->result()[0];
                $last      = $this->HistoriModel->get(['tb_histori.id_skripsi' => $id_skripsi, 'desc'])->result()[0];
                $notif = [
                    'type' => '2', //ganti_judul
                    'action' => 'acc',
                    'description' => 'Ganti Judul Diterima',
                    'user_from' => $nidn,
                    'user_to' => $bimbingan->nim,
                    'ref_id' => $last->id_histori
                ];
                $ok = $this->NotifikasiModel->add($notif);
                $this->session->set_flashdata('success', "Perubahan judul diterima!");
                redirect('dosen-bimbingan');
            } else {
                $this->session->set_flashdata('error', "Terjadi kesalahan!");
                redirect('dosen-bimbingan');
            }
        } else {
            $this->session->set_flashdata('error', "Perubahan judul gagal diterima!");
            redirect('dosen-bimbingan');
        }
    }

    public function tolakJudul($pbb, $id_histori, $id_skripsi)
    {
        $komentar = $this->input->get('komentar');
        $data['histori'] = $this->HistoriModel->get(['id_histori' => $id_histori])->row();
        $judul      = $data['histori']->judul_histori;
        $abstrak    = $data['histori']->abstrak_histori;
        $status_h   = $data['histori']->status_histori;
        $kategori   = $data['histori']->kategori_histori;
        $status1    = $data['histori']->status1;
        $status2    = $data['histori']->status2;
        $alasan     = $data['histori']->alasan;
        if ($pbb == 1) {
            if ($status2 == 'pengajuan') {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'ditolak',
                    'status1' => 'ditolak',
                    'status2' => '-',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            } else {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'ditolak',
                    'status1' => 'ditolak',
                    'status2' => '-',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            }
        } else {
            if ($status1 == 'pengajuan') {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'ditolak',
                    'status1' => '-',
                    'status2' => 'ditolak',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            } else {
                $data = array(
                    'id_skripsi' => $id_skripsi,
                    'judul_histori' => $judul,
                    'abstrak_histori' => $abstrak,
                    'status_histori' => 'ditolak',
                    'status1' => '-',
                    'status2' => 'ditolak',
                    'kategori_histori' => $kategori,
                    'visibility' => '0',
                    'alasan' => $alasan,
                    'komentar' => $komentar
                );
            }
        }
        $dataUpdate   = array('visibility' => '1');
        $statusUpdate = $this->HistoriModel->update($id_histori, $dataUpdate);
        $status       = $this->HistoriModel->add($data);
        if ($status == 1) {
            if ($statusUpdate) {
                $nidn      = $this->session->userdata('username');
                $skripsi   = $this->SkripsiModel->getById($id_skripsi);
                $bimbingan = $this->BimbinganModel->get(['nim' => $skripsi->username, 'status_pengajuan' => 'diterima'])->result()[0];
                $last      = $this->HistoriModel->get(['tb_histori.id_skripsi' => $id_skripsi, 'desc'])->result()[0];
                $notif = [
                    'type' => '2', //ganti_judul
                    'action' => 'tolak',
                    'description' => 'Penolakan Ganti Judul',
                    'user_from' => $nidn,
                    'user_to' => $bimbingan->nim,
                    'ref_id' => $last->id_histori
                ];
                $ok = $this->NotifikasiModel->add($notif);
                $this->session->set_flashdata('success', "Perubahan judul ditolak!");
                redirect('dosen-bimbingan');
            } else {
                $this->session->set_flashdata('error', "Terjadi kesalahan!");
                redirect('dosen-bimbingan');
            }
        } else {
            $this->session->set_flashdata('error', "Perubahan judul gagal ditolak!");
            redirect('dosen-bimbingan');
        }
    }

    public function delete($id)
    {
        $status = $this->PostinganModel->delete($id);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Mahasiswa bimbingan berhasil dihapus!");
            redirect('dosen-bimbingan');
        } else {
            $this->session->set_flashdata('error', "Mahasiswa bimbingan gagal dihapus!");
            redirect('dosen-bimbingan');
        }
    }
}
