<?php

class DashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('PostinganModel');
        $this->load->model('MahasiswaModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul'] = "Dashboard";
        $data['postingan'] = $this->PostinganModel->get()->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/HomeMahasiswa', $data);
    }

    public function help()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul'] = "Instruksi Manual";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/help', $data);
    }
}
