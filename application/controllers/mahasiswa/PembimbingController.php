<?php

class PembimbingController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('DosenModel');
        $this->load->model('BimbinganModel');
        $this->load->model('NotifikasiModel');
        $this->load->model('MahasiswaModel');
    }

    public function index()
    {
        $data['jenisAkun']      = "mahasiswa";
        $data['judul']          = "Pembimbing";
        $data['jml_bimbingan']  = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')]
        )->num_rows();
        $data['jml_pbb1']       = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')],
            ['tb_bimbingan.pembimbing' => '1']
        )->num_rows();
        $data['jml_pbb2']       = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')],
            ['tb_bimbingan.pembimbing' => '2']
        )->num_rows();
        $data['bimbingan']      = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')]
        )->result();
        $data['pbb1']           = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')],
            ['tb_bimbingan.pembimbing' => '1']
        )->result();
        $data['pbb2']           = $this->BimbinganModel->get(
            ['tb_bimbingan.nim' => $this->session->userdata('username')],
            ['tb_bimbingan.pembimbing' => '2']
        )->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/pembimbing', $data);
    }

    public function create()
    {
        $data['jenisAkun']      = "mahasiswa";
        $data['judul']          = "Pembimbing";
        $data['dosen']          = $this->DosenModel->get(['status' => 'aktif'])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/createPembimbing1', $data);
    }

    public function createD()
    {
        $data['jenisAkun']      = "mahasiswa";
        $data['judul']          = "Pembimbing";
        $data['dosen']          = $this->DosenModel->get(['status' => 'aktif'])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/createPembimbing2', $data);
    }

    public function edit()
    {
        $this->load->view('mahasiswa/EditPembimbing');
    }

    public function store()
    {
        $p1 = $this->input->post('pembimbing1');
        $nim = $this->session->userdata('username');
        $stat = "pengajuan";
        $data1['nidn'] = $p1;
        $data1['nim'] = $nim;
        $data1['status_pengajuan'] = $stat;
        $data1['pembimbing'] = 1;

        $status = $this->BimbinganModel->add($data1);
        if ($status == 1) {
            $last      = $this->BimbinganModel->get(['nim' => $nim, 'nidn' => $p1, 'desc'])->result()[0];
            $notif = [
                'type' => '1', //pengajuan
                'action' => 'request',
                'description' => 'Request Dosen Pembimbing',
                'user_from' => $nim,
                'user_to' => $p1,
                'ref_id_dos' => $last->id_bimbingan
            ];
            $ok = $this->NotifikasiModel->add($notif);
            $this->session->set_flashdata('success', "Pembimbing berhasil dipilih!");
            redirect('pembimbing');
        } else {
            $this->session->set_flashdata('error', "Pembimbing gagal dipilih!");
            redirect('pembimbing');
        }
    }

    public function storeD()
    {
        $p2 = $this->input->post('pembimbing2');
        $nim = $this->session->userdata('username');
        $stat = "pengajuan";

        $data2['nidn'] = $p2;
        $data2['nim'] = $nim;
        $data2['status_pengajuan'] = $stat;
        $data2['pembimbing'] = 2;
        $status2 = $this->BimbinganModel->add($data2);
        if ($status2 == 1) {
            $last      = $this->BimbinganModel->get(['nim' => $nim, 'nidn' => $p2, 'desc'])->result()[0];
            $notif = [
                'type' => '1', //pengajuan
                'action' => 'request',
                'description' => 'Request Dosen Pembimbing',
                'user_from' => $nim,
                'user_to' => $p2,
                'ref_id_dos' => $last->id_bimbingan
            ];
            $ok = $this->NotifikasiModel->add($notif);
            $this->session->set_flashdata('success', "Pembimbing berhasil dipilih!");
            redirect('pembimbing');
        } else {
            $this->session->set_flashdata('error', "Pembimbing gagal dipilih!");
            redirect('pembimbing');
        }
    }

    public function delete($id)
    {
        $status = $this->BimbinganModel->delete($id);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil dihapus!");
            redirect('pembimbing');
        } else {
            $this->session->set_flashdata('error', "Data gagal dihapus!");
            redirect('pembimbing');
        }
    }
}
