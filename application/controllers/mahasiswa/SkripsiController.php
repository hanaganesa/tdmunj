<?php

class SkripsiController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
        $this->load->model('BimbinganModel');
        $this->load->model('NotifikasiModel');
        $this->load->model('MahasiswaModel');
    }

    public function index()
    {
        $data['jenisAkun']  = "mahasiswa";
        $data['judul']      = "Kelola Skripsi";
        $username           = $this->session->userdata('username');
        $data['pembimbing'] = $this->BimbinganModel->get(['tb_bimbingan.nim' => $username, 'status_pengajuan' => 'diterima'])->num_rows();
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.username' => $username])->result();
        $data['histori']    = $this->HistoriModel->getWhere(['tb_mahasiswa.username' => $username], 'desc')->result();
        @$data['histori1']  = $this->HistoriModel->get(['tb_mahasiswa.username' => $username, 'status_histori' => 'diterima'], 'desc', 1)->row();
        @$data['lastRow']   = $this->HistoriModel->get(['tb_mahasiswa.username' => $username], 'desc', 1)->row();
        @$status_h          = @$data['lastRow']->status_histori;
        if (@$status_h == 'pengajuan') {
            @$data['pengajuan'] = 1;
        } else {
            @$data['pengajuan'] = 0;
        }
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/kelolaSkripsi', $data);
    }

    public function detailSkripsi($id)
    {
        $data['jenisAkun']  = "mahasiswa";
        $data['judul']      = "Skripsi";
        $username           = $this->session->userdata('username');
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.id_skripsi' => $id])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/detailSkripsi', $data);
    }

    public function detailHistori($id)
    {
        $data['jenisAkun']  = "mahasiswa";
        $data['judul']      = "Skripsi";
        $username           = $this->session->userdata('username');
        $data['histori']    = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/detailHistori', $data);
    }

    public function detail($id, $id_skripsi)
    {
        $data['jenisAkun']  = "mahasiswa";
        $data['judul']      = "Skripsi";
        $username           = $this->session->userdata('username');
        $data['histori']    = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->result();
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.id_skripsi' => $id_skripsi])->result();
        $data['histori_count'] = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->num_rows();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/detail', $data);
    }

    public function listSkripsi()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul']     = "List Skripsi";
        $data['skripsi']   = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/skripsi', $data);
    }

    public function kesamaanSkripsi()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul']     = "Kesamaan Skripsi";
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/kesamaanSkripsi', $data);
    }

    public function kesamaanUkur()
    {
        $data['jenisAkun']      = "mahasiswa";
        $data['judul']          = "Kesamaan Skripsi";
        $data['judulSkripsi']   = $this->input->post('judul');
        $data['abstrak']        = $this->input->post('abstrak');
        $data['skripsiMhsLain'] = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        $newSkripsiMhsLain = [];
        foreach ($data['skripsiMhsLain'] as $key => $m) {
            similar_text($data['judulSkripsi'], $m->judul, $percent);
            similar_text($data['abstrak'], $m->abstrak, $percent2);
            $m->percent = round($percent);
            $m->percent2 = round($percent2);
            $m->average = ($percent + $percent2) * 0.5;
            $newSkripsiMhsLain[$key] = $m;
        }
        usort($newSkripsiMhsLain, function ($a, $b) {
            if ($a->average == $b->average) {
                return 0;
            }
            return ($a->average > $b->average) ? -1 : 1;
        });
        $data['skripsiMhsLain'] = $newSkripsiMhsLain;
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/kesamaanSkripsi', $data);
    }

    public function create()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul']     = "Pengajuan Ganti Judul";
        $username          = $this->session->userdata('username');
        $data['histori1']  = $this->HistoriModel->get(['tb_mahasiswa.username' => $username, 'status_histori' => 'diterima'], 'desc', 1)->row();
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/gantiJudul', $data);
    }

    public function edit($id)
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul'] = "Edit Data Skripsi";
        $data['skripsi'] = $this->SkripsiModel->getById($id);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/createSkripsi', $data);
    }

    public function editH($id)
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul'] = "Edit Data Skripsi";
        $data['histori'] = $this->HistoriModel->getById($id);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/createSkripsiH', $data);
    }

    public function store($id_skripsi, $id_histori)
    {
        $username   = $this->session->userdata('username');
        $judul      = $this->input->post('judul_histori');
        $abstrak    = $this->input->post('abstrak_histori');
        $kategori   = $this->input->post('kategori_histori');
        $alasan     = $this->input->post('alasan');
        $data       = array(
            'id_skripsi' => $id_skripsi,
            'judul_histori' => $judul,
            'abstrak_histori' => $abstrak,
            'status_histori' => 'pengajuan',
            'status1' => 'pengajuan',
            'status2' => 'pengajuan',
            'kategori_histori' => $kategori,
            'visibility' => '0',
            'alasan' => $alasan
        );

        $this->form_validation->set_rules('judul_histori', 'Judul', 'required');
        $this->form_validation->set_rules('abstrak_histori', 'Abstrak', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('mahasiswa/gantiJudul');
        } else {
            $status = $this->HistoriModel->add($data);
            if ($status == 1) {
                $skripsi = $this->SkripsiModel->getById($id_skripsi);
                $bimbingan = $this->BimbinganModel
                    ->get(['nim' => $skripsi->username, 'status_pengajuan' => 'diterima', 'pembimbing' => '1'])->result()[0];
                $last = $this->HistoriModel->get(['tb_histori.id_skripsi' => $id_skripsi, 'desc'])->result()[0];
                $notif = [
                    'type' => '2', //ganti_judul
                    'action' => 'request',
                    'description' => 'Pengajuan Ganti Judul',
                    'user_from' => $skripsi->username,
                    'user_to' => $bimbingan->nidn,
                    'ref_id' => $id_histori
                ];
                $bimbingan2 = $this->BimbinganModel
                    ->get(['nim' => $skripsi->username, 'status_pengajuan' => 'diterima', 'pembimbing' => '2'])->result()[0];
                $notif2 = [
                    'type' => '2', //ganti_judul
                    'action' => 'request',
                    'description' => 'Pengajuan Ganti Judul',
                    'user_from' => $skripsi->username,
                    'user_to' => $bimbingan2->nidn,
                    'ref_id' => $id_histori
                ];
                $ok  = $this->NotifikasiModel->add($notif);
                $ok2 = $this->NotifikasiModel->add($notif2);
                $this->session->set_flashdata('success', "Data berhasil diajukan!");
                redirect('mahasiswa-skripsi');
            } else {
                $this->session->set_flashdata('error', "Data gagal diajukan!");
                redirect('mahasiswa-skripsi');
            }
        }
    }

    public function update($id)
    {
        $data = $this->input->post(NULL, true);
        $status = $this->SkripsiModel->update($id, $data);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil diupdate!");
            redirect('mahasiswa-skripsi');
        } else {
            $this->session->set_flashdata('error', "Data gagal diupdate!");
            redirect('mahasiswa-skripsi/edit/' . $id);
        }
    }

    public function updateH($id)
    {
        $data = $this->input->post(NULL, true);
        $status = $this->HistoriModel->update($id, $data);
        if ($status == 1) {
            $this->session->set_flashdata('success', "Data berhasil diupdate!");
            redirect('mahasiswa-skripsi');
        } else {
            $this->session->set_flashdata('error', "Data gagal diupdate!");
            redirect('mahasiswa-skripsi/editH/' . $id);
        }
    }

    public function delete($id)
    {
        $id_b    = $id - 1;
        $data    = array('visibility' => '0');
        $status0 = $this->HistoriModel->update($id_b, $data);
        $status  = $this->HistoriModel->delete($id);
        if ($status0 == 1) {
            if ($status == 1) {
                $this->session->set_flashdata('success', "Pengajuan berhasil dibatalkan!");
                redirect('mahasiswa-skripsi');
            } else {
                $this->session->set_flashdata('error', "Pengajuan gagal dibatalkan!");
                redirect('mahasiswa-skripsi');
            }
        } else {
            $this->session->set_flashdata('error', "Pengajuan gagal dibatalkan!");
            redirect('mahasiswa-skripsi');
        }
    }
}
