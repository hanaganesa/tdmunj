<?php

class MahasiswaController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('username')) {
            redirect('signin');
        }
        $this->load->model('MahasiswaModel');
    }

    public function index()
    {
        $data['jenisAkun'] = "mahasiswa";
        $data['judul'] = "Profile";
        $username = $this->session->userdata('username');
        $data['mahasiswa'] = $this->MahasiswaModel->getById($username);
        // nama
        $nim = $this->session->userdata('username');
        $data['namaRaw'] = $this->MahasiswaModel->get(['username' => $nim])->row();
        $data['nama'] = $data['namaRaw']->nama;
        //endNama
        $this->load->view('mahasiswa/mahasiswa', $data);
    }

    public function update($id)
    {
        $username = $this->input->post('username');
        $nama     = $this->input->post('nama');
        $email    = $this->input->post('email');
        $pass     = $this->input->post('password');
        $password = md5($pass);
        $conf_pass = $this->input->post('confirm_password');
        $confirm_password = md5($pass);
        $foto     = $_FILES['foto'];

        if ($foto['name'] != NULL) {
            $config['upload_path']   = './assets/foto/';
            $config['allowed_types'] = 'jpg|png|gif';
            $config['file_name']     = $username;
            $config['overwrite']     = true;
            $config['max_size']      = 5000000;
            $path = $_FILES['foto']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                $this->session->set_flashdata('error', "Upload foto gagal!");
                redirect('mahasiswa-profile');
            } else {
                $foto = $this->upload->data('foto');
            }
            $foto = $username . '.' . $ext;
        } else {
            $foto = 'profile.png';
        }

        $data     = array(
            'username' => $username,
            'nama' => $nama,
            'password' => $password,
            'foto' => $foto,
            'email' => $email
        );

        $this->form_validation->set_rules(
            'username',
            'Username',
            'required|min_length[10]|max_length[10]'
        );
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['jenisAkun'] = "mahasiswa";
            $data['judul'] = "Profile";
            $data['mahasiswa'] = $this->MahasiswaModel->getById($id);
            $this->load->view('mahasiswa/mahasiswa', $data);
        } else {
            $status = $this->MahasiswaModel->update($id, $data);
            if ($status == 1) {
                $this->session->set_flashdata('success', "Data berhasil diupdate!");
                redirect('dashboard-mahasiswa', 'refresh');
            } else {
                $this->session->set_flashdata('error', "Data gagal diupdate!");
                redirect('mahasiswa-profile');
            }
        }
    }
}
