<?php

class UmumController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SkripsiModel');
        $this->load->model('HistoriModel');
    }

    public function index()
    {
        $this->load->view('frontend/Index');
    }

    public function listSkripsi()
    {
        $data['judul'] = "List Skripsi";
        $data['skripsi'] = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        $this->load->view('frontend/skripsi', $data);
    }

    public function detailSkripsi($id, $id_skripsi)
    {
        $data['judul']      = "Skripsi";
        $data['histori']    = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->result();
        $data['skripsi']    = $this->SkripsiModel->get(['tb_skripsi.id_skripsi' => $id_skripsi])->result();
        $data['histori_count'] = $this->HistoriModel->get(['tb_histori.id_histori' => $id], 'desc', 1)->num_rows();
        $this->load->view('frontend/detailSkripsi', $data);
    }

    public function kesamaanSkripsi()
    {
        $data['judul'] = "Kesamaan Skripsi";
        $this->load->view('frontend/kesamaanSkripsi', $data);
    }

    public function kesamaanUkur()
    {
        $data['judul'] = "Kesamaan Skripsi";
        $data['judulSkripsi']   = $this->input->post('judul');
        $data['abstrak']        = $this->input->post('abstrak');
        $data['skripsiMhsLain'] = $this->HistoriModel->getWhere(['status_histori' => 'diterima', 'visibility' => '0'])->result();
        $newSkripsiMhsLain = [];
        foreach ($data['skripsiMhsLain'] as $key => $m) {
            similar_text($data['judulSkripsi'], $m->judul, $percent);
            similar_text($data['abstrak'], $m->abstrak, $percent2);
            $m->percent = round($percent);
            $m->percent2 = round($percent2);
            $m->average = ($percent + $percent2) * 0.5;
            $newSkripsiMhsLain[$key] = $m;
        }
        usort($newSkripsiMhsLain, function ($a, $b) {
            if ($a->average == $b->average) {
                return 0;
            }
            return ($a->average > $b->average) ? -1 : 1;
        });
        $data['skripsiMhsLain'] = $newSkripsiMhsLain;
        $this->load->view('frontend/kesamaanSkripsi', $data);
    }

    public function help()
    {
        $data['judul'] = "Instruksi Manual";
        $this->load->view('frontend/help', $data);
    }
}
