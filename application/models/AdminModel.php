<?php

class AdminModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
        return $this->db->insert('tb_admin', $data);
    }

    public function get($where = '', $limit = '')
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }

        $this->db->order_by('username', 'desc');
        $query = $this->db->get('tb_admin');
        return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['username' => $id]);
        $query = $this->db->get('tb_admin')->row();
        return $query;
    }

    public function update($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('username', $param);
        return $this->db->update('tb_admin', $data);
    }

    public function updatePassword($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('reset_password', $param);
        return $this->db->update('tb_admin', $data);
    }

    public function updateStatus($id, $status)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('username', $id);
        return $this->db->update('tb_admin', array('status' => $status));
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('username', $id);
        return $this->db->delete('tb_admin');
    }

    public function login()
    {
        $this->db->query("SET sql_mode = '' ");
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        return $this->db->get_where('tb_admin', array('username' => $username, 'password' => md5($password)));
    }

    public function updateResetKey($email, $reset_key)
    {
        $this->db->where('email', $email);
        $data = array('reset_password' => $reset_key);
        $this->db->update('tb_admin', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
