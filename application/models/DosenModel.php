<?php 

class DosenModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
		return $this->db->insert('tb_dosen', $data);
    }

    public function get($where='',$limit='')
    {
        $this->db->query("SET sql_mode = '' ");
        if(!empty($where)){$this->db->where($where);}
        if(!empty($limit)){$this->db->limit($limit);}
		
        $this->db->order_by('username', 'desc');
		$query = $this->db->get('tb_dosen');
		return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['username'=>$id]);
		$query = $this->db->get('tb_dosen')->row();
		return $query;
    }

    public function update($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('username', $param);
		return $this->db->update('tb_dosen', $data);
    }

    public function updateStatus($id, $status)
	{
		$this->db->query("SET sql_mode = '' ");
		$this->db->where('username', $id);
		return $this->db->update('tb_dosen', array('status'=>$status));
	}

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('username', $id);
		return $this->db->delete('tb_dosen');
    }

    public function login()
    {
        $this->db->query("SET sql_mode = '' ");
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		return $this->db->get_where('tb_dosen', array('username' => $username, 'password' => md5($password) ));
    }
}


?>