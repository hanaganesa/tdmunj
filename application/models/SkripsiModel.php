<?php 

class SkripsiModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
		return $this->db->insert('tb_skripsi', $data);
    }

    public function get($where='', $limit='')
    {
        $this->db->query("SET sql_mode = '' ");
        if(!empty($where)){$this->db->where($where);}
        if(!empty($limit)){$this->db->limit($limit);}
        
        $this->db->join('tb_mahasiswa', 'tb_skripsi.username=tb_mahasiswa.username', 'inner');
		$query = $this->db->get('tb_skripsi');
		return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['tb_skripsi.id_skripsi'=>$id]);
        $this->db->join('tb_mahasiswa', 'tb_skripsi.username=tb_mahasiswa.username', 'inner');
		$query = $this->db->get('tb_skripsi')->row();
		return $query;
    }

    public function getSummary()
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->select('kategori, COUNT(id_skripsi) as total');
        $this->db->order_by('id_skripsi', 'asc');
        $this->db->group_by('kategori');
		$query = $this->db->get('tb_skripsi');
		return $query;
    }

    public function update($id, $data)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('id_skripsi', $id);
		return $this->db->update('tb_skripsi', $data);
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('id_skripsi', $id);
		return $this->db->delete('tb_skripsi');
    }
}
