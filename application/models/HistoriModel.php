<?php

class HistoriModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
        return $this->db->insert('tb_histori', $data);
    }

    public function get($where = '', $order = '', $limit = '')
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($order)) {
            $this->db->order_by('tb_histori.id_histori', $order);
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }

        $this->db->join('tb_skripsi', 'tb_histori.id_skripsi=tb_skripsi.id_skripsi', 'inner');
        $this->db->join('tb_mahasiswa', 'tb_skripsi.username=tb_mahasiswa.username', 'inner');
        $this->db->join('tb_bimbingan', 'tb_skripsi.username=tb_bimbingan.nim', 'inner');
        $query = $this->db->get('tb_histori');
        return $query;
    }

    public function getWhere($where = '', $order = '')
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($order)) {
            $this->db->order_by('tb_histori.id_histori', $order);
        }

        $this->db->join('tb_skripsi', 'tb_histori.id_skripsi=tb_skripsi.id_skripsi', 'inner');
        $this->db->join('tb_mahasiswa', 'tb_skripsi.username=tb_mahasiswa.username', 'inner');
        $query = $this->db->get('tb_histori');
        return $query;
    }

    //select distinct
    public function getWhereNot($where = '', $nim)
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->db->join('tb_skripsi', 'tb_histori.id_skripsi=tb_skripsi.id_skripsi', 'inner');
        $this->db->join('tb_mahasiswa', 'tb_skripsi.username=tb_mahasiswa.username', 'inner');
        $this->db->where_not_in('tb_mahasiswa.username', $nim);
        $query = $this->db->get('tb_histori');
        return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['tb_histori.id_histori' => $id]);
        $query = $this->db->get('tb_histori')->row();
        return $query;
    }

    public function getSummary()
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->select('kategori, COUNT(id_histori) as total');
        $this->db->order_by('id_histori', 'asc');
        $this->db->group_by('kategori');
        $query = $this->db->get('tb_histori');
        return $query;
    }

    public function update($id, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id_histori', $id);
        return $this->db->update('tb_histori', $data);
    }

    public function updateAll($id, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id_skripsi', $id);
        return $this->db->update('tb_histori', $data);
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id_histori', $id);
        return $this->db->delete('tb_histori');
    }
}
