<?php 

class PostinganModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
		return $this->db->insert('tb_postingan', $data);
    }

    public function get($where='',$limit='')
    {
        $this->db->query("SET sql_mode = '' ");
        if(!empty($where)){$this->db->where($where);}
        if(!empty($limit)){$this->db->limit($limit);}
		
        $this->db->order_by('id_postingan', 'desc');
		$query = $this->db->get('tb_postingan');
		return $query;
    }
    
    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['id_postingan'=>$id]);
		$query = $this->db->get('tb_postingan')->row();
		return $query;
    }

    public function update($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('id_postingan', $param);
		return $this->db->update('tb_postingan', $data);
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
		$this->db->where('id_postingan', $id);
		return $this->db->delete('tb_postingan');
    }
}


?>