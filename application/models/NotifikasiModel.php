<?php

class NotifikasiModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
        return $this->db->insert('tb_notifikasi', $data);
    }

    public function get($where = '', $limit = '')
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }

        $this->db->order_by('created_at', 'desc');
        $query = $this->db->get('tb_notifikasi');
        return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['id' => $id]);
        $query = $this->db->get('tb_notifikasi')->row();
        return $query;
    }

    public function update($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id', $param);
        return $this->db->update('tb_notifikasi', $data);
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id', $id);
        return $this->db->delete('tb_notifikasi');
    }
}
