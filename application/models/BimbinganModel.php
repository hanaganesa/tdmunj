<?php

class BimbinganModel extends CI_Model
{
    public function add($data)
    {
        $this->db->query("SET sql_mode = '' ");
        return $this->db->insert('tb_bimbingan', $data);
    }

    public function get($where = '', $where2 = '', $limit = '')
    {
        $this->db->query("SET sql_mode = '' ");
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($where2)) {
            $this->db->where($where2);
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }

        $this->db->join('tb_mahasiswa', 'tb_bimbingan.nim=tb_mahasiswa.username', 'inner');
        $this->db->join('tb_dosen', 'tb_dosen.username=tb_bimbingan.nidn', 'inner');
        $this->db->join('tb_skripsi', 'tb_skripsi.username=tb_bimbingan.nim', 'inner');
        $query = $this->db->get('tb_bimbingan');
        return $query;
    }

    public function getById($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where(['nidn' => $id]);
        $query = $this->db->get('tb_bimbingan')->row();
        return $query;
    }

    public function update($param, $data)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id_bimbingan', $param);
        return $this->db->update('tb_bimbingan', $data);
    }

    public function delete($id)
    {
        $this->db->query("SET sql_mode = '' ");
        $this->db->where('id_bimbingan', $id);
        return $this->db->delete('tb_bimbingan');
    }
}
