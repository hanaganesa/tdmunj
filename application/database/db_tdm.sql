-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 05:47 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `username` varchar(20) NOT NULL,
  `nama` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `reset_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`username`, `nama`, `password`, `email`, `reset_password`) VALUES
('kikuchi', 'kikuchi', 'aee2be2ae68c5cfb869e1c2d299fca33', 'kikuchi@gmail.com', NULL),
('nursaadah', 'Yamaguchi Momoe', '67d1bbd776d8989b879d65c426d3e775', 'yamagushimomoe@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bimbingan`
--

CREATE TABLE `tb_bimbingan` (
  `id_bimbingan` int(20) NOT NULL,
  `nidn` varchar(20) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `pembimbing` text NOT NULL,
  `status_pengajuan` enum('diterima','pengajuan','ditolak') NOT NULL,
  `waktu` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bimbingan`
--

INSERT INTO `tb_bimbingan` (`id_bimbingan`, `nidn`, `nim`, `pembimbing`, `status_pengajuan`, `waktu`) VALUES
(53, '1111111112', '6704188881', '1', 'diterima', '2020-04-14 22:40:00'),
(57, '1111111112', '6704188883', '1', 'diterima', '2020-04-14 23:09:06'),
(59, '1111111111', '6704188881', '2', 'diterima', '2020-04-15 22:06:34'),
(60, '1111111112', '6704188886', '1', 'diterima', '2020-04-16 10:27:12'),
(61, '1111111111', '6704188886', '2', 'diterima', '2020-04-16 10:27:27'),
(70, '1111111111', '6704188889', '2', 'diterima', '2020-04-17 21:02:06'),
(71, '1111111112', '6704188889', '1', 'diterima', '2020-04-17 21:34:43'),
(72, '1111111112', '6704188882', '1', 'diterima', '2020-04-18 00:02:49'),
(73, '1111111111', '6704188882', '2', 'diterima', '2020-04-18 00:03:07'),
(74, '1111111112', '6704188887', '1', 'diterima', '2020-04-18 00:54:15'),
(75, '1111111111', '6704188887', '2', 'diterima', '2020-04-18 00:54:19'),
(77, '1111111111', '6704188883', '2', 'pengajuan', '2020-04-19 18:23:25'),
(78, '1111111111', '6704111111', '1', 'diterima', '2020-04-20 16:57:01'),
(79, '1111111112', '6704111111', '2', 'diterima', '2020-04-20 16:57:07'),
(80, '1111111111', '6704111113', '1', 'diterima', '2020-04-22 13:01:31'),
(81, '1111111112', '6704111113', '2', 'diterima', '2020-04-22 13:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dosen`
--

CREATE TABLE `tb_dosen` (
  `username` varchar(20) NOT NULL,
  `nama_dosen` text NOT NULL,
  `password` text NOT NULL,
  `status` enum('aktif','nonaktif') NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dosen`
--

INSERT INTO `tb_dosen` (`username`, `nama_dosen`, `password`, `status`, `email`) VALUES
('0111111110', 'Sunshine Ikezaki', 'd3fcc45c15069c1971488b8cc633a699', 'aktif', ''),
('0111111111', 'Jin Katagiri', '24c9586fb5384a263ec9fd4a89bbac59', 'aktif', ''),
('0111111112', 'Tetsuro Degawa', '642dbdc577b212c274001b6c72e67368', 'aktif', ''),
('0111111113', 'Daimaou Kosaka', '97c8051459003a04eef963c1bf3a4ef6', 'aktif', ''),
('0111111114', 'Naomi Watanabe', '1ad6b009c292910a7cd2b613fc0c6fe6', 'aktif', ''),
('0111111115', 'Masatoshi Hamada', 'ef5de9ee2fa04f1deeb427ce70f5a273', 'aktif', ''),
('0111111116', 'Sanma Akashiya', '75f8253e7b71514ec1c4383b974179f2', 'aktif', ''),
('0111111117', 'Tamori', '18681fdb1dd385bc109d041e7ce7062c', 'aktif', ''),
('0111111118', 'Takeshi Kitano', 'eded92986ed830f27436b0756193cfe3', 'aktif', ''),
('0111111119', 'Masaki Aiba', '61f0629f51fe9aef57e68d8cf8941b2e', 'aktif', ''),
('0111111120', 'Rika Adachi', '3c3bf36e1900beb5474dc8271d278023', 'aktif', ''),
('1111111111', 'Konno Yoshio', 'e11170b8cbd2d74102651cb967fa28e5', 'aktif', ''),
('1111111112', 'Himura Yuuki', '2a7796ce858d1514242d628aff038992', 'aktif', ''),
('1111111114', 'Shitara Osamu', '362cd236ad8ad79d0fbc35389c49f3ef', 'nonaktif', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori`
--

CREATE TABLE `tb_histori` (
  `id_histori` int(11) NOT NULL,
  `id_skripsi` int(11) NOT NULL,
  `judul_histori` longtext NOT NULL,
  `abstrak_histori` longtext NOT NULL,
  `status_histori` enum('pengajuan','diterima','ditolak') NOT NULL,
  `kategori_histori` enum('pendidikan','linguistik','komprehensif') NOT NULL,
  `status1` enum('pengajuan','diterima','ditolak','-') NOT NULL,
  `status2` enum('pengajuan','diterima','ditolak','-') NOT NULL,
  `tanggal_histori` datetime NOT NULL DEFAULT current_timestamp(),
  `visibility` enum('0','1') NOT NULL,
  `alasan` longtext DEFAULT NULL,
  `komentar` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_histori`
--

INSERT INTO `tb_histori` (`id_histori`, `id_skripsi`, `judul_histori`, `abstrak_histori`, `status_histori`, `kategori_histori`, `status1`, `status2`, `tanggal_histori`, `visibility`, `alasan`, `komentar`) VALUES
(82, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-14 22:28:28', '1', NULL, NULL),
(83, 103, 'Aplikasi Sensor Sidik Jari', 'Abstrak aplikasi sensor sidik jari', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-14 22:35:38', '1', NULL, NULL),
(84, 104, 'judul skripsi', 'abstrak skripsi', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-14 23:00:01', '1', NULL, NULL),
(85, 104, 'ganti judul skripsi', 'abstrak ganti judul skripsi', 'pengajuan', 'komprehensif', 'pengajuan', 'pengajuan', '2020-04-14 23:11:31', '1', NULL, NULL),
(86, 104, 'ganti judul skripsi', 'abstrak ganti judul skripsi', 'pengajuan', 'komprehensif', 'diterima', 'pengajuan', '2020-04-14 23:13:09', '1', NULL, NULL),
(87, 104, 'ganti judul skripsi', 'abstrak ganti judul skripsi', 'ditolak', 'komprehensif', 'diterima', 'ditolak', '2020-04-14 23:14:55', '1', NULL, NULL),
(88, 104, 'judul skripsi diterima', 'abstrak skripsi diterima', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-14 23:16:05', '1', NULL, NULL),
(89, 104, 'judul skripsi diterima', 'abstrak skripsi diterima', 'pengajuan', 'pendidikan', 'pengajuan', 'diterima', '2020-04-14 23:16:26', '1', NULL, NULL),
(90, 104, 'judul skripsi diterima', 'abstrak skripsi diterima', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-14 23:17:00', '0', NULL, NULL),
(91, 103, 'Aplikasi Sensor Sidik Jari ganti', 'Abstrak aplikasi sensor sidik jari ganti', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-15 22:07:52', '1', NULL, NULL),
(92, 103, 'Aplikasi Sensor Sidik Jari ganti', 'Abstrak aplikasi sensor sidik jari ganti', 'pengajuan', 'pendidikan', 'pengajuan', 'diterima', '2020-04-15 22:09:01', '1', NULL, NULL),
(93, 103, 'Aplikasi Sensor Sidik Jari ganti coba lagi', 'Abstrak aplikasi sensor sidik jari ganti lagi', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-15 22:09:38', '0', '', NULL),
(94, 105, 'judul skripsi', 'abstrak skripsi', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-16 10:25:59', '1', NULL, NULL),
(95, 105, 'judul skripsi ganti judul', 'abstrak skripsi ganti judul', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-16 10:30:08', '1', NULL, NULL),
(96, 105, 'judul skripsi ganti judul', 'abstrak skripsi ganti judul', 'pengajuan', 'pendidikan', 'diterima', 'pengajuan', '2020-04-16 10:31:40', '1', NULL, NULL),
(97, 105, 'judul skripsi ganti judul', 'abstrak skripsi ganti judul', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-16 10:32:24', '0', NULL, NULL),
(98, 105, 'judul skripsi ganti judul baru', 'abstrak skripsi ganti judul baru', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-16 10:44:01', '1', NULL, NULL),
(99, 105, 'judul skripsi ganti judul baru', 'abstrak skripsi ganti judul baru', 'pengajuan', 'linguistik', 'pengajuan', 'ditolak', '2020-04-16 11:02:43', '1', NULL, NULL),
(100, 103, 'Aplikasi Sensor Sidik Jari ganti coba', 'Abstrak aplikasi sensor sidik jari ganti coba', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-17 15:07:24', '1', 'pengen ganti aja', NULL),
(101, 103, 'Aplikasi Sensor Sidik Jari ganti coba', 'Abstrak aplikasi sensor sidik jari ganti coba', 'pengajuan', 'pendidikan', 'pengajuan', 'diterima', '2020-04-17 17:38:14', '1', NULL, NULL),
(102, 103, 'Aplikasi Sensor Sidik Jari ganti coba', 'Abstrak aplikasi sensor sidik jari ganti coba', 'ditolak', 'pendidikan', 'ditolak', 'diterima', '2020-04-17 17:45:33', '0', NULL, 'Perbaiki lagi sikapnya wkkw'),
(123, 105, 'judul skripsi ganti judul baru', 'abstrak skripsi ganti judul baru', 'ditolak', 'linguistik', 'ditolak', 'ditolak', '2020-04-17 19:31:54', '0', NULL, ''),
(124, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-17 19:33:49', '1', NULL, ''),
(125, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-17 19:35:44', '1', NULL, ''),
(126, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-17 19:36:16', '1', NULL, ''),
(127, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'ditolak', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-17 19:38:55', '1', NULL, ''),
(131, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-17 19:50:06', '1', 'pengen liburan', NULL),
(132, 106, 'judul skripsi', 'abstrak skripsi', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-17 19:53:02', '1', NULL, NULL),
(138, 106, 'judul skripsi', 'abstrak skripsi', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-17 20:16:30', '1', 'asq', NULL),
(139, 106, 'judul skripsi', 'abstrak skripsi', 'pengajuan', 'linguistik', 'diterima', 'pengajuan', '2020-04-17 20:17:03', '1', NULL, ''),
(140, 106, 'judul skripsi', 'abstrak skripsi', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-17 20:17:29', '0', NULL, ''),
(141, 106, 'judul skripsi', 'abstrak skripsi', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-17 20:40:04', '1', 'a', NULL),
(142, 106, 'judul skripsi', 'abstrak skripsi', 'ditolak', 'linguistik', 'pengajuan', 'ditolak', '2020-04-17 20:40:38', '0', NULL, 'kurang'),
(143, 106, 'judul skripsi', 'abstrak skripsi', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-17 20:41:09', '1', 'coba lagi', NULL),
(144, 106, 'judul skripsi', 'abstrak skripsi', 'ditolak', 'linguistik', 'ditolak', 'pengajuan', '2020-04-17 20:41:28', '0', NULL, 'gabole'),
(145, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'diterima', 'pengajuan', '2020-04-18 00:39:53', '1', NULL, ''),
(146, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-18 00:41:32', '1', NULL, 'nais'),
(147, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-18 00:42:33', '1', 'pengen beli ikan cupang', NULL),
(148, 102, 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'ditolak', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-18 00:51:03', '1', NULL, 'kurang detail'),
(149, 107, 'judul kaki', 'abstrak kaki', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-18 00:53:49', '0', NULL, NULL),
(150, 107, 'judul kaki baru', 'abstrak kaki baru', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-18 14:30:53', '1', 'pengen beli tuing tuing', NULL),
(151, 107, 'judul kaki baru', 'abstrak kaki baru', 'ditolak', 'linguistik', 'ditolak', 'pengajuan', '2020-04-18 14:32:34', '0', 'pengen beli tuing tuing', 'gaboleh'),
(152, 107, 'judul kaki coba acc', 'abstrak kaki coba acc', 'pengajuan', 'komprehensif', 'pengajuan', 'pengajuan', '2020-04-18 14:39:23', '0', 'pengen aja', NULL),
(153, 102, 'Aplikasi Pembelajaran Online SD maiyan', 'Abstrak dari aplikasi pembelajaran online SD maiyan', 'pengajuan', 'linguistik', 'pengajuan', 'pengajuan', '2020-04-18 17:44:27', '1', 'bosen', NULL),
(154, 102, 'Aplikasi Pembelajaran Online SD maiyan', 'Abstrak dari aplikasi pembelajaran online SD maiyan', 'pengajuan', 'linguistik', 'pengajuan', 'diterima', '2020-04-18 18:04:26', '1', 'bosen', 'baik'),
(155, 102, 'Aplikasi Pembelajaran Online SD maiyan', 'Abstrak dari aplikasi pembelajaran online SD maiyan', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-18 18:05:01', '0', 'bosen', 'lanjutkan'),
(156, 102, 'Aplikasi Pembelajaran Online SD maiyan oi', 'Abstrak dari aplikasi pembelajaran online SD maiyan oi', 'pengajuan', 'komprehensif', 'pengajuan', 'pengajuan', '2020-04-18 19:16:19', '0', 'pengen aja', NULL),
(157, 108, 'judul', 'abstrak', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-20 16:56:17', '0', NULL, NULL),
(158, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-22 12:46:25', '1', NULL, NULL),
(159, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw.', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-22 13:09:14', '1', 'perusahaan menolak', NULL),
(160, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw.', 'pengajuan', 'pendidikan', 'diterima', 'pengajuan', '2020-04-22 13:10:49', '1', 'perusahaan menolak', 'boleh'),
(161, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw.', 'diterima', 'pendidikan', 'diterima', 'diterima', '2020-04-22 13:11:21', '1', 'perusahaan menolak', 'perbaiki bla bla bla'),
(162, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed.', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-22 13:12:21', '1', 'pengen aja', NULL),
(163, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed.', 'ditolak', 'pendidikan', 'pengajuan', 'ditolak', '2020-04-22 13:12:42', '1', 'pengen aja', 'gaboleh ganti terus'),
(164, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sedd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-22 13:29:07', '1', 'pengen', NULL),
(165, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sedd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-22 13:29:31', '1', 'pengen', 'gaboleh'),
(166, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sedd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-22 13:30:12', '1', 'pengen', 'gaboleh'),
(167, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sedd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-22 13:30:49', '1', 'pengen', 'g'),
(168, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new sedd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw sed', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-22 13:31:16', '1', 'pengen', 'a'),
(169, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new pin', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw pin', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-22 13:33:29', '1', 'aa', NULL),
(170, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new pin', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw pin', 'ditolak', 'pendidikan', '-', 'ditolak', '2020-04-22 13:38:49', '1', 'aa', 'ga'),
(171, 109, 'tristique senectus et netus et malesuada fames ac turpis egestas sed new pin', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum nw pin', 'diterima', 'pendidikan', 'diterima', 'ditolak', '2020-04-22 13:39:23', '0', 'aa', 'ok'),
(172, 103, 'Aplikasi Sensor Sidik Jari ganti coba lagi yes', 'Abstrak aplikasi sensor sidik jari ganti lagi yes', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-22 13:47:15', '1', 'a', NULL),
(173, 103, 'Aplikasi Sensor Sidik Jari ganti coba lagi yes', 'Abstrak aplikasi sensor sidik jari ganti lagi yes', 'ditolak', 'pendidikan', '-', 'ditolak', '2020-04-22 13:47:40', '0', 'a', 'a'),
(174, 103, 'Aplikasi Sensor Sidik Jari ganti coba lagi ganti judul', 'Abstrak aplikasi sensor sidik jari ganti lagi ganti judul', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-24 14:00:41', '1', 'alasannya karena perusahaannya tidak mau diajak kerja sama', NULL),
(175, 103, 'Aplikasi Sensor Sidik Jari ganti coba lagi ganti judul', 'Abstrak aplikasi sensor sidik jari ganti lagi ganti judul', 'pengajuan', 'pendidikan', 'pengajuan', 'diterima', '2020-04-24 14:03:58', '0', 'alasannya karena perusahaannya tidak mau diajak kerja sama', 'Boleh ganti judul'),
(176, 108, 'judul', 'abstrak', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-27 13:30:13', '1', 'mau aja', NULL),
(177, 108, 'judul', 'abstrak', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-27 13:30:35', '0', 'mau aja', 'tidak boleh'),
(178, 108, 'judul baru', 'abstrak baru', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-27 13:33:59', '1', 'ingin pindah perusahaan', NULL),
(179, 108, 'judul baru', 'abstrak baru', 'ditolak', 'pendidikan', '-', 'ditolak', '2020-04-27 13:35:45', '0', 'ingin pindah perusahaan', 'tidak boleh ganti judul'),
(180, 108, 'judul ganti baru', 'abstrak ganti baru', 'pengajuan', 'pendidikan', 'pengajuan', 'pengajuan', '2020-04-30 15:16:38', '1', 'ingin ganti judul', NULL),
(181, 108, 'judul ganti baru', 'abstrak ganti baru', 'ditolak', 'pendidikan', 'ditolak', '-', '2020-04-30 15:24:08', '0', 'ingin ganti judul', 'tidak boleh ganti judul'),
(182, 110, 'judul skripsi hana', 'abstrak skripsi hana', 'diterima', 'linguistik', 'diterima', 'diterima', '2020-04-30 15:35:42', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `username` varchar(20) NOT NULL,
  `nama` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `status` enum('aktif','nonaktif') DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT current_timestamp(),
  `reset_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mahasiswa`
--

INSERT INTO `tb_mahasiswa` (`username`, `nama`, `email`, `password`, `status`, `foto`, `tanggal`, `reset_password`) VALUES
('6704111111', 'Pablo Escobar', 'pasblo@gmail.com', '1e9bb0e6823658d96ce24a2ad7cdd918', 'aktif', 'profile.png', '2020-04-20', NULL),
('6704111112', NULL, NULL, NULL, NULL, NULL, '2020-04-20', NULL),
('6704111113', 'Kakehashi Sayaka', 'sayaka@gmail.com', '874522250a5ef8164b70df8585cc05ff', 'aktif', 'profile.png', '2020-04-20', NULL),
('6704111116', 'Hana Fitria', 'hana@gmail.com', '2a8a7deb1c9ab0a4b9c074b641c4e5c1', 'aktif', 'profile.png', '2020-04-30', NULL),
('6704111117', NULL, NULL, NULL, NULL, NULL, '2020-04-30', NULL),
('6704111118', NULL, NULL, NULL, NULL, NULL, '2020-04-30', NULL),
('6704188881', 'Endo Sakura', 'haefge@gmail.com', 'ab837fdb7a02bc428fe4a7cb77a8392f', 'aktif', '6704188881.jpg', '2020-04-14', ''),
('6704188882', 'Shiraishi Mai', 'mai@gmail.com', '0d524f0934a054b4fe8bb53b1ba8fed2', 'aktif', 'profile.png', '2020-04-14', 'vecxr4wA0ZC9GJoiLMO5kRQ62gI1tahXBSKTp7HjNm8yWVDYlb'),
('6704188883', 'Matsumura Sayuri', 'sayuri@gmail.com', 'b2d3d0d6c29d0248b71b48cda3e33c02', 'aktif', '6704188883.jpg', '2020-04-14', NULL),
('6704188886', 'Shiraishi Mai', 'mai@gmail.com', '0d524f0934a054b4fe8bb53b1ba8fed2', 'aktif', 'profile.png', '2020-04-16', 'vecxr4wA0ZC9GJoiLMO5kRQ62gI1tahXBSKTp7HjNm8yWVDYlb'),
('6704188887', 'Kaki Haruka', 'kakki@gmail.com', '573fe1dbeb8f9432f80d588620b775ce', 'aktif', 'profile.png', '2020-04-18', NULL),
('6704188889', 'Hori Miona', 'miona@gmail.com', '848da8e552161d9cf6ace69c4e6a2177', 'aktif', 'profile.png', '2020-04-17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_notifikasi`
--

CREATE TABLE `tb_notifikasi` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `user_from` varchar(20) NOT NULL,
  `user_to` varchar(20) NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `ref_id_dos` int(11) DEFAULT NULL,
  `is_read` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notifikasi`
--

INSERT INTO `tb_notifikasi` (`id`, `type`, `action`, `description`, `user_from`, `user_to`, `ref_id`, `ref_id_dos`, `is_read`, `created_at`) VALUES
(57, 2, 'request', 'Pengajuan Ganti Judul', '6704188882', '1111111112', 102, NULL, 1, '2020-04-18 10:44:27'),
(58, 2, 'request', 'Pengajuan Ganti Judul', '6704188882', '1111111111', 102, NULL, 1, '2020-04-18 10:44:27'),
(59, 2, 'acc', 'Ganti Judul Diterima', '1111111111', '6704188882', 82, NULL, 1, '2020-04-18 11:04:26'),
(60, 2, 'acc', 'Ganti Judul Diterima', '1111111112', '6704188882', 82, NULL, 1, '2020-04-18 11:05:01'),
(61, 2, 'request', 'Pengajuan Ganti Judul', '6704188882', '1111111112', 102, NULL, 1, '2020-04-18 12:16:19'),
(62, 2, 'request', 'Pengajuan Ganti Judul', '6704188882', '1111111111', 102, NULL, 1, '2020-04-18 12:16:19'),
(69, 1, 'request', 'Request Dosen Pembimbing', '6704111113', '1111111111', NULL, 80, 1, '2020-04-22 06:01:31'),
(70, 1, 'request', 'Request Dosen Pembimbing', '6704111113', '1111111112', NULL, 81, 1, '2020-04-22 06:01:38'),
(71, 1, 'acc', 'Request Dosen Pembimbing', '1111111111', '6704111113', NULL, 80, 1, '2020-04-22 06:07:30'),
(72, 1, 'acc', 'Request Dosen Pembimbing', '1111111112', '6704111113', NULL, 81, 1, '2020-04-22 06:07:44'),
(73, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111111', 158, NULL, 1, '2020-04-22 06:09:14'),
(74, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111112', 158, NULL, 1, '2020-04-22 06:09:14'),
(75, 2, 'acc', 'Ganti Judul Diterima', '1111111111', '6704111113', 158, NULL, 1, '2020-04-22 06:10:49'),
(76, 2, 'acc', 'Ganti Judul Diterima', '1111111112', '6704111113', 158, NULL, 1, '2020-04-22 06:11:21'),
(77, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111111', 161, NULL, 1, '2020-04-22 06:12:21'),
(78, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111112', 161, NULL, 1, '2020-04-22 06:12:21'),
(79, 2, 'tolak', 'Penolakan Ganti Judul', '1111111112', '6704111113', 158, NULL, 1, '2020-04-22 06:12:42'),
(80, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111111', 161, NULL, 1, '2020-04-22 06:29:07'),
(81, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111112', 161, NULL, 1, '2020-04-22 06:29:07'),
(82, 2, 'tolak', 'Penolakan Ganti Judul', '1111111111', '6704111113', 158, NULL, 1, '2020-04-22 06:31:16'),
(83, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111111', 161, NULL, 1, '2020-04-22 06:33:29'),
(84, 2, 'request', 'Pengajuan Ganti Judul', '6704111113', '1111111112', 161, NULL, 1, '2020-04-22 06:33:29'),
(85, 2, 'tolak', 'Penolakan Ganti Judul', '1111111112', '6704111113', 158, NULL, 0, '2020-04-22 06:38:50'),
(86, 2, 'acc', 'Ganti Judul Diterima', '1111111111', '6704111113', 158, NULL, 0, '2020-04-22 06:39:23'),
(87, 2, 'request', 'Pengajuan Ganti Judul', '6704188881', '1111111112', 93, NULL, 1, '2020-04-22 06:47:15'),
(88, 2, 'request', 'Pengajuan Ganti Judul', '6704188881', '1111111111', 93, NULL, 1, '2020-04-22 06:47:15'),
(89, 2, 'tolak', 'Penolakan Ganti Judul', '1111111111', '6704188881', 83, NULL, 1, '2020-04-22 06:47:40'),
(90, 2, 'request', 'Pengajuan Ganti Judul', '6704188881', '1111111112', 93, NULL, 0, '2020-04-24 07:00:42'),
(91, 2, 'request', 'Pengajuan Ganti Judul', '6704188881', '1111111111', 93, NULL, 1, '2020-04-24 07:00:42'),
(92, 2, 'acc', 'Ganti Judul Diterima', '1111111111', '6704188881', 83, NULL, 0, '2020-04-24 07:03:58'),
(93, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111111', 157, NULL, 1, '2020-04-27 06:30:13'),
(94, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111112', 157, NULL, 0, '2020-04-27 06:30:13'),
(95, 2, 'tolak', 'Penolakan Ganti Judul', '1111111111', '6704111111', 157, NULL, 0, '2020-04-27 06:30:35'),
(96, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111111', 157, NULL, 0, '2020-04-27 06:33:59'),
(97, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111112', 157, NULL, 0, '2020-04-27 06:33:59'),
(98, 2, 'tolak', 'Penolakan Ganti Judul', '1111111112', '6704111111', 157, NULL, 0, '2020-04-27 06:35:45'),
(99, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111111', 157, NULL, 1, '2020-04-30 08:16:38'),
(100, 2, 'request', 'Pengajuan Ganti Judul', '6704111111', '1111111112', 157, NULL, 0, '2020-04-30 08:16:38'),
(101, 2, 'tolak', 'Penolakan Ganti Judul', '1111111111', '6704111111', 157, NULL, 1, '2020-04-30 08:24:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_postingan`
--

CREATE TABLE `tb_postingan` (
  `id_postingan` int(11) NOT NULL,
  `judul_postingan` text NOT NULL,
  `isi_postingan` text NOT NULL,
  `link` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `tanggal_posting` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_postingan`
--

INSERT INTO `tb_postingan` (`id_postingan`, `judul_postingan`, `isi_postingan`, `link`, `username`, `tanggal_posting`) VALUES
(29, 'Hasil Pengujian Proposal Skripsi Semester 111 Tanggal 18 Agustus 2019', 'Dokumen dapat diakses melalui link dibawah.', 'drive.google.com/open?id=1QV9WQ8v5-7NXLxMhpBIkDt6r3XyJO5lQ', 'kikuchi', '2020-01-23 23:27:30'),
(30, 'Pembimbing Skripsi Semester 111 Tanggal 18 Agustus 2019', 'Dokumen dapat diakses melalui link dibawah.', 'drive.google.com/open?id=1AnOt_hSIUxX7uYvx79tZ9g1muyk_nqQr', 'kikuchi', '2020-01-23 23:28:51'),
(31, 'Angket Kepuasan Aplikasi', 'Berikut merupakan kuisioner untuk mengukur kepuasan user terhadap penggunaan aplikasi TDM UNJ. \r\nIsi kuisioner terdiri dari 8 pernyataan yang kurang lebih hanya memakan waktu 5 menit untuk melakukan pengisiannya. \r\nApabila berkenan mohon sertakan saran dan pendapat anda. Terima kasih.', 'https://akimotoyoda.typeform.com/to/dR76b0', 'kikuchi', '2020-02-17 20:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_skripsi`
--

CREATE TABLE `tb_skripsi` (
  `id_skripsi` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `judul` longtext NOT NULL,
  `abstrak` longtext NOT NULL,
  `kategori` enum('pendidikan','linguistik','komprehensif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_skripsi`
--

INSERT INTO `tb_skripsi` (`id_skripsi`, `username`, `judul`, `abstrak`, `kategori`) VALUES
(102, '6704188882', 'Aplikasi Pembelajaran Online SD', 'Abstrak dari aplikasi pembelajaran online SD', 'pendidikan'),
(103, '6704188881', 'Aplikasi Sensor Sidik Jari', 'Abstrak aplikasi sensor sidik jari', 'linguistik'),
(104, '6704188883', 'judul skripsi edit', 'abstrak skripsi edit', 'pendidikan'),
(105, '6704188886', 'judul skripsi', 'abstrak skripsi', 'linguistik'),
(106, '6704188889', 'judul skripsi', 'abstrak skripsi', 'linguistik'),
(107, '6704188887', 'judul kaki', 'abstrak kaki', 'pendidikan'),
(108, '6704111111', 'judul', 'abstrak', 'pendidikan'),
(109, '6704111113', 'tristique senectus et netus et malesuada fames ac turpis egestas sed', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'pendidikan'),
(110, '6704111116', 'judul skripsi hana', 'abstrak skripsi hana', 'linguistik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_bimbingan`
--
ALTER TABLE `tb_bimbingan`
  ADD PRIMARY KEY (`id_bimbingan`),
  ADD KEY `nim` (`nidn`,`nim`),
  ADD KEY `nim_2` (`nim`),
  ADD KEY `nidn` (`nidn`);

--
-- Indexes for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_histori`
--
ALTER TABLE `tb_histori`
  ADD PRIMARY KEY (`id_histori`),
  ADD KEY `id_judul` (`id_skripsi`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_notifikasi`
--
ALTER TABLE `tb_notifikasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_id_dos` (`ref_id_dos`),
  ADD KEY `ref_id` (`ref_id`);

--
-- Indexes for table `tb_postingan`
--
ALTER TABLE `tb_postingan`
  ADD PRIMARY KEY (`id_postingan`),
  ADD KEY `username` (`username`),
  ADD KEY `username_2` (`username`);

--
-- Indexes for table `tb_skripsi`
--
ALTER TABLE `tb_skripsi`
  ADD PRIMARY KEY (`id_skripsi`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bimbingan`
--
ALTER TABLE `tb_bimbingan`
  MODIFY `id_bimbingan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `tb_histori`
--
ALTER TABLE `tb_histori`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `tb_notifikasi`
--
ALTER TABLE `tb_notifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `tb_postingan`
--
ALTER TABLE `tb_postingan`
  MODIFY `id_postingan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tb_skripsi`
--
ALTER TABLE `tb_skripsi`
  MODIFY `id_skripsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_bimbingan`
--
ALTER TABLE `tb_bimbingan`
  ADD CONSTRAINT `tb_bimbingan_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `tb_mahasiswa` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_bimbingan_ibfk_2` FOREIGN KEY (`nidn`) REFERENCES `tb_dosen` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_histori`
--
ALTER TABLE `tb_histori`
  ADD CONSTRAINT `tb_histori_ibfk_1` FOREIGN KEY (`id_skripsi`) REFERENCES `tb_skripsi` (`id_skripsi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_notifikasi`
--
ALTER TABLE `tb_notifikasi`
  ADD CONSTRAINT `tb_notifikasi_ibfk_1` FOREIGN KEY (`ref_id_dos`) REFERENCES `tb_bimbingan` (`id_bimbingan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_notifikasi_ibfk_2` FOREIGN KEY (`ref_id`) REFERENCES `tb_histori` (`id_histori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_postingan`
--
ALTER TABLE `tb_postingan`
  ADD CONSTRAINT `tb_postingan_ibfk_1` FOREIGN KEY (`username`) REFERENCES `tb_admin` (`username`);

--
-- Constraints for table `tb_skripsi`
--
ALTER TABLE `tb_skripsi`
  ADD CONSTRAINT `tb_skripsi_ibfk_1` FOREIGN KEY (`username`) REFERENCES `tb_mahasiswa` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
